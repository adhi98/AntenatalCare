<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class WaliModel extends Model
{    
            protected $fillable = [
                'nama',
                'tanggal_lahir',
                'agama_id',
                'golongan_darah',
                'pendidikan_id',
                'pekerjaan_id',
                'no_tlp',
                'no_ktp',
                'alamat',
                'pasien_id'
            ];
}