<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PersalinanTerakhirModel extends Model
{
    protected $fillable = [
        'jenis',
    ];
}