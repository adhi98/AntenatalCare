<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PemeriksaanKehamilanPertama extends Model
{
    protected $fillable = [
        'pasien_id',
        'kontrasepsi',
        'riwayat_penyakit',
        'riwayat_alergi',
        'kek_id',
        'jumlah_kehamilan',
        'jumlah_persalinan',
        'jumlah_keguguran',
        'jumlah_anak_hidup',
        'jumlah_anak_lahir_mati',
        'jumlah_anak_lahir_prematur',
        'persalinan_terakhir',
        'imunisasi_TT',
    ];
}