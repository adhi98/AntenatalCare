<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LetakJaninModel extends Model
{
    protected $fillable = [
        'posisi',
    ];
}