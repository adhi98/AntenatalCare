<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class HasilLab extends Model
{
    protected $fillable = [
        'pemeriksaan_kehamilan_id',
        'anemia',
        'hiv',
        'sivilis'
    ];
}