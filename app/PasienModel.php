<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Tymon\JWTAuth\Contracts\JWTSubject;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class PasienModel extends Authenticatable implements JWTSubject
{    

    use Notifiable;

    protected $fillable = [
        'nama',
        'tanggal_lahir',
        'agama_id',
        'bidan_id',
        'golongan_darah',
        'pekerjaan_id',
        'no_tlp',
        'no_ktp',
        'no_jkn',
        'tinggi_badan',
        'no_pasien',
        'password',
        'alamat',
        'tgl_mens_terakhir',
        'tgl_perkiraan_melahirkan',
        'status',        
    ];

    protected $dates = [
        'tgl_mens_terakhir',
        'tgl_perkiraan_melahirkan',
    ];

    public function getJWTIdentifier()
    {
        return $this->getKey(); 
    }

    public function getJWTCustomClaims()
    {
        return [];
    }
    
    public function diagnosas_pasien()
    {
        return $this->hasMany('App\DiagnosaModel');
    }
}