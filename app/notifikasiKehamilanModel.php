<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class notifikasiKehamilanModel extends Model
{
    protected $fillable= [
        'pemeriksan_kehamilan_id',
        'pasien_id',
    ];
}