<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Relasi extends Model
{
    protected $fillable =   [
        'kode_penyakit',
        'kode_gejala'
    ];

    public function penyakit_model(){
        return $this->belongsTo('App\PenyakitModel');
    }
}
