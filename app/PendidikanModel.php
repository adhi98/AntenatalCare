<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PendidikanModel extends Model
{
    protected $fillable = [
        'pendidikan'
    ];
}