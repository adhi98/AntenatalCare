<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PemeriksaanKehamilanModel extends Model
{
    protected $fillable = [
        'pasien_id',        
        'tanggal_pemeriksaan',
        'berat_badan',
        'tekanan_darah',
        'lingkar_lengan',
        'tinggi_rahim',
        'tanggal_kembali',
        'letak_janin',
        'denyut_jantung_janin',        
        'hasil_konsultasi',
        'keluhan',
        'status',
        'kaki_bengkak',
        'tindakan',        
    ];    
}