<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\PasienModel;
use App\WaliModel;
use App\User;
use App\AgamaModel;
use App\PekerjaanModel;
use App\PendidikanModel;
use App\SemesterModel;
use App\GolonganDarahModel;
use App\PemeriksaanKehamilanModel;
use App\PemeriksaanNifasModel;
use App\notifikasiKehamilanModel;
use App\notifikasiNifasModel;
use App\PenyakitModel;
use App\GejalaModel;
use App\Relasi;
use App\DiagnosaModel;
use JWTFactory;
use JWTAuth;
use Validator;
use Carbon\Carbon;



class PasienControllerAPI extends Controller
{

    public function login(Request $request){

        $validator = Validator::make($request->all(), [
                'no_pasien' => 'required',
                'password' => 'required',
        ]);

            $nopasien = $request->input('no_pasien');
            $password = $request->input('password');


            $data = PasienModel::select('pasien_models.id as id' , 'nama', 'tanggal_lahir', 'no_jkn', 'agama_models.agama as agama', 'darah', 'bidan_id', 'pekerjaan', 'bidans.name as bidan_nama', 'pasien_models.password')
            ->join('agama_models', 'pasien_models.agama_id', '=', 'agama_models.id')
            ->join('golongan_darah_models', 'pasien_models.golongan_darah', '=', 'golongan_darah_models.id')
            ->join('pekerjaan_models', 'pasien_models.pekerjaan_id', '=', 'pekerjaan_models.id')
            ->join('bidans', 'bidans.id', '=', 'pasien_models.bidan_id')
            ->where([
                'no_pasien' => $nopasien,
                'pasien_models.password' => $password
            ])->first();



            // converse Umur Ibu
            $parseTanggalLahir = Carbon::parse($data->tanggal_lahir);
            $parseTanggalSekarang = Carbon::today();

            $getYears = $parseTanggalLahir->diffInYears($parseTanggalSekarang);


            return response()->json([
                'code' =>200,
                'data' => $data,
                'umur' => $getYears,
            ]);

    }

    public function pemeriksaankehamilanbyId(Request $request){

        $id = $request->input('id');

        $data = PemeriksaanKehamilanModel::select('pemeriksaan_kehamilan_models.id as pkid','tanggal_pemeriksaan', 'hasil_konsultasi', 'semester')
        ->join('pasien_models', 'pasien_models.id', '=', 'pemeriksaan_kehamilan_models.pasien_id')
        ->join('semester_models', 'semester_models.id', '=', 'pemeriksaan_kehamilan_models.status')
        ->where('pasien_models.id', $id)
        ->orderBy('tanggal_pemeriksaan', 'DESC')
        ->get();

        return response()->json([
            'code' => 200,
            'data' => $data,
        ]);

    }

    public function detailpemeriksaankehamilanbyId(Request $request){
        $pkid = $request->input('pkid');


        $a = PemeriksaanKehamilanModel::select('*')
        ->join('semester_models', 'semester_models.id', '=', 'pemeriksaan_kehamilan_models.status')
        ->join('hasil_labs', 'hasil_labs.pemeriksaan_kehamilan_id', '=', 'pemeriksaan_kehamilan_models.id')
        ->join('letak_janin_models', 'letak_janin_models.id', '=', 'pemeriksaan_kehamilan_models.letak_janin')
        ->where([
            'pemeriksaan_kehamilan_models.id' => $pkid
        ])->first();


        // Get Umur Kehamilan pasien
        $getDatePasien = PemeriksaanKehamilanModel::select('tanggal_pemeriksaan', 'tgl_mens_terakhir')
        ->join('pasien_models', 'pasien_models.id', '=', 'pemeriksaan_kehamilan_models.pasien_id')->where([
            'pemeriksaan_kehamilan_models.id' => $pkid
        ])->first();

        $parseHpht = Carbon::parse($getDatePasien->tgl_mens_terakhir);
        $parseTanggalPemeriksaan = Carbon::parse($getDatePasien->tanggal_pemeriksaan);

        $getWeeks = $parseHpht->diffInWeeks($parseTanggalPemeriksaan);

        // Array Of Object

        $array[0] = [
            "title" => "Keluhan Sekarang",
            "image" => asset('assets/icon-mobile/icon-keluhan.png'),
            "value" => $a->keluhan,
            "type" => 2,
        ];

        $array[1] = [
            "title" => "Tekanan Darah",
            "image" => asset('assets/icon-mobile/icon-tekanandarah.png'),
            "value" => $a->tekanan_darah,
            "type" => 2,
        ];

        $array[2] = [
            "title" => "Berat Badan",
            "image" => asset('assets/icon-mobile/icon-beratbadan.png'),
            "value" => $a->berat_badan,
            "type" => 1,
        ];

        $array[3] = [
            "title" => "Tinggi Rahim",
            "image" => asset('assets/icon-mobile/icon-tinggirahim.png'),
            "value" => $a->tinggi_rahim,
            "type" => 1,
        ];

        $array[4] = [
            "title" => "lingkar Lengan Atas",
            "image" => asset('assets/icon-mobile/icon-lila.png'),
            "value" => $a->lingkar_lengan,
            "type" => 1,
        ];

        $array[5] = [
            "title" => "Denyut Jantung Janin",
            "image" => asset('assets/icon-mobile/icon-denyutjanin.png'),
            "value" => $a->denyut_jantung_janin,
            "type" => 2,
        ];

        $array[6] = [
            "title" => "Letak Janin",
            "image" => asset('assets/icon-mobile/icon-letakjanin.png'),
            "value" => $a->posisi,
            "type" => 2,
        ];

        $array[7] = [
            "title" => "Tindakan",
            "image" => asset('assets/icon-mobile/icon-keluhan.png'),
            "value" => $a->tindakan,
            "type" => 2,
        ];

        $array[8] = [
            "title" => "Hasil Lab",
            "image" => asset('assets/icon-mobile/icon-hasillab.png'),
            "value" => $a->anemia,
            "type" => 2,
        ];

        $array[9] = [
            "title" => "Tanggal Kembali",
            "image" => asset('assets/icon-mobile/icon-tanggalkonsultasi.png'),
            "value" => $a->tanggal_kembali,
            "type" => 2,
        ];


        $array[10] = [
            "title" => "Hasil Konsultasi",
            "image" => asset('assets/icon-mobile/hasilkonsultasinifas.png'),
            "value" => $a->hasil_konsultasi,
            "type" => 2,
        ];


        return response()->json([
            'code' => 200,
            'tanggal_pemeriksaan' => $a->tanggal_pemeriksaan,
            'trimester' => $a->semester,
            'umur' => $getWeeks,
            'data' => $array,
        ]);

    }

    // Grafik Berat Badan
    public function grafikBeratBadan(Request $request){
        $pasien_id = $request->input('pasien_id');
        $tanggal_pemeriksaan = $request->input('tanggal_pemeriksaan');

        // Cek Trimester Pasien, Berat Badan, dan Status
        $trimesterStatus = PemeriksaanKehamilanModel::
        select('status', 'semester', 'berat_badan')
        ->where('tanggal_pemeriksaan', $tanggal_pemeriksaan)
        ->join('semester_models', 'semester_models.id', '=',
         'pemeriksaan_kehamilan_models.status')
        ->first();

        // Get Data Grafik By Trimester dan Tanggal Pemeriksaan
        $dataGrafik = PemeriksaanKehamilanModel::select('pemeriksaan_kehamilan_models.status as status',
         'tanggal_pemeriksaan', 'berat_badan')
        ->join('semester_models', 'semester_models.id', '=', 'pemeriksaan_kehamilan_models.status')
        ->join('pasien_models', 'pasien_models.id', '=', 'pemeriksaan_kehamilan_models.pasien_id')
        ->where([
            ['pasien_id', '=', $pasien_id],
            ['tanggal_pemeriksaan', '<=', $tanggal_pemeriksaan]
            ])
        ->where('pemeriksaan_kehamilan_models.status', $trimesterStatus->status)
        ->get();


        return response()->json([
                'data' => $dataGrafik,
                'trimester' => $trimesterStatus->semester,
                'berat_badan' => $trimesterStatus->berat_badan,
        ]);
    }

    // Grafik Lila
    public function grafikLila(Request $request){
        $pasien_id = $request->input('pasien_id');
        $tanggal_pemeriksaan = $request->input('tanggal_pemeriksaan');

        // Cek Trimester Pasien, Berat Badan, dan Status
        $trimesterStatus = PemeriksaanKehamilanModel::select('status', 'semester', 'lingkar_lengan')
        ->where('tanggal_pemeriksaan', $tanggal_pemeriksaan)
        ->join('semester_models', 'semester_models.id', '=', 'pemeriksaan_kehamilan_models.status')
        ->first();

        // Get Data Grafik By Trimester dan Tanggal Pemeriksaan
        $dataGrafik = PemeriksaanKehamilanModel::select('pemeriksaan_kehamilan_models.status as status', 'tanggal_pemeriksaan', 'lingkar_lengan')
        ->join('semester_models', 'semester_models.id', '=', 'pemeriksaan_kehamilan_models.status')
        ->join('pasien_models', 'pasien_models.id', '=', 'pemeriksaan_kehamilan_models.pasien_id')
        ->where([
            ['pasien_id', '=', $pasien_id],
            ['tanggal_pemeriksaan', '<=', $tanggal_pemeriksaan]
            ])
        ->where('pemeriksaan_kehamilan_models.status', $trimesterStatus->status)
        ->get();

        return response()->json([
                'data' => $dataGrafik,
                'trimester' => $trimesterStatus->semester,
                'lingkar_lengan' => $trimesterStatus->lingkar_lengan,
        ]);
    }

    // Grafik Tinggi Rahim
    public function grafikTinggiRahim(Request $request){
        $pasien_id = $request->input('pasien_id');
        $tanggal_pemeriksaan = $request->input('tanggal_pemeriksaan');

        // Cek Trimester Pasien, Tinggi Rahim, dan Status
        $trimesterStatus = PemeriksaanKehamilanModel::select('status', 'semester', 'tinggi_rahim')
        ->where('tanggal_pemeriksaan', $tanggal_pemeriksaan)
        ->join('semester_models', 'semester_models.id', '=', 'pemeriksaan_kehamilan_models.status')
        ->first();

        // Get Data Grafik By Trimester dan Tanggal Pemeriksaan
        $dataGrafik = PemeriksaanKehamilanModel::select('pemeriksaan_kehamilan_models.status as status', 'tanggal_pemeriksaan', 'tinggi_rahim')
        ->join('semester_models', 'semester_models.id', '=', 'pemeriksaan_kehamilan_models.status')
        ->join('pasien_models', 'pasien_models.id', '=', 'pemeriksaan_kehamilan_models.pasien_id')
        ->where([
            ['pasien_id', '=', $pasien_id],
            ['tanggal_pemeriksaan', '<=', $tanggal_pemeriksaan]
            ])
        ->where('pemeriksaan_kehamilan_models.status', $trimesterStatus->status)
        ->get();

        return response()->json([
                'data' => $dataGrafik,
                'trimester' => $trimesterStatus->semester,
                'tinggi_rahim' => $trimesterStatus->tinggi_rahim,
        ]);
    }



    public function updatePassword(Request $request){
        $pasien_id = $request->input('pasien_id');
        $password = $request->input('password');

        $updatePassword = PasienModel::where('id', $pasien_id)->update([
            'password' => $password
        ]);

        return response()->json([
                'data' => $updatePassword,
        ]);
    }

    public function umurKehamilanbyId(Request $request){
        $pasien_id = $request->input('pasien_id');
        $dateNow = $request->input('tanggal_sekarang');

        $getDatePasien = PasienModel::select('tgl_mens_terakhir', 'tgl_perkiraan_melahirkan', 'status')->where('id', $pasien_id)
        ->first();

        // Parse Date
        $parseHpht = Carbon::parse($getDatePasien->tgl_mens_terakhir);
        $parseHPL = Carbon::parse($getDatePasien->tgl_perkiraan_melahirkan);
        $parseDatenow = Carbon::parse($dateNow);

        $getWeeks = $parseHpht->diffInWeeks($parseDatenow);

        // Get Semester Pasien
        $semester = '';
        $perkembanganBayi = [];
        if($getWeeks < 14){
            $semester = 1;
            $perkembanganBayi[0] = [
                "umur" => "1 - 6 Minggu Umur Bayi",
                "ukuran" => "Biji Delima",
                "isi" => "Minggu Ini merupakan periode Pra-mebrionik dan Embrionik. Pada periode pra-embrionik hasil pembuahan akan tumbuh menjadi zigot dan mnyusun sel- kehidupan. Periode embrionik berkembang  menjadi 3 tahapan yaitu : Endoderm, Mesoderm dan Ectoderm.  Pada fase embrionic akan dimulai pembentukan lapisan organ dalam manusia. pada saat umur kehamilan memasuki 6 minggu, ukuran janin memiliki berat rata-rata 0,3 gram dengan panjang 0,8 cm menyerupai ukuran biji delima.",
                "gambar" => asset('assets/icon-mobile/delima.png'),
            ];
            $perkembanganBayi[1] = [
                "umur" => "7 - 9 Minggu Umur Bayi",
                "ukuran" => "Buah Leci",
                "isi" => "Pada minggu ini masih merupakan tahapan periode Embrionic dan sudah memasuki tahapan Ectoderm. lapisan luar (kulit), sistem saraf dan penerima sensor seperti mata, hidung, telinga mulai terbentuk. apabila seluruh tahapan lapisan sudah terbentuk, maka sistem kehidupan embrio menjadi matang dan berkembang lebih cepat. pada saat umur kehamilan memasuki 9 minggu, Ukuran Janin memiliki berat rata-rata 1,9 gram dengan panjang 2,2 cm.",
                "gambar" => asset('assets/icon-mobile/leci.png'),
            ];
            $perkembanganBayi[2] = [
                "umur" => "10 - 12 Minggu Umur Bayi",
                "ukuran" => "Buah Plum",
                "isi" => "Pada awal minggu ke-9 sampai minggu ke-28/ (7 bulan) merupakan periode Fetal. Yaitu periode peekembangan organ tubuh janin.  Pada saat umur kehamilan memasuki minggu ke 12, Ukuran Janin memiliki berat rata-rata 1,9 gram dengan panjang 2,2 cm. menyerupai buah plum",
                "gambar" => asset('assets/icon-mobile/plum.png'),
            ];

        }
        if($getWeeks >14 && $getWeeks < 27) {
            $semester = 2;
            $perkembanganBayi[0] = [
                "umur" => "13 - 16 Minggu Umur Bayi",
                "ukuran" => "Buah Alpukat",
                "isi" => "Memasuki minggu ke 16, janin sudah dapat melakukan gerakan-gerakan pertama di dalam rahim. Terjadi peningkatan jumlah sel saraf secara drastis. sebagian besar organ yang dimiliki janin sudah mulai matang secara sempurna. Pada minggu ini ukuran janin memiliki berat rata-rata 100gram dengan panjang sekitar 12cm.",
                "gambar" => asset('assets/icon-mobile/alpukat.png'),
            ];
            $perkembanganBayi[1] = [
                "umur" => "17 - 20 Minggu Umur Bayi",
                "ukuran" => "Buah Pisang",
                "isi" => "Rentang antara minggu ke 17 - 20, pergerakan janin sudah mulai aktif. Indra Penciuman, perasa, pendengaran semakin membaik. Lapisan verniks caseosa sudah mulai terbentuk, adapun fungsi dari lapisan ini adalah untuk melindungi kulit janin dari cairan amnion. ",
                "gambar" => asset('assets/icon-mobile/pisang.png'),
            ];
            $perkembanganBayi[2] = [
                "umur" => "21 - 24 Minggu Umur Bayi",
                "ukuran" => "Buah Melon",
                "isi" => "Pada akhir minggu ke 24 perkembangan janin semakin meningkat ditandai dengan meningkatnya fungsi organ paru-paru. fungsi Indra janin sudah matang secara sempurna. pada akhir minggu ke-24 janin memiliki berat sebesar 600 gram dengan panjang 30 cm.",
                "gambar" => asset('assets/icon-mobile/melon.png'),
            ];
        }
        if($getWeeks > 27){
            $semester = 3;
            $perkembanganBayi[0] = [
                "umur" => "25 - 28 Minggu Umur Bayi",
                "ukuran" => "Sayur Terong",
                "isi" => "Memasuki umur kehamilan ke 27 perkembangan janin semakin sempurna ditandai dengan meningkatnya organ sensorik . ukuran janin dapat mencapai berat 1kg dengan panjang 38cm. pada rentang minggu ini otakbayi berkembang dengan cepat.",
                "gambar" => asset('assets/icon-mobile/alpukat.png'),
            ];
            $perkembanganBayi[1] = [
                "umur" => "28 - 32 Minggu Umur Bayi",
                "ukuran" => "Memasuki umur ke 32, sistem kekebalan tubuh janin mulai terbentuk. perkembangan janin didalam rahim mulai berkurang dikarenakan ukuran janin yang semakin besar. pada minggu ke 32 ukuran janin dapat mencapai berat 1,7kg dan panjang 42,4 cm.",
                "isi" => "a",
                "gambar" => asset('assets/icon-mobile/terong.png'),
            ];
            $perkembanganBayi[2] = [
                "umur" => "33 - 36 Minggu Umur Bayi",
                "ukuran" => "Buah Pepaya",
                "isi" => "memasuki umur ke 36 pendengaran janin sudah berfungsi secara sempurna, pada minggu ini janin sudah memasuki panggul. pada minggu ke 36 ukuran janin dapat mencapai berat 2,6kg dan panjang 47,4 cm.",
                "gambar" => asset('assets/icon-mobile/pepaya.png'),
            ];
            $perkembanganBayi[3] = [
                "umur" => "37 - 40 Minggu Umur Bayi",
                "ukuran" => "Buah Semangka",
                "isi" => "Memasuki tahapan akhir sebelum memasui persalinan, perkembangan berat janin sudah mencapai 3,5kg dengan panjang 50,8 cm. pada minggu ini umumnya bayi sudah siap untuk dilahirkan ke dunia. Hanya menunggu perkiraan HPL (Hari Perkiraan Lahir) yang sudah ditentukan.",
                "gambar" => asset('assets/icon-mobile/semangka.png'),
            ];

        }

        if($getWeeks > 40){
            return response()->json([
                'code' => 200,
                'data' => 40,
                'semester' => $semester,
                'status' => $getDatePasien->status,
                'perkembanganBayi' => $perkembanganBayi,
                'perkembanganBayiHead' => $perkembanganBayi[0],
            ]);
        }else {
            return response()->json([
                'code' => 200,
                'data' => $getWeeks,
                'semester' => $semester,
                'status' => $getDatePasien->status,
                'perkembanganBayi' => $perkembanganBayi,
                'perkembanganBayiHead' => $perkembanganBayi[0],
            ]);
        }

    }

    public function notifikasiKehamilan(Request $request){
        $pasien_id = $request->input('pasien_id');

        $NotifikasiKehamilan = PemeriksaanKehamilanModel::select('tanggal_kembali', 'tanggal_pemeriksaan')
        ->join('notifikasi_kehamilan_models', 'notifikasi_kehamilan_models.pasien_id', '=', 'pemeriksaan_kehamilan_models.pasien_id')
        ->where('notifikasi_kehamilan_models.pasien_id', $pasien_id)
        ->orderBy('tanggal_pemeriksaan', 'DESC')
        ->distinct()
        ->get();


        return response()->json([
            'code' => 200,
            'data' => $NotifikasiKehamilan,
        ]);
    }

    public function notifikasiNifas(Request $request){

        $pasien_id = $request->input('pasien_id');

        $NotifikasiNifas = PemeriksaanNifasModel::select('tgl_pemeriksaan as tanggal_pemeriksaan', 'tgl_kembali as tanggal_kembali')
        ->join('notifikasi_nifas_models', 'notifikasi_nifas_models.pasien_id', '=', 'pemeriksaan_nifas_models.pasien_id')
        ->where('notifikasi_nifas_models.pasien_id', $pasien_id)
        ->orderBy('tgl_kembali', 'DESC')
        ->distinct()
        ->get();

        return response()->json([
            'code' => 200,
            'data' => $NotifikasiNifas,
        ]);

    }

    public function pemeriksaannifasbyId(Request $request){
        //List Nifas Mau di uji Coba dulu.
        $id = $request->input('id');

        $data = PemeriksaanNifasModel::select('pemeriksaan_nifas_models.id as id','tgl_pemeriksaan as tanggal_pemeriksaan', 'nasihat', 'pemeriksaan_nifas_models.status')
        ->join('pasien_models', 'pasien_models.id', '=', 'pemeriksaan_nifas_models.pasien_id')
        ->where('pasien_models.id', $id)
        ->orderBy('tanggal_pemeriksaan', 'DESC')
        ->get();

        $countdata = PemeriksaanNifasModel::where('pasien_models.id', $id)
        ->join('pasien_models', 'pasien_models.id', '=', 'pemeriksaan_nifas_models.pasien_id')
        ->count();


        $dataKesimpulan = PemeriksaanNifasModel::select('pemeriksaan_nifas_models.id as id','tgl_pemeriksaan as tanggal_pemeriksaan', 'keadaan as nasihat', 'pasien_models.status')
        ->join('pasien_models', 'pasien_models.id', '=', 'pemeriksaan_nifas_models.pasien_id')
        ->join('kesimpulan_nifas_models', 'pemeriksaan_nifas_models.id', '=', 'kesimpulan_nifas_models.pemeriksaan_nifas_id')
        ->where('pasien_models.id', $id)
        ->orderBy('tanggal_pemeriksaan', 'DESC')
        ->get();



        $arrayMerge = array_merge($dataKesimpulan->toArray(),$data->toArray());
        //dd($arrayMerge);


        if($countdata == 3){
            return response()->json([
            'code' => 200,
            'data' => $arrayMerge,
            ]);

        }else {
            return response()->json([
            'code' => 200,
            'data' => $data,
            ]);
        }
    }

    public function detailpemeriksaannifasbyId(Request $request){
        $id = $request->input('id');

        $a = PemeriksaanNifasModel::select('*')->where('id', $id)->first();


        // Array Object
        $array[0] = [
            "title" => "Kondisi Lokhia",
            "image" => asset('assets/icon-mobile/lokhianifas.png'),
            "value" => $a->lokhia,
            "type" => 2,
        ];

        $array[1] = [
            "title" => "Kondisi Perinium/Jalan Lahir",
            "image" => asset('assets/icon-mobile/periniumnifas.png'),
            "value" => $a->jalan_lahir,
            "type" => 2,
        ];

        $array[2] = [
            "title" => "Kontraksi Rahim",
            "image" => asset('assets/icon-mobile/kontraksirahimnifas.png'),
            "value" => $a->tekanan_darah,
            "type" => 1,
        ];

        $array[3] = [
            "title" => "Kondisi Payudara",
            "image" => asset('assets/icon-mobile/payudaranifas.png'),
            "value" => $a->pemeriksaan_payudara,
            "type" => 1,
        ];

        $array[4] = [
            "title" => "Nasihat Hasil Konsultasi",
            "image" => asset('assets/icon-mobile/hasilkonsultasinifas.png'),
            "value" => $a->nasihat,
            "type" => 1,
        ];


        return response()->json([
            'code' => 200,
            'data' => $array,
            'status' => $a->status,
            'tanggal_pemeriksaan' => $a->tgl_pemeriksaan,
        ]);

    }

    public function konsultasiNifasbyId(Request $request){
        $id = $request->input('id');

        $konsultasiNifas = PemeriksaanNifasModel::where('pasien_id', $id)->count();

        $keterangan = '';
        $perkembanganKonsultasiNifas = [];
        if($konsultasiNifas == 1){
            $keterangan = '6 Jam - 3 Hari';

            $perkembanganKonsultasiNifas[0] = [
                "umur" => "Tips Perawatan Selama Periode Nifas",
                "ukuran" => "1. Makan makanan yang bergizi tinggi.",
                "isi" => "Makan makanan yang mengandung karbohidrad, protein hewni, protein nabati, sayur dan buah-buahan. Makanan yangs sehat sangat diperlukan untuk meningkatkan metobolisme tubuh.",
                "gambar" => asset('assets/icon-mobile/icon-keluhan.png'),
            ];

            $perkembanganKonsultasiNifas[1] = [
                "umur" => "Tips Perawatan Selama Periode Nifas",
                "ukuran" => "2. Menjaga Kebutuhan Air Minum",
                "isi" => "Kebutuhan air minum pada ibu menyusui pada 6 bulan pertama adalah 14 gelas sehari dan pada 6 bulan kedua adalah 12 gelas sehari.",
                "gambar" => asset('assets/icon-mobile/icon-keluhan.png'),
            ];
            $perkembanganKonsultasiNifas[2] = [
                "umur" => "Tips Perawatan Selama Periode Nifas",
                "ukuran" => "3. Menjaga Kebersihan dan Kesehatan",
                "isi" => "Menjaga Kebersihan diri, termasuk kebersihan daerah kemaluan, dengan cara mengganti pembalut sesering mungkin.",
                "gambar" => asset('assets/icon-mobile/icon-keluhan.png'),
            ];
            $perkembanganKonsultasiNifas[3] = [
                "umur" => "Tips Perawatan Selama Periode Nifas",
                "ukuran" => "4. Rutin Membersihakan Bekas Luka",
                "isi" => "Bagi Ibu yang melahirkan dengan cara operasi caesar maka harus menjaga kebersihan luka bekas operasi secara teratur.",
                "gambar" => asset('assets/icon-mobile/icon-keluhan.png'),
            ];

        }else if($konsultasiNifas == 2){
            $keterangan = '4 - 28 Hari';
            $perkembanganKonsultasiNifas[0] = [
                "umur" => "Hal-Hal yang Harus dihindari Selama fase Nifas",
                "ukuran" => "1. Membuang Kolostrum",
                "isi" => "Membuang Asi yang pertama keluan (kolustrum) karena sangat berguna untuk kekebalan tubuh anak.",
                "gambar" => asset('assets/icon-mobile/icon-keluhan.png'),
            ];
            $perkembanganKonsultasiNifas[1] = [
                "umur" => "Hal-Hal yang Harus dihindari Selama fase Nifas",
                "ukuran" => "2. Membersihkan dengan Hal yang Dilarang",
                "isi" => "Membersihkan payudara dengan alkohol / povidon / iodine / obat merah atau sabun sangat tidak dianjurkan karena dapat terminum oleh bayi.",
                "gambar" => asset('assets/icon-mobile/icon-keluhan.png'),
            ];
            $perkembanganKonsultasiNifas[2] = [
                "umur" => "Hal-Hal yang Harus dihindari Selama fase Nifas",
                "ukuran" => "2. Membersihkan dengan Hal yang Dilarang",
                "isi" => "Membersihkan payudara dengan alkohol / povidon / iodine / obat merah atau sabun sangat tidak dianjurkan karena dapat terminum oleh bayi.",
                "gambar" => asset('assets/icon-mobile/icon-keluhan.png'),
            ];
        }else {
            $keterangan = '29 - 42 Hari';

            $perkembanganKonsultasiNifas[0] = [
                "umur" => "Keluarga Berencana",
                "ukuran" => "1. Pentingnya Mengikuti Program KB",
                "isi" => "Salah satu Tujuan utama ibu untuk mengikuti program KB adalah untuk mengatur jarak kehamilan dan mencegah kehamilan (minimal 2 tahun setelah melahirkan). Menjaga kesehatan Ibu, Bayi, dan Balita. Ibu dapat meemiliki waktu yang cukup untuk menjaga kesehatan dirinya dan bayi setalah melahirkan.",
                "gambar" => asset('assets/icon-mobile/icon-keluhan.png'),
            ];
            $perkembanganKonsultasiNifas[1] = [
                "umur" => "Keluarga Berencana",
                "ukuran" => "2. Metode Kontrasepsi Jangka Panjang ",
                "isi" => "Membuang Asi yang pertama keluan (kolustrum) karena sangat berguna untuk kekebalan tubuh anak.",
                "gambar" => asset('assets/icon-mobile/icon-keluhan.png'),
            ];
            $perkembanganKonsultasiNifas[2] = [
                "umur" => "Keluarga Berencana",
                "ukuran" => "3. Metode Kontrasepsi Jangka Pendek",
                "isi" => "Membersihkan payudara dengan alkohol / povidon / iodine / obat merah atau sabun sangat tidak dianjurkan karena dapat terminum oleh bayi.",
                "gambar" => asset('assets/icon-mobile/icon-keluhan.png'),
            ];
        }


        return response()->json([
            'code' => 200,
            'jumlah_konsultasi' => $konsultasiNifas,
            'keterangan' => $keterangan,
            'perkembanganNifas' => $perkembanganKonsultasiNifas,
            'perkembanganNifasHead' => $perkembanganKonsultasiNifas[0],
        ]);
    }

    public function Biodata(Request $request){
        $id = $request->input('id');

        $data = PasienModel::select('pasien_models.id as id' , 'pasien_models.nama as nama',
         'pasien_models.tanggal_lahir', 'no_jkn', 'agama_models.agama as agama', 'darah',
          'pekerjaan', 'bidans.name as bidan_nama', 'jumlah_kehamilan')
            ->join('agama_models', 'pasien_models.agama_id', '=', 'agama_models.id')
            ->join('golongan_darah_models', 'pasien_models.golongan_darah', '=', 'golongan_darah_models.id')
            ->join('pekerjaan_models', 'pasien_models.pekerjaan_id', '=', 'pekerjaan_models.id')
            ->join('bidans', 'bidans.id', '=', 'pasien_models.bidan_id')
            ->join('pemeriksaan_kehamilan_pertamas', 'pasien_models.id', '=', 'pemeriksaan_kehamilan_pertamas.pasien_id')
            ->where([
                'pasien_models.id' => $id,
            ])->first();

            $datawali = WaliModel::select('nama', 'no_tlp', 'alamat', 'pekerjaan', 'pendidikan')
            ->join('pekerjaan_models', 'wali_models.pekerjaan_id', '=', 'pekerjaan_models.id')
            ->join('pendidikan_models', 'wali_models.pendidikan_id', '=', 'pendidikan_models.id')
            ->where('pasien_id', $id)
            ->first();

        return response()->json([
            'code' => 200,
            'datapasien' => $data,
            'datawali' => $datawali
        ]);
    }

    public function ListPenyakit(Request $request){
        $jenis = $request->input('jenis');

        if($jenis == 'Nifas'){
            $dataPenyakit = PenyakitModel::select('*')->where('fase', 'Nifas')->get();
        }else{
            $dataPenyakit = PenyakitModel::select('*')->where('fase', 'Kehamilan')->get();
        }

        return response()->json([
            'code' => 200,
            'list' => $dataPenyakit,
        ]);

    }

    public function DetailPenyakit(Request $request){
        $kode_penyakit = $request->input('kode_penyakit');

            $Detailpenyakit = Relasi::select('penyakit_models.nama as nama_penyakit', 'definisi', 'faktor', 'pencegahan_umum', 'pencegahan_khusus', 'fase')
            ->join('penyakit_models', 'relasis.kode_penyakit', '=', 'penyakit_models.kode_penyakit')
            ->where('penyakit_models.kode_penyakit', $kode_penyakit)
            ->first();

            $gejalaModel = Relasi::select('gejala_models.nama as gejala')
            ->join('gejala_models', 'relasis.kode_gejala', '=', 'gejala_models.kode_gejala')
            ->where('kode_penyakit', $kode_penyakit)
            ->get();

        return response()->json([
            'code' => 200,
            'data' => $Detailpenyakit,
            'gejala_model' => $gejalaModel,
        ]);
    }




    public function test() {

        //List Nifas Mau di uji Coba dulu.
        $id = 1;

        $data = PemeriksaanNifasModel::select('pemeriksaan_nifas_models.id as id','tgl_pemeriksaan as tanggal_pemeriksaan', 'nasihat', 'pemeriksaan_nifas_models.status')
        ->join('pasien_models', 'pasien_models.id', '=', 'pemeriksaan_nifas_models.pasien_id')
        ->where('pasien_models.id', $id)
        ->orderBy('tanggal_pemeriksaan', 'DESC')
        ->get();

        $countdata = PemeriksaanNifasModel::where('pasien_models.id', $id)
        ->join('pasien_models', 'pasien_models.id', '=', 'pemeriksaan_nifas_models.pasien_id')
        ->count();


        $dataKesimpulan = PemeriksaanNifasModel::select('pemeriksaan_nifas_models.id as id','tgl_pemeriksaan as tanggal_pemeriksaan', 'keadaan as nasihat', 'pasien_models.status')
        ->join('pasien_models', 'pasien_models.id', '=', 'pemeriksaan_nifas_models.pasien_id')
        ->join('kesimpulan_nifas_models', 'pemeriksaan_nifas_models.id', '=', 'kesimpulan_nifas_models.pemeriksaan_nifas_id')
        ->where('pasien_models.id', $id)
        ->orderBy('tanggal_pemeriksaan', 'DESC')
        ->get();



        $arrayMerge = array_merge($dataKesimpulan->toArray(),$data->toArray());
        //dd($arrayMerge);


        if($countdata == 3){
            return response()->json([
            'code' => 200,
            'data' => $arrayMerge,
            ]);

        }else {
            return response()->json([
            'code' => 200,
            'data' => $data,
            ]);
        }
    }



    public function hasil_pemeriksaan_diagnosa(Request $request){
        $hasil_diagnosa = $request->input('hasil_diagnosa');
        $id_diagnosa = $request->input('id_diagnosa');

        $hasil_pemeriksaan_diagnosa = DiagnosaModel::select('tanggal', 'nama', 'pencegahan_umum', 'pencegahan_khusus', 'faktor', 'definisi')
        ->join('penyakit_models', 'diagnosa_models.kode_penyakit', '=', 'penyakit_models.kode_penyakit')
        ->where('id', $id_diagnosa)
        ->first();

        $gejala = Relasi::select('gejala_models.nama as nama_gejala')
        ->join('gejala_models', 'relasis.kode_gejala', '=', 'gejala_models.kode_gejala')
        ->where('kode_penyakit', $hasil_diagnosa)
        ->get();

        return response()->json([
            'code' =>200,
            'data' => $hasil_pemeriksaan_diagnosa,
            'gejala_model' => $gejala
        ]);
    }

    public function riwayat(Request $request)
    {
        $id_pasien = $request->input('id_pasien');

            $riwayat = DiagnosaModel::select('diagnosa_models.id as id', 'penyakit_models.nama as nama_penyakit', 'tanggal', 'diagnosa_models.status as status', 'pencegahan_umum', 'pencegahan_khusus', 'fase', 'penyakit_models.kode_penyakit')
            ->join('pasien_models', 'diagnosa_models.id_pasien', '=', 'pasien_models.id')
            ->join('penyakit_models', 'diagnosa_models.kode_penyakit', '=', 'penyakit_models.kode_penyakit')
            ->where('pasien_models.id', $id_pasien)
            ->where('diagnosa_models.status', 1)
            ->get();

        return response()->json([
            'code' =>200,
            'data' =>$riwayat
        ]);
    }
}
