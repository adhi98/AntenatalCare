<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Relasi;
use App\PenyakitModel;
use App\GejalaModel;
use App\DiagnosaModel;

class RelasiControllerAPI extends Controller
{
    public function index()
    {
        
    }

    public function proses(Request $request)
    {
        $id_pasien = $request->input('id_pasien');
        // $tanggal = $request->input('tgl_diagnosa');

        if ($request->tipe == 'kehamilan') { //ketika milih fase kehamilan
            if ($request->pertanyaan == '0' && $request->jawaban == '0'){
                $gejala = GejalaModel::select('gejala_models.nama as nama_gejala', 'kode_gejala')
                ->where('kode_gejala','GK01')
                ->first();

                $jika_selesai['jika_selesai'] = false;
            }
            if ($request->pertanyaan == 'GK01' && $request->jawaban == 'true'){
                $gejala = GejalaModel::select('gejala_models.nama as nama_gejala', 'kode_gejala')
                ->where('kode_gejala','GK02')
                ->first();

                $jika_selesai['jika_selesai'] = false;
            }
            if ($request->pertanyaan == 'GK02' && $request->jawaban == 'true'){
                $gejala = GejalaModel::select('gejala_models.nama as nama_gejala', 'kode_gejala')
                ->where('kode_gejala','GK03')
                ->first();

                $jika_selesai['jika_selesai'] = false;
            }
            if ($request->pertanyaan == 'GK03' && $request->jawaban == 'true'){
                $gejala = GejalaModel::select('gejala_models.nama as nama_gejala', 'kode_gejala')
                ->where('kode_gejala','GK04')
                ->first();

                $jika_selesai['jika_selesai'] = false;
            }
            if ($request->pertanyaan == 'GK04' && $request->jawaban == 'true'){
                $gejala = GejalaModel::select('gejala_models.nama as nama_gejala', 'kode_gejala')
                ->where('kode_gejala','GK11')
                ->first();

                $jika_selesai['jika_selesai'] = false;
            }
            if ($request->pertanyaan == 'GK11' && $request->jawaban == 'true'){
                $gejala = GejalaModel::select('gejala_models.nama as nama_gejala', 'kode_gejala')
                ->where('kode_gejala','GK12')
                ->first();

                $jika_selesai['jika_selesai'] = false;
            }
            if ($request->pertanyaan == 'GK12' && $request->jawaban == 'true'){
                $hasil_diagnosa = Relasi::select('penyakit_models.kode_penyakit as kode_penyakit', 'penyakit_models.nama as nama_penyakit', 'pencegahan_umum', 'pencegahan_khusus')
                ->join('penyakit_models', 'relasis.kode_penyakit', '=', 'penyakit_models.kode_penyakit')
                ->where('penyakit_models.kode_penyakit','PK03')
                ->first();

                $jika_selesai['jika_selesai'] = true;
                
            }
            if ($request->pertanyaan == 'GK10' && $request->jawaban == 'true'){
                $hasil_diagnosa = Relasi::select('penyakit_models.kode_penyakit as kode_penyakit', 'penyakit_models.nama as nama_penyakit', 'pencegahan_umum', 'pencegahan_khusus')
                ->join('penyakit_models', 'relasis.kode_penyakit', '=', 'penyakit_models.kode_penyakit')
                ->where('penyakit_models.kode_penyakit','PK02')
                ->first();

                $jika_selesai['jika_selesai'] = true;
                
            }
            if ($request->pertanyaan == 'GK17' && $request->jawaban == 'true'){
                $gejala = GejalaModel::select('gejala_models.nama as nama_gejala', 'kode_gejala')
                ->where('kode_gejala','GK18')
                ->first();

                $jika_selesai['jika_selesai'] = false;
            }
            if ($request->pertanyaan == 'GK18' && $request->jawaban == 'true'){
                $gejala = GejalaModel::select('gejala_models.nama as nama_gejala', 'kode_gejala')
                ->where('kode_gejala','GK19')
                ->first();

                $jika_selesai['jika_selesai'] = false;
            }
            if ($request->pertanyaan == 'GK19' && $request->jawaban == 'true'){
                $gejala = GejalaModel::select('gejala_models.nama as nama_gejala', 'kode_gejala')
                ->where('kode_gejala','GK21')
                ->first();

                $jika_selesai['jika_selesai'] = false;
            }
            if ($request->pertanyaan == 'GK20' && $request->jawaban == 'true'){
                $gejala = GejalaModel::select('gejala_models.nama as nama_gejala', 'kode_gejala')
                ->where('kode_gejala','GK21')
                ->first();

                $jika_selesai['jika_selesai'] = false;
            }
            if ($request->pertanyaan == 'GK21' && $request->jawaban == 'true'){
                $gejala = GejalaModel::select('gejala_models.nama as nama_gejala', 'kode_gejala')
                ->where('kode_gejala','GK23')
                ->first();

                $jika_selesai['jika_selesai'] = false;
            }
            if ($request->pertanyaan == 'GK23' && $request->jawaban == 'true'){
                $gejala = GejalaModel::select('gejala_models.nama as nama_gejala', 'kode_gejala')
                ->where('kode_gejala','GK42')
                ->first();

                $jika_selesai['jika_selesai'] = false;
            }
            if ($request->pertanyaan == 'GK42' && $request->jawaban == 'true'){
                $gejala = GejalaModel::select('gejala_models.nama as nama_gejala', 'kode_gejala')
                ->where('kode_gejala','GK43')
                ->first();

                $jika_selesai['jika_selesai'] = false;
            }
            if ($request->pertanyaan == 'GK43' && $request->jawaban == 'true'){
                $gejala = GejalaModel::select('gejala_models.nama as nama_gejala', 'kode_gejala')
                ->where('kode_gejala','GK44')
                ->first();

                $jika_selesai['jika_selesai'] = false;
            }
            if ($request->pertanyaan == 'GK44' && $request->jawaban == 'true'){
                $hasil_diagnosa = Relasi::select('penyakit_models.kode_penyakit as kode_penyakit', 'penyakit_models.nama as nama_penyakit', 'pencegahan_umum', 'pencegahan_khusus')
                ->join('penyakit_models', 'relasis.kode_penyakit', '=', 'penyakit_models.kode_penyakit')
                ->where('penyakit_models.kode_penyakit','PK13')
                ->first();

                $jika_selesai['jika_selesai'] = true;
                
            }
            if ($request->pertanyaan == 'GK38' && $request->jawaban == 'true'){
                $gejala = GejalaModel::select('gejala_models.nama as nama_gejala', 'kode_gejala')
                ->where('kode_gejala','GK39')
                ->first();

                $jika_selesai['jika_selesai'] = false;
            }
            if ($request->pertanyaan == 'GK39' && $request->jawaban == 'true'){
                $gejala = GejalaModel::select('gejala_models.nama as nama_gejala', 'kode_gejala')
                ->where('kode_gejala','GK40')
                ->first();

                $jika_selesai['jika_selesai'] = false;
            }
            if ($request->pertanyaan == 'GK40' && $request->jawaban == 'true'){
                $gejala = GejalaModel::select('gejala_models.nama as nama_gejala', 'kode_gejala')
                ->where('kode_gejala','GK41')
                ->first();

                $jika_selesai['jika_selesai'] = false;
            }
            if ($request->pertanyaan == 'GK41' && $request->jawaban == 'true'){
                $hasil_diagnosa = Relasi::select('penyakit_models.kode_penyakit as kode_penyakit', 'penyakit_models.nama as nama_penyakit', 'pencegahan_umum', 'pencegahan_khusus')
                ->join('penyakit_models', 'relasis.kode_penyakit', '=', 'penyakit_models.kode_penyakit')
                ->where('penyakit_models.kode_penyakit','PK12')
                ->first();
                
                $jika_selesai['jika_selesai'] = true;
            }
            if ($request->pertanyaan == 'GK22' && $request->jawaban == 'true'){
                $hasil_diagnosa = Relasi::select('penyakit_models.kode_penyakit as kode_penyakit', 'penyakit_models.nama as nama_penyakit', 'pencegahan_umum', 'pencegahan_khusus')
                ->join('penyakit_models', 'relasis.kode_penyakit', '=', 'penyakit_models.kode_penyakit')
                ->where('penyakit_models.kode_penyakit','PK06')
                ->first();

                $jika_selesai['jika_selesai'] = true;
                
            }
            if ($request->pertanyaan == 'GK06' && $request->jawaban == 'true'){
                $gejala = GejalaModel::select('gejala_models.nama as nama_gejala', 'kode_gejala')
                ->where('kode_gejala','GK07')
                ->first();

                $jika_selesai['jika_selesai'] = false;
            }
            if ($request->pertanyaan == 'GK05' && $request->jawaban == 'true'){
                $gejala = GejalaModel::select('gejala_models.nama as nama_gejala', 'kode_gejala')
                ->where('kode_gejala','GK26')
                ->first();

                $jika_selesai['jika_selesai'] = false;
            }
            if ($request->pertanyaan == 'GK13' && $request->jawaban == 'true'){
                $gejala = GejalaModel::select('gejala_models.nama as nama_gejala', 'kode_gejala')
                ->where('kode_gejala','GK14')
                ->first();

                $jika_selesai['jika_selesai'] = false;
            }
            if ($request->pertanyaan == 'GK14' && $request->jawaban == 'true'){
                $gejala = GejalaModel::select('gejala_models.nama as nama_gejala', 'kode_gejala')
                ->where('kode_gejala','GK15')
                ->first();

                $jika_selesai['jika_selesai'] = false;
            }
            if ($request->pertanyaan == 'GK15' && $request->jawaban == 'true'){
                $gejala = GejalaModel::select('gejala_models.nama as nama_gejala', 'kode_gejala')
                ->where('kode_gejala','GK16')
                ->first();

                $jika_selesai['jika_selesai'] = false;
            }
            if ($request->pertanyaan == 'GK16' && $request->jawaban == 'true'){
                $gejala = GejalaModel::select('gejala_models.nama as nama_gejala', 'kode_gejala')
                ->where('kode_gejala','GK36')
                ->first();

                $jika_selesai['jika_selesai'] = false;
            }
            if ($request->pertanyaan == 'GK36' && $request->jawaban == 'true'){
                $hasil_diagnosa = Relasi::select('penyakit_models.kode_penyakit as kode_penyakit', 'penyakit_models.nama as nama_penyakit', 'pencegahan_umum', 'pencegahan_khusus')
                ->join('penyakit_models', 'relasis.kode_penyakit', '=', 'penyakit_models.kode_penyakit')
                ->where('penyakit_models.kode_penyakit','PK04')
                ->first();

                $jika_selesai['jika_selesai'] = true;
                
            }
            if ($request->pertanyaan == 'GK26' && $request->jawaban == 'true'){
                $gejala = GejalaModel::select('gejala_models.nama as nama_gejala', 'kode_gejala')
                ->where('kode_gejala','GK27')
                ->first();

                $jika_selesai['jika_selesai'] = false;
            }
            if ($request->pertanyaan == 'GK24' && $request->jawaban == 'true'){
                $gejala = GejalaModel::select('gejala_models.nama as nama_gejala', 'kode_gejala')
                ->where('kode_gejala','GK25')
                ->first();

                $jika_selesai['jika_selesai'] = false;
            }
            if ($request->pertanyaan == 'GK25' && $request->jawaban == 'true'){
                $hasil_diagnosa = Relasi::select('penyakit_models.kode_penyakit as kode_penyakit', 'penyakit_models.nama as nama_penyakit', 'pencegahan_umum', 'pencegahan_khusus')
                ->join('penyakit_models', 'relasis.kode_penyakit', '=', 'penyakit_models.kode_penyakit')
                ->where('penyakit_models.kode_penyakit','PK07')
                ->first();
                
                $jika_selesai['jika_selesai'] = true;
            }
            if ($request->pertanyaan == 'GK27' && $request->jawaban == 'true'){
                $gejala = GejalaModel::select('gejala_models.nama as nama_gejala', 'kode_gejala')
                ->where('kode_gejala','GK28')
                ->first();

                $jika_selesai['jika_selesai'] = false;
            }
            if ($request->pertanyaan == 'GK28' && $request->jawaban == 'true'){
                $gejala = GejalaModel::select('gejala_models.nama as nama_gejala', 'kode_gejala')
                ->where('kode_gejala','GK29')
                ->first();

                $jika_selesai['jika_selesai'] = false;
            }
            if ($request->pertanyaan == 'GK29' && $request->jawaban == 'true'){
                $gejala = GejalaModel::select('gejala_models.nama as nama_gejala', 'kode_gejala')
                ->where('kode_gejala','GK37')
                ->first();

                $jika_selesai['jika_selesai'] = false;
            }
            if ($request->pertanyaan == 'GK37' && $request->jawaban == 'true'){
                $hasil_diagnosa = Relasi::select('penyakit_models.kode_penyakit as kode_penyakit', 'penyakit_models.nama as nama_penyakit', 'pencegahan_umum', 'pencegahan_khusus')
                ->join('penyakit_models', 'relasis.kode_penyakit', '=', 'penyakit_models.kode_penyakit')
                ->where('penyakit_models.kode_penyakit','PK10')
                ->first();

                $jika_selesai['jika_selesai'] = true;
                
            }
            if ($request->pertanyaan == 'GK07' && $request->jawaban == 'true'){
                $gejala = GejalaModel::select('gejala_models.nama as nama_gejala', 'kode_gejala')
                ->where('kode_gejala','GK08')
                ->first();

                $jika_selesai['jika_selesai'] = false;
            }
            if ($request->pertanyaan == 'GK08' && $request->jawaban == 'true'){
                $gejala = GejalaModel::select('gejala_models.nama as nama_gejala', 'kode_gejala')
                ->where('kode_gejala','GK09')
                ->first();

                $jika_selesai['jika_selesai'] = false;
            }
            if ($request->pertanyaan == 'GK09' && $request->jawaban == 'true'){
                $hasil_diagnosa = Relasi::select('penyakit_models.kode_penyakit as kode_penyakit', 'penyakit_models.nama as nama_penyakit', 'pencegahan_umum', 'pencegahan_khusus')
                ->join('penyakit_models', 'relasis.kode_penyakit', '=', 'penyakit_models.kode_penyakit')
                ->where('penyakit_models.kode_penyakit','PK01')
                ->first();

                $jika_selesai['jika_selesai'] = true;
                
            }
            if ($request->pertanyaan == 'GK30' && $request->jawaban == 'true'){
                $gejala = GejalaModel::select('gejala_models.nama as nama_gejala', 'kode_gejala')
                ->where('kode_gejala','GK31')
                ->first();

                $jika_selesai['jika_selesai'] = false;
            }
            if ($request->pertanyaan == 'GK32' && $request->jawaban == 'true'){
                $gejala = GejalaModel::select('gejala_models.nama as nama_gejala', 'kode_gejala')
                ->where('kode_gejala','GK33')
                ->first();

                $jika_selesai['jika_selesai'] = false;
            }
            if ($request->pertanyaan == 'GK33' && $request->jawaban == 'true'){
                $gejala = GejalaModel::select('gejala_models.nama as nama_gejala', 'kode_gejala')
                ->where('kode_gejala','GK34')
                ->first();

                $jika_selesai['jika_selesai'] = false;
            }
            if ($request->pertanyaan == 'GK34' && $request->jawaban == 'true'){
                $gejala = GejalaModel::select('gejala_models.nama as nama_gejala', 'kode_gejala')
                ->where('kode_gejala','GK35')
                ->first();

                $jika_selesai['jika_selesai'] = false;
            }
            if ($request->pertanyaan == 'GK35' && $request->jawaban == 'true'){
                $hasil_diagnosa = Relasi::select('penyakit_models.kode_penyakit as kode_penyakit', 'penyakit_models.nama as nama_penyakit', 'pencegahan_umum', 'pencegahan_khusus')
                ->join('penyakit_models', 'relasis.kode_penyakit', '=', 'penyakit_models.kode_penyakit')
                ->where('penyakit_models.kode_penyakit','PK11')
                ->first();

                $jika_selesai['jika_selesai'] = true;
            }
            if ($request->pertanyaan == 'GK31' && $request->jawaban == 'true'){
                $gejala = GejalaModel::select('gejala_models.nama as nama_gejala', 'kode_gejala')
                ->where('kode_gejala','GK32')
                ->first();

                $jika_selesai['jika_selesai'] = false;
            }
            if ($request->pertanyaan == 'GK31' && $request->jawaban == 'false'){
                $hasil_diagnosa['nama_penyakit'] = "Penyakit belum terdata";
                $hasil_diagnosa['pencegahan_umum'] = "-";
                $hasil_diagnosa['pencegahan_khusus'] = "-";

                $jika_selesai['jika_selesai'] = true;
            }
            if ($request->pertanyaan == 'GK35' && $request->jawaban == 'false'){
                $hasil_diagnosa['nama_penyakit'] = "Penyakit belum terdata";
                $hasil_diagnosa['pencegahan_umum'] = "-";
                $hasil_diagnosa['pencegahan_khusus'] = "-";

                $jika_selesai['jika_selesai'] = true;
            }
            if ($request->pertanyaan == 'GK34' && $request->jawaban == 'false'){
                $hasil_diagnosa['nama_penyakit'] = "Penyakit belum terdata";
                $hasil_diagnosa['pencegahan_umum'] = "-";
                $hasil_diagnosa['pencegahan_khusus'] = "-";

                $jika_selesai['jika_selesai'] = true;
            }
            if ($request->pertanyaan == 'GK33' && $request->jawaban == 'false'){
                $hasil_diagnosa['nama_penyakit'] = "Penyakit belum terdata";
                $hasil_diagnosa['pencegahan_umum'] = "-";
                $hasil_diagnosa['pencegahan_khusus'] = "-";

                $jika_selesai['jika_selesai'] = true;
            }
            if ($request->pertanyaan == 'GK32' && $request->jawaban == 'false'){
                $hasil_diagnosa['nama_penyakit'] = "Penyakit belum terdata";
                $hasil_diagnosa['pencegahan_umum'] = "-";
                $hasil_diagnosa['pencegahan_khusus'] = "-";

                $jika_selesai['jika_selesai'] = true;
            }
            if ($request->pertanyaan == 'GK30' && $request->jawaban == 'false'){
                $gejala = GejalaModel::select('gejala_models.nama as nama_gejala', 'kode_gejala')
                ->where('kode_gejala','GK02')
                ->first();

                $jika_selesai['jika_selesai'] = false;
            }
            if ($request->pertanyaan == 'GK09' && $request->jawaban == 'false'){
                $hasil_diagnosa['nama_penyakit'] = "Penyakit belum terdata";
                $hasil_diagnosa['pencegahan_umum'] = "-";
                $hasil_diagnosa['pencegahan_khusus'] = "-";

                $jika_selesai['jika_selesai'] = true;
                
            }
            if ($request->pertanyaan == 'GK08' && $request->jawaban == 'false'){
                $hasil_diagnosa['nama_penyakit'] = "Penyakit belum terdata";
                $hasil_diagnosa['pencegahan_umum'] = "-";
                $hasil_diagnosa['pencegahan_khusus'] = "-";

                $jika_selesai['jika_selesai'] = true;
                
            }
            if ($request->pertanyaan == 'GK07' && $request->jawaban == 'false'){
                $hasil_diagnosa['nama_penyakit'] = "Penyakit belum terdata";
                $hasil_diagnosa['pencegahan_umum'] = "-";
                $hasil_diagnosa['pencegahan_khusus'] = "-";

                $jika_selesai['jika_selesai'] = true;
                
            }
            if ($request->pertanyaan == 'GK37' && $request->jawaban == 'false'){
                $hasil_diagnosa = Relasi::select('penyakit_models.kode_penyakit as kode_penyakit', 'penyakit_models.nama as nama_penyakit', 'pencegahan_umum', 'pencegahan_khusus')
                ->join('penyakit_models', 'relasis.kode_penyakit', '=', 'penyakit_models.kode_penyakit')
                ->where('penyakit_models.kode_penyakit','PK09')
                ->first();

                $jika_selesai['jika_selesai'] = true;
                
            }
            if ($request->pertanyaan == 'GK29' && $request->jawaban == 'false'){
                $hasil_diagnosa['nama_penyakit'] = "Penyakit belum terdata";
                $hasil_diagnosa['pencegahan_umum'] = "-";
                $hasil_diagnosa['pencegahan_khusus'] = "-";

                $jika_selesai['jika_selesai'] = true;
                
            }
            if ($request->pertanyaan == 'GK28' && $request->jawaban == 'false'){
                $hasil_diagnosa['nama_penyakit'] = "Penyakit belum terdata";
                $hasil_diagnosa['pencegahan_umum'] = "-";
                $hasil_diagnosa['pencegahan_khusus'] = "-";

                $jika_selesai['jika_selesai'] = true;
                
            }
            if ($request->pertanyaan == 'GK27' && $request->jawaban == 'false'){
                $hasil_diagnosa['nama_penyakit'] = "Penyakit belum terdata";
                $hasil_diagnosa['pencegahan_umum'] = "-";
                $hasil_diagnosa['pencegahan_khusus'] = "-";

                $jika_selesai['jika_selesai'] = true;
                
            }
            if ($request->pertanyaan == 'GK25' && $request->jawaban == 'false'){
                $hasil_diagnosa = Relasi::select('penyakit_models.kode_penyakit as kode_penyakit', 'penyakit_models.nama as nama_penyakit', 'pencegahan_umum', 'pencegahan_khusus')
                ->join('penyakit_models', 'relasis.kode_penyakit', '=', 'penyakit_models.kode_penyakit')
                ->where('penyakit_models.kode_penyakit','PK08')
                ->first();

                $jika_selesai['jika_selesai'] = true;
                
            }
            if ($request->pertanyaan == 'GK24' && $request->jawaban == 'false'){
                $hasil_diagnosa['nama_penyakit'] = "Penyakit belum terdata";
                $hasil_diagnosa['pencegahan_umum'] = "-";
                $hasil_diagnosa['pencegahan_khusus'] = "-";

                $jika_selesai['jika_selesai'] = true;
                
            }
            if ($request->pertanyaan == 'GK26' && $request->jawaban == 'false'){
                $gejala = GejalaModel::select('gejala_models.nama as nama_gejala', 'kode_gejala')
                ->where('kode_gejala','GK24')
                ->first();

                $jika_selesai['jika_selesai'] = false;
            }
            if ($request->pertanyaan == 'GK36' && $request->jawaban == 'false'){
                $hasil_diagnosa['nama_penyakit'] = "Penyakit belum terdata";
                $hasil_diagnosa['pencegahan_umum'] = "-";
                $hasil_diagnosa['pencegahan_khusus'] = "-";

                $jika_selesai['jika_selesai'] = true;
                
            }
            if ($request->pertanyaan == 'GK16' && $request->jawaban == 'false'){
                $hasil_diagnosa['nama_penyakit'] = "Penyakit belum terdata";
                $hasil_diagnosa['pencegahan_umum'] = "-";
                $hasil_diagnosa['pencegahan_khusus'] = "-";

                $jika_selesai['jika_selesai'] = true;
                
            }
            if ($request->pertanyaan == 'GK15' && $request->jawaban == 'false'){
                $hasil_diagnosa['nama_penyakit'] = "Penyakit belum terdata";
                $hasil_diagnosa['pencegahan_umum'] = "-";
                $hasil_diagnosa['pencegahan_khusus'] = "-";

                $jika_selesai['jika_selesai'] = true;
                
            }
            if ($request->pertanyaan == 'GK14' && $request->jawaban == 'false'){
                $hasil_diagnosa['nama_penyakit'] = "Penyakit belum terdata";
                $hasil_diagnosa['pencegahan_umum'] = "-";
                $hasil_diagnosa['pencegahan_khusus'] = "-";

                $jika_selesai['jika_selesai'] = true;
                
            }
            if ($request->pertanyaan == 'GK13' && $request->jawaban == 'false'){
                $gejala = GejalaModel::select('gejala_models.nama as nama_gejala', 'kode_gejala')
                ->where('kode_gejala','GK03')
                ->first();

                $jika_selesai['jika_selesai'] = false;
            }
            if ($request->pertanyaan == 'GK05' && $request->jawaban == 'false'){
                $gejala = GejalaModel::select('gejala_models.nama as nama_gejala', 'kode_gejala')
                ->where('kode_gejala','GK13')
                ->first();

                $jika_selesai['jika_selesai'] = false;
            }
            if ($request->pertanyaan == 'GK06' && $request->jawaban == 'false'){
                $gejala = GejalaModel::select('gejala_models.nama as nama_gejala', 'kode_gejala')
                ->where('kode_gejala','GK05')
                ->first();

                $jika_selesai['jika_selesai'] = false;
            }
            if ($request->pertanyaan == 'GK22' && $request->jawaban == 'false'){
                $hasil_diagnosa['nama_penyakit'] = "Penyakit belum terdata";
                $hasil_diagnosa['pencegahan_umum'] = "-";
                $hasil_diagnosa['pencegahan_khusus'] = "-";

                $jika_selesai['jika_selesai'] = true;
                
            }
            if ($request->pertanyaan == 'GK43' && $request->jawaban == 'false'){
                $hasil_diagnosa['nama_penyakit'] = "Penyakit belum terdata";
                $hasil_diagnosa['pencegahan_umum'] = "-";
                $hasil_diagnosa['pencegahan_khusus'] = "-";

                $jika_selesai['jika_selesai'] = true;
                
            }
            if ($request->pertanyaan == 'GK40' && $request->jawaban == 'false'){
                $hasil_diagnosa['nama_penyakit'] = "Penyakit belum terdata";
                $hasil_diagnosa['pencegahan_umum'] = "-";
                $hasil_diagnosa['pencegahan_khusus'] = "-";

                $jika_selesai['jika_selesai'] = true;
                
            }
            if ($request->pertanyaan == 'GK39' && $request->jawaban == 'false'){
                $hasil_diagnosa['nama_penyakit'] = "Penyakit belum terdata";
                $hasil_diagnosa['pencegahan_umum'] = "-";
                $hasil_diagnosa['pencegahan_khusus'] = "-";

                $jika_selesai['jika_selesai'] = true;
                
            }
            if ($request->pertanyaan == 'GK38' && $request->jawaban == 'false'){
                $hasil_diagnosa['nama_penyakit'] = "Penyakit belum terdata";
                $hasil_diagnosa['pencegahan_umum'] = "-";
                $hasil_diagnosa['pencegahan_khusus'] = "-";

                $jika_selesai['jika_selesai'] = true;
                
            }
            if ($request->pertanyaan == 'GK44' && $request->jawaban == 'false'){
                $hasil_diagnosa['nama_penyakit'] = "Penyakit belum terdata";
                $hasil_diagnosa['pencegahan_umum'] = "-";
                $hasil_diagnosa['pencegahan_khusus'] = "-";

                $jika_selesai['jika_selesai'] = true;
                
            }
            if ($request->pertanyaan == 'GK43' && $request->jawaban == 'false'){
                $hasil_diagnosa['nama_penyakit'] = "Penyakit belum terdata";
                $hasil_diagnosa['pencegahan_umum'] = "-";
                $hasil_diagnosa['pencegahan_khusus'] = "-";

                $jika_selesai['jika_selesai'] = true;
                
            }
            if ($request->pertanyaan == 'GK42' && $request->jawaban == 'false'){
                $gejala = GejalaModel::select('gejala_models.nama as nama_gejala', 'kode_gejala')
                ->where('kode_gejala','GK38')
                ->first();

                $jika_selesai['jika_selesai'] = false;
            }
            if ($request->pertanyaan == 'GK23' && $request->jawaban == 'false'){
                $gejala = GejalaModel::select('gejala_models.nama as nama_gejala', 'kode_gejala')
                ->where('kode_gejala','GK22')
                ->first();

                $jika_selesai['jika_selesai'] = false;
            }
            if ($request->pertanyaan == 'GK21' && $request->jawaban == 'false'){
                $hasil_diagnosa = Relasi::select('penyakit_models.kode_penyakit as kode_penyakit', 'penyakit_models.nama as nama_penyakit', 'pencegahan_umum', 'pencegahan_khusus')
                ->join('penyakit_models', 'relasis.kode_penyakit', '=', 'penyakit_models.kode_penyakit')
                ->where('penyakit_models.kode_penyakit','PK05')
                ->first();

                $jika_selesai['jika_selesai'] = true;
                
            }
            if ($request->pertanyaan == 'GK20' && $request->jawaban == 'false'){
                $gejala = GejalaModel::select('gejala_models.nama as nama_gejala', 'kode_gejala')
                ->where('kode_gejala','GK23')
                ->first();

                $jika_selesai['jika_selesai'] = false;
            }
            if ($request->pertanyaan == 'GK19' && $request->jawaban == 'false'){
                $hasil_diagnosa['nama_penyakit'] = "Penyakit belum terdata";
                $hasil_diagnosa['pencegahan_umum'] = "-";
                $hasil_diagnosa['pencegahan_khusus'] = "-";

                $jika_selesai['jika_selesai'] = true;
                
            }
            if ($request->pertanyaan == 'GK18' && $request->jawaban == 'false'){
                $gejala = GejalaModel::select('gejala_models.nama as nama_gejala', 'kode_gejala')
                ->where('kode_gejala','GK20')
                ->first();

                $jika_selesai['jika_selesai'] = false;
            }
            if ($request->pertanyaan == 'GK17' && $request->jawaban == 'false'){
                $hasil_diagnosa['nama_penyakit'] = "Penyakit belum terdata";
                $hasil_diagnosa['pencegahan_umum'] = "-";
                $hasil_diagnosa['pencegahan_khusus'] = "-";

                $jika_selesai['jika_selesai'] = true;
                
            }
            if ($request->pertanyaan == 'GK10' && $request->jawaban == 'false'){
                $gejala = GejalaModel::select('gejala_models.nama as nama_gejala', 'kode_gejala')
                ->where('kode_gejala','GK17')
                ->first();

                $jika_selesai['jika_selesai'] = false;
            }
            if ($request->pertanyaan == 'GK12' && $request->jawaban == 'false'){
                $hasil_diagnosa['nama_penyakit'] = "Penyakit belum terdata";
                $hasil_diagnosa['pencegahan_umum'] = "-";
                $hasil_diagnosa['pencegahan_khusus'] = "-";

                $jika_selesai['jika_selesai'] = true;
                
            }
            if ($request->pertanyaan == 'GK11' && $request->jawaban == 'false'){
                $hasil_diagnosa['nama_penyakit'] = "Penyakit belum terdata";
                $hasil_diagnosa['pencegahan_umum'] = "-";
                $hasil_diagnosa['pencegahan_khusus'] = "-";
                
                $jika_selesai['jika_selesai'] = true;
            }
            if ($request->pertanyaan == 'GK04' && $request->jawaban == 'false'){
                $gejala = GejalaModel::select('gejala_models.nama as nama_gejala', 'kode_gejala')
                ->where('kode_gejala','GK10')
                ->first();

                $jika_selesai['jika_selesai'] = false;
            }
            if ($request->pertanyaan == 'GK03' && $request->jawaban == 'false'){
                $gejala = GejalaModel::select('gejala_models.nama as nama_gejala', 'kode_gejala')
                ->where('kode_gejala','GK18')
                ->first();

                $jika_selesai['jika_selesai'] = false;
            }
            if ($request->pertanyaan == 'GK02' && $request->jawaban == 'false'){
                $gejala = GejalaModel::select('gejala_models.nama as nama_gejala', 'kode_gejala')
                ->where('kode_gejala','GK06')
                ->first();

                $jika_selesai['jika_selesai'] = false;
            }
            if ($request->pertanyaan == 'GK01' && $request->jawaban == 'false'){
                $gejala = GejalaModel::select('gejala_models.nama as nama_gejala', 'kode_gejala')
                ->where('kode_gejala','GK30')
                ->first();

                $jika_selesai['jika_selesai'] = false;
            }
        } else { //ketika milih fase nifas
            if ($request->pertanyaan == '0' && $request->jawaban == '0'){
                $gejala = GejalaModel::select('gejala_models.nama as nama_gejala', 'kode_gejala')
                ->where('kode_gejala','GN01')
                ->first();

                $jika_selesai['jika_selesai'] = false;
            }
            if ($request->pertanyaan == 'GN01' && $request->jawaban == 'true'){
                $gejala = GejalaModel::select('gejala_models.nama as nama_gejala', 'kode_gejala')
                ->where('kode_gejala','GN02')
                ->first();

                $jika_selesai['jika_selesai'] = false;
            }
            if ($request->pertanyaan == 'GN02' && $request->jawaban == 'true'){
                $gejala = GejalaModel::select('gejala_models.nama as nama_gejala', 'kode_gejala')
                ->where('kode_gejala','GN03')
                ->first();

                $jika_selesai['jika_selesai'] = false;
            }
            if ($request->pertanyaan == 'GN03' && $request->jawaban == 'true'){
                $gejala = GejalaModel::select('gejala_models.nama as nama_gejala', 'kode_gejala')
                ->where('kode_gejala','GN05')
                ->first();

                $jika_selesai['jika_selesai'] = false;
            }
            if ($request->pertanyaan == 'GN05' && $request->jawaban == 'true'){
                $gejala = GejalaModel::select('gejala_models.nama as nama_gejala', 'kode_gejala')
                ->where('kode_gejala','GN06')
                ->first();

                $jika_selesai['jika_selesai'] = false;
            }
            if ($request->pertanyaan == 'GN06' && $request->jawaban == 'true'){
                $gejala = GejalaModel::select('gejala_models.nama as nama_gejala', 'kode_gejala')
                ->where('kode_gejala','GN07')
                ->first();

                $jika_selesai['jika_selesai'] = false;
            }
            if ($request->pertanyaan == 'GN07' && $request->jawaban == 'true'){
                $gejala = GejalaModel::select('gejala_models.nama as nama_gejala', 'kode_gejala')
                ->where('kode_gejala','GN08')
                ->first();

                $jika_selesai['jika_selesai'] = false;
            }
            if ($request->pertanyaan == 'GN08' && $request->jawaban == 'true'){
                $hasil_diagnosa = Relasi::select('penyakit_models.kode_penyakit as kode_penyakit', 'penyakit_models.nama as nama_penyakit', 'pencegahan_umum', 'pencegahan_khusus')
                ->join('penyakit_models', 'relasis.kode_penyakit', '=', 'penyakit_models.kode_penyakit')
                ->where('penyakit_models.kode_penyakit','PN01')
                ->first();

                $jika_selesai['jika_selesai'] = true;
            }
            if ($request->pertanyaan == 'GN04' && $request->jawaban == 'true'){
                $gejala = GejalaModel::select('gejala_models.nama as nama_gejala', 'kode_gejala')
                ->where('kode_gejala','GN16')
                ->first();

                $jika_selesai['jika_selesai'] = false;
            }
            if ($request->pertanyaan == 'GN16' && $request->jawaban == 'true'){
                $hasil_diagnosa = Relasi::select('penyakit_models.kode_penyakit as kode_penyakit', 'penyakit_models.nama as nama_penyakit', 'pencegahan_umum', 'pencegahan_khusus')
                ->join('penyakit_models', 'relasis.kode_penyakit', '=', 'penyakit_models.kode_penyakit')
                ->where('penyakit_models.kode_penyakit','PN04')
                ->first();

                $jika_selesai['jika_selesai'] = true;
            }
            if ($request->pertanyaan == 'GN17' && $request->jawaban == 'true'){
                $hasil_diagnosa = Relasi::select('penyakit_models.kode_penyakit as kode_penyakit', 'penyakit_models.nama as nama_penyakit', 'pencegahan_umum', 'pencegahan_khusus')
                ->join('penyakit_models', 'relasis.kode_penyakit', '=', 'penyakit_models.kode_penyakit')
                ->where('penyakit_models.kode_penyakit','PN05')
                ->first();

                $jika_selesai['jika_selesai'] = true;
            }
            if ($request->pertanyaan == 'GN09' && $request->jawaban == 'true'){
                $gejala = GejalaModel::select('gejala_models.nama as nama_gejala', 'kode_gejala')
                ->where('kode_gejala','GN10')
                ->first();

                $jika_selesai['jika_selesai'] = false;
            }
            if ($request->pertanyaan == 'GN10' && $request->jawaban == 'true'){
                $gejala = GejalaModel::select('gejala_models.nama as nama_gejala', 'kode_gejala')
                ->where('kode_gejala','GN11')
                ->first();

                $jika_selesai['jika_selesai'] = false;
            }
            if ($request->pertanyaan == 'GN11' && $request->jawaban == 'true'){
                $hasil_diagnosa = Relasi::select('penyakit_models.kode_penyakit as kode_penyakit', 'penyakit_models.nama as nama_penyakit', 'pencegahan_umum', 'pencegahan_khusus')
                ->join('penyakit_models', 'relasis.kode_penyakit', '=', 'penyakit_models.kode_penyakit')
                ->where('penyakit_models.kode_penyakit','PN02')
                ->first();

                $jika_selesai['jika_selesai'] = true;
            }
            if ($request->pertanyaan == 'GN12' && $request->jawaban == 'true'){
                $gejala = GejalaModel::select('gejala_models.nama as nama_gejala', 'kode_gejala')
                ->where('kode_gejala','GN13')
                ->first();

                $jika_selesai['jika_selesai'] = false;
            }
            if ($request->pertanyaan == 'GN13' && $request->jawaban == 'true'){
                $gejala = GejalaModel::select('gejala_models.nama as nama_gejala', 'kode_gejala')
                ->where('kode_gejala','GN14')
                ->first();

                $jika_selesai['jika_selesai'] = false;
            }
            if ($request->pertanyaan == 'GN14' && $request->jawaban == 'true'){
                $gejala = GejalaModel::select('gejala_models.nama as nama_gejala', 'kode_gejala')
                ->where('kode_gejala','GN15')
                ->first();

                $jika_selesai['jika_selesai'] = false;
            }
            if ($request->pertanyaan == 'GN15' && $request->jawaban == 'true'){
                $hasil_diagnosa = Relasi::select('penyakit_models.kode_penyakit as kode_penyakit', 'penyakit_models.nama as nama_penyakit', 'pencegahan_umum', 'pencegahan_khusus')
                ->join('penyakit_models', 'relasis.kode_penyakit', '=', 'penyakit_models.kode_penyakit')
                ->where('penyakit_models.kode_penyakit','PN03')
                ->first();

                $jika_selesai['jika_selesai'] = true;
            }
            if ($request->pertanyaan == 'GN15' && $request->jawaban == 'false'){
                $hasil_diagnosa['nama_penyakit'] = "Penyakit belum terdata";
                $hasil_diagnosa['pencegahan_umum'] = "-";
                $hasil_diagnosa['pencegahan_khusus'] = "-";

                $jika_selesai['jika_selesai'] = true;
            }
            if ($request->pertanyaan == 'GN14' && $request->jawaban == 'false'){
                $hasil_diagnosa['nama_penyakit'] = "Penyakit belum terdata";
                $hasil_diagnosa['pencegahan_umum'] = "-";
                $hasil_diagnosa['pencegahan_khusus'] = "-";

                $jika_selesai['jika_selesai'] = true;
            }
            if ($request->pertanyaan == 'GN13' && $request->jawaban == 'false'){
                $hasil_diagnosa['nama_penyakit'] = "Penyakit belum terdata";
                $hasil_diagnosa['pencegahan_umum'] = "-";
                $hasil_diagnosa['pencegahan_khusus'] = "-";

                $jika_selesai['jika_selesai'] = true;
            }
            if ($request->pertanyaan == 'GN12' && $request->jawaban == 'false'){
                $gejala = GejalaModel::select('gejala_models.nama as nama_gejala', 'kode_gejala')
                ->where('kode_gejala','GN04')
                ->first();

                $jika_selesai['jika_selesai'] = false;
            }
            if ($request->pertanyaan == 'GN11' && $request->jawaban == 'false'){
                $hasil_diagnosa['nama_penyakit'] = "Penyakit belum terdata";
                $hasil_diagnosa['pencegahan_umum'] = "-";
                $hasil_diagnosa['pencegahan_khusus'] = "-";

                $jika_selesai['jika_selesai'] = true;
            }
            if ($request->pertanyaan == 'GN10' && $request->jawaban == 'false'){
                $hasil_diagnosa['nama_penyakit'] = "Penyakit belum terdata";
                $hasil_diagnosa['pencegahan_umum'] = "-";
                $hasil_diagnosa['pencegahan_khusus'] = "-";

                $jika_selesai['jika_selesai'] = true;
            }
            if ($request->pertanyaan == 'GN09' && $request->jawaban == 'false'){
                $hasil_diagnosa['nama_penyakit'] = "Penyakit belum terdata";
                $hasil_diagnosa['pencegahan_umum'] = "-";
                $hasil_diagnosa['pencegahan_khusus'] = "-";

                $jika_selesai['jika_selesai'] = true;
            }
            if ($request->pertanyaan == 'GN17' && $request->jawaban == 'false'){
                $hasil_diagnosa['nama_penyakit'] = "Penyakit belum terdata";
                $hasil_diagnosa['pencegahan_umum'] = "-";
                $hasil_diagnosa['pencegahan_khusus'] = "-";

                $jika_selesai['jika_selesai'] = true;
            }
            if ($request->pertanyaan == 'GN16' && $request->jawaban == 'false'){
                $gejala = GejalaModel::select('gejala_models.nama as nama_gejala', 'kode_gejala')
                ->where('kode_gejala','GN17')
                ->first();

                $jika_selesai['jika_selesai'] = false;
            }
            if ($request->pertanyaan == 'GN04' && $request->jawaban == 'false'){
                $hasil_diagnosa['nama_penyakit'] = "Penyakit belum terdata";
                $hasil_diagnosa['pencegahan_umum'] = "-";
                $hasil_diagnosa['pencegahan_khusus'] = "-";

                $jika_selesai['jika_selesai'] = true;
            }
            if ($request->pertanyaan == 'GN08' && $request->jawaban == 'false'){
                $hasil_diagnosa['nama_penyakit'] = "Penyakit belum terdata";
                $hasil_diagnosa['pencegahan_umum'] = "-";
                $hasil_diagnosa['pencegahan_khusus'] = "-";

                $jika_selesai['jika_selesai'] = true;
            }
            if ($request->pertanyaan == 'GN07' && $request->jawaban == 'false'){
                $hasil_diagnosa['nama_penyakit'] = "Penyakit belum terdata";
                $hasil_diagnosa['pencegahan_umum'] = "-";
                $hasil_diagnosa['pencegahan_khusus'] = "-";

                $jika_selesai['jika_selesai'] = true;
            }
            if ($request->pertanyaan == 'GN06' && $request->jawaban == 'false'){
                $hasil_diagnosa['nama_penyakit'] = "Penyakit belum terdata";
                $hasil_diagnosa['pencegahan_umum'] = "-";
                $hasil_diagnosa['pencegahan_khusus'] = "-";

                $jika_selesai['jika_selesai'] = true;
            }
            if ($request->pertanyaan == 'GN05' && $request->jawaban == 'false'){
                $gejala = GejalaModel::select('gejala_models.nama as nama_gejala', 'kode_gejala')
                ->where('kode_gejala','GN09')
                ->first();

                $jika_selesai['jika_selesai'] = false;
            }
            if ($request->pertanyaan == 'GN03' && $request->jawaban == 'false'){
                $hasil_diagnosa['nama_penyakit'] = "Penyakit belum terdata";
                $hasil_diagnosa['pencegahan_umum'] = "-";
                $hasil_diagnosa['pencegahan_khusus'] = "-";

                $jika_selesai['jika_selesai'] = true;
            }
            if ($request->pertanyaan == 'GN02' && $request->jawaban == 'false'){
                $hasil_diagnosa['nama_penyakit'] = "Penyakit belum terdata";
                $hasil_diagnosa['pencegahan_umum'] = "-";
                $hasil_diagnosa['pencegahan_khusus'] = "-";

                $jika_selesai['jika_selesai'] = true;
            }
            if ($request->pertanyaan == 'GN01' && $request->jawaban == 'false'){
                $gejala = GejalaModel::select('gejala_models.nama as nama_gejala', 'kode_gejala')
                ->where('kode_gejala','GN12')
                ->first();

                $jika_selesai['jika_selesai'] = false;
            }
        }
        if ($request->pertanyaan == '0'){
            return response()->json([    
                'code' =>200,
                'pertanyaan' => $gejala,
                'status' => $jika_selesai
            ]);
        }elseif  ($request->pertanyaan != '0'){
            if ($request->pertanyaan == 'GN15' && $request->jawaban == 'true' || $request->pertanyaan == 'GN17' && $request->jawaban == 'true' || $request->pertanyaan == 'GN16' && $request->jawaban == 'true' || $request->pertanyaan == 'GN11' && $request->jawaban == 'true' || $request->pertanyaan == 'GN08' && $request->jawaban == 'true')
            {
                return response()->json([    
                    'code' =>200,
                    'data' => $id_pasien,
                    'hasil' => $hasil_diagnosa,
                    'status' => $jika_selesai
                ]);
            }elseif ($request->pertanyaan == 'GK21' && $request->jawaban == 'false' || $request->pertanyaan == 'GK25' && $request->jawaban == 'false' || $request->pertanyaan == 'GK09' && $request->jawaban == 'true' || $request->pertanyaan == 'GK10' && $request->jawaban == 'true' || $request->pertanyaan == 'GK12' && $request->jawaban == 'true' || $request->pertanyaan == 'GK36' && $request->jawaban == 'true' || $request->pertanyaan == 'GK22' && $request->jawaban == 'true' || $request->pertanyaan == 'GK25' && $request->jawaban == 'true' || $request->pertanyaan == 'GK37' && $request->jawaban == 'false' || $request->pertanyaan == 'GK37' && $request->jawaban == 'true' || $request->pertanyaan == 'GK35' && $request->jawaban == 'true' || $request->pertanyaan == 'GK41' && $request->jawaban == 'true' || $request->pertanyaan == 'GK44' && $request->jawaban == 'true')
            {
                return response()->json([    
                    'code' =>200,
                    'data' => $id_pasien,
                    'hasil' => $hasil_diagnosa,
                    'status' => $jika_selesai
                ]);
            }elseif ($request->pertanyaan == 'GN02' && $request->jawaban == 'false' || $request->pertanyaan == 'GN03' && $request->jawaban == 'false' || $request->pertanyaan == 'GN06' && $request->jawaban == 'false' || $request->pertanyaan == 'GN07' && $request->jawaban == 'false' || $request->pertanyaan == 'GN08' && $request->jawaban == 'false' || $request->pertanyaan == 'GN04' && $request->jawaban == 'false' || $request->pertanyaan == 'GN17' && $request->jawaban == 'false' || $request->pertanyaan == 'GN09' && $request->jawaban == 'false' || $request->pertanyaan == 'GN10' && $request->jawaban == 'false' || $request->pertanyaan == 'GN13' && $request->jawaban == 'false' || $request->pertanyaan == 'GN14' && $request->jawaban == 'false' || $request->pertanyaan == 'GN15' && $request->jawaban == 'false' || $request->pertanyaan == 'GK11' && $request->jawaban == 'false' || $request->pertanyaan == 'GK12' && $request->jawaban == 'false' || $request->pertanyaan == 'GK17' && $request->jawaban == 'false' || $request->pertanyaan == 'GK19' && $request->jawaban == 'false' || $request->pertanyaan == 'GK40' && $request->jawaban == 'false' || $request->pertanyaan == 'GK43' && $request->jawaban == 'false' || $request->pertanyaan == 'GK22' && $request->jawaban == 'false' || $request->pertanyaan == 'GK14' && $request->jawaban == 'false' || $request->pertanyaan == 'GK15' && $request->jawaban == 'false' || $request->pertanyaan == 'GK16' && $request->jawaban == 'false' || $request->pertanyaan == 'GK36' && $request->jawaban == 'false' || $request->pertanyaan == 'GK24' && $request->jawaban == 'false' || $request->pertanyaan == 'GK27' && $request->jawaban == 'false' || $request->pertanyaan == 'GK28' && $request->jawaban == 'false' || $request->pertanyaan == 'GK29' && $request->jawaban == 'false' || $request->pertanyaan == 'GK07' && $request->jawaban == 'false' || $request->pertanyaan == 'GK08' && $request->jawaban == 'false' || $request->pertanyaan == 'GK32' && $request->jawaban == 'false' || $request->pertanyaan == 'GK33' && $request->jawaban == 'false' || $request->pertanyaan == 'GK34' && $request->jawaban == 'false' || $request->pertanyaan == 'GK35' && $request->jawaban == 'false' || $request->pertanyaan == 'GK31' && $request->jawaban == 'false' || $request->pertanyaan == 'GK09' && $request->jawaban == 'false' || $request->pertanyaan == 'GK43' && $request->jawaban == 'false' || $request->pertanyaan == 'GK44' && $request->jawaban == 'false' || $request->pertanyaan == 'GK38' && $request->jawaban == 'false' || $request->pertanyaan == 'GK39' && $request->jawaban == 'false')
            {
                return response()->json([    
                    'code' =>200,
                    'data' => $id_pasien,
                    'hasil' => $hasil_diagnosa,
                    'status' => $jika_selesai
                ]);
            }else
            {
                return response()->json([    
                    'code' =>200,
                    'pertanyaan' => $gejala,
                    'status' => $jika_selesai
                ]);
            }
        }else {
            return response()->json([
                'code' =>200,
                'data' => $id_pasien,
                'hasil' => $hasil_diagnosa,
                'status' => $jika_selesai
            ]);
        }
    }

    public function simpan(Request $request)
    {
        if ($request->jawaban == 'true')
        {
            $hasil = DiagnosaModel::create([
            'id_pasien' => $request->id_pasien_hasil,
            'kode_penyakit' => $request->kode_penyakit_hasil,
            'tanggal' => $request->tanggal_hasil,
            'status' => 1
            ]);
        }else
        {
            $hasil = DiagnosaModel::create([
            'id_pasien' => $request->id_pasien_hasil,
            'kode_penyakit' => $request->kode_penyakit_hasil,
            'tanggal' => $request->tanggal_hasil,
            'status' => 0
            ]);
        }

        return response()->json([
            'code' =>200,
            'data' => 'Data Sukses Masuk'
        ]);
    }
}