<?php

namespace App\Http\Controllers;

use App\PemeriksaanKehamilanPertama;
use Illuminate\Http\Request;

class PemeriksaanKehamilanPertamaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\PemeriksaanKehamilanPertama  $pemeriksaanKehamilanPertama
     * @return \Illuminate\Http\Response
     */
    public function show(PemeriksaanKehamilanPertama $pemeriksaanKehamilanPertama)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\PemeriksaanKehamilanPertama  $pemeriksaanKehamilanPertama
     * @return \Illuminate\Http\Response
     */
    public function edit(PemeriksaanKehamilanPertama $pemeriksaanKehamilanPertama)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\PemeriksaanKehamilanPertama  $pemeriksaanKehamilanPertama
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, PemeriksaanKehamilanPertama $pemeriksaanKehamilanPertama)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\PemeriksaanKehamilanPertama  $pemeriksaanKehamilanPertama
     * @return \Illuminate\Http\Response
     */
    public function destroy(PemeriksaanKehamilanPertama $pemeriksaanKehamilanPertama)
    {
        //
    }
}
