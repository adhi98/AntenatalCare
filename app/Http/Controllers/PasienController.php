<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\PasienModel;
use App\WaliModel;
use App\User;
use App\AgamaModel;
use App\PekerjaanModel;
use App\PendidikanModel;
use App\SemesterModel;
use App\GolonganDarahModel;
use Auth;
use Session;

class PasienController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()    
    {   $pagama = AgamaModel::get();
        $wagama = AgamaModel::get();
        $ppekerjaan = PekerjaanModel::where('status', 'p')->get();
        $wpekerjaan = PekerjaanModel::where('status', 'w')->get();
        $pendidikan = PendidikanModel::get();
        $goldarp = GolonganDarahModel::get();
        $goldarw = GolonganDarahModel::get();
        //dd($agama, $ppekerjaan, $wpekerjaan, $pendidikan);
        return view('admin/pendaftaranPasien', ['pagama' => $pagama, 'wagama' => $wagama,'ppekerjaan' => $ppekerjaan, 'wpekerjaan' => $wpekerjaan, 'pendidikan' => $pendidikan, 'goldarp' => $goldarp, 'goldarw' => $goldarw]);
    }

    public function listPasien()
    {        
        $list = PasienModel::select('pasien_models.nama as nama', 'pasien_models.status as fase','semester_models.semester as status', 'pasien_models.id as id','pasien_models.tanggal_lahir as tanggal_lahir', 'pekerjaan_models.pekerjaan as pekerjaan', 'pasien_models.alamat as alamat', 'pasien_models.no_tlp as no_tlp', 'agama_models.agama as agama')->join('agama_models', 'pasien_models.agama_id', '=', 'agama_models.id')
        ->join('pekerjaan_models', 'pasien_models.pekerjaan_id', '=', 'pekerjaan_models.id')->join('semester_models', 'pasien_models.status', '=', 'semester_models.id')->orderBy('fase')
        ->get();        
        //dd($list);
        
                        
        return view('admin/listPasien', ['pasien' => $list]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $aturan = [
            'nojkn_pasien' => 'required|numeric',
            'nopasien' => 'required|numeric',
            'nama_pasien' => 'required',
            'tgl_lahir_pasien' => 'required',
            'agama_pasien' => 'required',
            'golongan_darah_pasien' => 'required',
            'pekerjaan_pasien' => 'required',
            'notlp_pasien' => 'required|numeric',
            'noktp_pasien' => 'required|numeric',
            'noktp_wali' => 'required|numeric',
            'alamat_pasien' => 'required',
            'nama_wali' => 'required',
            'tgl_lahir_wali' => 'required',
            'agama_wali' => 'required',
            'golongan_darah_wali' => 'required',
            'pendidikan_wali' => 'required',
            'pekerjaan_wali' => 'required',
            'notlp_wali' => 'required|numeric',
            'noktp_wali' => 'required|numeric',
            'alamat_wali' => 'required',
        ];

        $pesan = [
            'required' => 'Data ini wajib di Isi',
            'required|numeric' => 'Mohon isi dengan angka'
        ];

        $this->validate($request,$aturan,$pesan);

        $bidan_id = Auth::id();

        $pasien = PasienModel::create([
            'nama' => $request->nama_pasien,
            'tanggal_lahir' => $request->tgl_lahir_pasien,            
            'agama_id' => $request->agama_pasien,
            'bidan_id' => $bidan_id,
            'golongan_darah' => $request->golongan_darah_pasien,
            'pekerjaan_id' => $request->pekerjaan_pasien,
            'no_tlp' => $request->notlp_pasien,
            'no_ktp' => $request->noktp_pasien,
            'no_jkn' => $request->nojkn_pasien,
            'no_pasien' => $request->nopasien,
            'password' => 12345678,
            'alamat' => $request->alamat_pasien,
            'status' => '1'
        ]);        
        
        

        $wali = WaliModel::create([            
            'nama' => $request->nama_wali,
            'tanggal_lahir' => $request->tgl_lahir_wali,
            'agama_id' => $request->agama_wali,
            'golongan_darah' => $request->golongan_darah_wali,
            'pendidikan_id' => $request->pendidikan_wali,
            'pekerjaan_id' => $request->pekerjaan_wali,
            'no_tlp' => $request->notlp_wali,            
            'no_ktp' => $request->noktp_wali,            
            'alamat' => $request->alamat_wali,            
            'pasien_id' => $pasien->id,
        ]);

        Session::flash('success', 'Data Berhasil Ditambahkan!');
        
        return redirect()->route('listpasien');


    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $pasien = PasienModel::select('wali_models.nama as wnama', 'wali_models.tanggal_lahir as wtgl_lahir', 'wali_models.agama_id as wagama', 'wali_models.golongan_darah as wgoldar', 'wali_models.pendidikan_id as wpendidikan', 'wali_models.pekerjaan_id as wpekerjaan', 'wali_models.no_ktp as wnoktp', 'wali_models.no_tlp as wnotlp', 'wali_models.alamat as walamat', 'wali_models.pasien_id as wpasien_id', 'pasien_models.nama as pnama', 'pasien_models.tanggal_lahir as ptgl_lahir', 'pasien_models.agama_id as pagama', 'pasien_models.golongan_darah as pgoldar', 'pasien_models.pekerjaan_id as ppekerjaan', 'pasien_models.no_tlp as pnotlp', 'pasien_models.no_ktp as pnoktp', 'pasien_models.no_jkn as pnojkn', 'pasien_models.no_pasien as pnopasien', 'pasien_models.alamat as palamat', 'pasien_models.id as pid' )->join('wali_models', 'wali_models.pasien_id', '=' , 'pasien_models.id')->where('pasien_models.id', $id)->first();        

        //dd($pasien);
        

           $pagama = AgamaModel::get();
            $wagama = AgamaModel::get();
            $ppekerjaan = PekerjaanModel::where('status', 'p')->get();
            $wpekerjaan = PekerjaanModel::where('status', 'w')->get();
            $pendidikan = PendidikanModel::get();

            $goldarp = GolonganDarahModel::get();
            $goldarw = GolonganDarahModel::get();

        return view('admin/editPasien', ['pasien' => $pasien, 'pagama' => $pagama, 'wagama' => $wagama, 'ppekerjaan' => $ppekerjaan, 'wpekerjaan' => $wpekerjaan, 'pendidikan' => $pendidikan, 'goldarp' => $goldarp, 'goldarw' => $goldarw]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //dd($id);
        $bidan_id = Auth::id();
        PasienModel::where('id', $id)
        ->update([
            'nama' => $request->nama_pasien,
            'tanggal_lahir' => $request->tgl_lahir_pasien,            
            'agama_id' => $request->agama_pasien,
            'bidan_id' => $bidan_id,
            'golongan_darah' => $request->golongan_darah_pasien,
            'pekerjaan_id' => $request->pekerjaan_pasien,
            'no_tlp' => $request->notlp_pasien,
            'no_ktp' => $request->noktp_pasien,
            'no_jkn' => $request->nojkn_pasien,
            'no_pasien' => $request->nopasien,
            'alamat' => $request->alamat_pasien,
        ]);

        WaliModel::where('pasien_id', $id)
        ->update([
            'nama' => $request->nama_wali,
            'tanggal_lahir' => $request->tgl_lahir_wali,
            'agama_id' => $request->agama_wali,
            'golongan_darah' => $request->golongan_darah_wali,
            'pendidikan_id' => $request->pendidikan_wali,
            'pekerjaan_id' => $request->pekerjaan_wali,
            'no_tlp' => $request->notlp_wali,            
            'no_ktp' => $request->noktp_wali,            
            'alamat' => $request->alamat_wali,                        
        ]);

        Session::flash('success', 'Data Berhasil Diupdate!');

        return redirect()->back();

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}