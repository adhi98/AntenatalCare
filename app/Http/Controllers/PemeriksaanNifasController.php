<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\PasienModel;
use App\PemeriksaanNifasModel;
use App\PemeriksaanKehamilanModel;
use App\notifikasiNifasModel;
use App\KesimpulanNifasModel;
use Session;
use Carbon\Carbon;
use PDF;

class PemeriksaanNifasController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($id)
    {

        $pasien = PasienModel::select('*')->where('id', $id)->first();
        //dd($pasien);
         
        $hpl = Carbon::parse($pasien->tgl_perkiraan_melahirkan);

        $today = Carbon::today();

        if($today == $hpl || $today > $hpl || $pasien->status === 4) {

            $status = PemeriksaanNifasModel::where('id', $id)->update([            
                'status' => '4',            
            ]);
            
            //status Pasien
            $statusPasien = PasienModel::where('id', $id)->update([
                'status' => '4',
            ]);

            $jumlahPemeriksaanNifas = PemeriksaanNifasModel::where('pasien_id', $id)->count();
            //dd($jumlahPemeriksaanNifas);
            
            if($jumlahPemeriksaanNifas < 2){
                return view('admin/pemeriksaanNifas', ['pasien' => $pasien, 'akhirNifas' => $jumlahPemeriksaanNifas]);                                
                
            }
            if($jumlahPemeriksaanNifas > 1) {
                return view('admin/pemeriksaanNifas', ['pasien' => $pasien, 'akhirNifas' => $jumlahPemeriksaanNifas]);
            }                       
                            
        }
        

        Session::flash('error', 'Pasien Belum masuk Fase Nifas');                                      

        return redirect()->route('listpasien');
       
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $id)
    {                   

        $jumlahPemeriksaanNifas = PemeriksaanNifasModel::where('pasien_id', $id)->count(); 
        //dd($jumlahPemeriksaanNifas);
        
        if($jumlahPemeriksaanNifas == 0){
            $hpl = PasienModel::select('tgl_perkiraan_melahirkan')->where('id', $id)->first();

            $hpl = Carbon::parse($hpl->tgl_perkiraan_melahirkan);

            //hari pertama nifas
            $hpn = $hpl->copy()->addDays(1)->format('Y-m-d');

            //hari akhir nifas
            $han = $hpl->copy()->addDays(42)->format('Y-m-d');        
            
                            
            $pnifas = PemeriksaanNifasModel::create([
                'pasien_id' => $id,            
                'lokhia' => $request->lokhia,
                'jalan_lahir' => $request->jalan_lahir,     
                'tekanan_darah' => $request->tekanan_darah,
                'tinggi_fundus' => $request->tinggi_fundus,
                'pemeriksaan_payudara' => $request->pemeriksaan_payudara,
                'nasihat' => $request->nasihat,
                'status'=> 1,
                'tgl_awal_nifas' => $hpn,
                'tgl_akhir_nifas' => $han,
                'tgl_pemeriksaan' => $request->tgl_pemeriksaan,
                'tgl_kembali' => $request->tgl_kembali,
            ]);
        }
        
        if($jumlahPemeriksaanNifas == 1){

            $hpl = PasienModel::select('tgl_perkiraan_melahirkan')->where('id', $id)->first();

            $hpl = Carbon::parse($hpl->tgl_perkiraan_melahirkan);

            //hari pertama nifas
            $hpn = $hpl->copy()->addDays(1)->format('Y-m-d');

            //hari akhir nifas
            $han = $hpl->copy()->addDays(42)->format('Y-m-d');        
            
                            
            $pnifas = PemeriksaanNifasModel::create([
                'pasien_id' => $id,            
                'lokhia' => $request->lokhia,
                'jalan_lahir' => $request->jalan_lahir,     
                'tekanan_darah' => $request->tekanan_darah,
                'tinggi_fundus' => $request->tinggi_fundus,
                'pemeriksaan_payudara' => $request->pemeriksaan_payudara,
                'nasihat' => $request->nasihat,
                'status'=> 1,
                'tgl_awal_nifas' => $hpn,
                'tgl_akhir_nifas' => $han,
                'tgl_pemeriksaan' => $request->tgl_pemeriksaan,
                'tgl_kembali' => $request->tgl_kembali,
            ]);

            $statusNifas = PemeriksaanNifasModel::where('id', $pnifas->id)->update([
                'status' => 2,
            ]);
            //dd(2);
        }
        

        if($jumlahPemeriksaanNifas == 2){

            $hpl = PasienModel::select('tgl_perkiraan_melahirkan')->where('id', $id)->first();

            $hpl = Carbon::parse($hpl->tgl_perkiraan_melahirkan);

            //hari pertama nifas
            $hpn = $hpl->copy()->addDays(1)->format('Y-m-d');

            //hari akhir nifas
            $han = $hpl->copy()->addDays(42)->format('Y-m-d');        
            
                            
            $pnifas = PemeriksaanNifasModel::create([
                'pasien_id' => $id,            
                'lokhia' => $request->lokhia,
                'jalan_lahir' => $request->jalan_lahir,     
                'tekanan_darah' => $request->tekanan_darah,
                'tinggi_fundus' => $request->tinggi_fundus,
                'pemeriksaan_payudara' => $request->pemeriksaan_payudara,
                'nasihat' => $request->nasihat,
                'status'=> 1,
                'tgl_awal_nifas' => $hpn,
                'tgl_akhir_nifas' => $han,
                'tgl_pemeriksaan' => $request->tgl_pemeriksaan,
                'tgl_kembali' => $request->tgl_pemeriksaan,
            ]);

            $kesimpulanNifas = KesimpulanNifasModel::create([
                'pemeriksaan_nifas_id' => $pnifas->id,
                'keadaan' => $request->keadaan
            ]);

            //statusnifas
            $statusNifas = PemeriksaanNifasModel::where('id', $pnifas->id)->update([
                'status' => 3,
            ]);
            
            //status Pasien
            $statusPasien = PasienModel::where('id', $id)->update([
                'status' => '5',
            ]);
            //dd(3);
        }       
                
        
        //Add Notifikasi Nifas
        $notif = notifikasiNifasModel::create([
            'pasien_id' => $id,
            'pemeriksan_nifas_id' => $pnifas->id,
        ]);        
        
        Session::flash('success', 'Data Berhasil Disimpan!');                                      
        return redirect()->route('listpasien');
        
        
    }

    public function riwayatPemeriksaan(){
        $pasien = PasienModel::select('*')->where('status', '>', '3')->get();
        return view('admin/riwayatPemeriksaanNifas', ['pasien' => $pasien]);
    }

    public function detailPemeriksaan($id){        
        $pasien = PasienModel::select('nama')->where('id', $id)->first();
        $pkn = PemeriksaanNifasModel::select('*')->where('pasien_id', $id)->get(); 
        //dd($pkn);       
        return view('admin/detailRiwayatNifas', ['pkn' => $pkn, 'pasien' => $pasien]);
    }
    

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {   

        // $pkn = PemeriksaanNifasModel::select('*')->where('id', $id)->first();
        //dd($pkn);
        $pkn = PemeriksaanNifasModel::select('pemeriksaan_nifas_models.tgl_pemeriksaan as tgl_pemeriksaan', 'pemeriksaan_nifas_models.tekanan_darah as tekanan_darah', 'pemeriksaan_nifas_models.lokhia as lokhia',
        'pemeriksaan_nifas_models.jalan_lahir as jalan_lahir',
        'pemeriksaan_nifas_models.tinggi_fundus as tinggi_fundus',
        'pemeriksaan_nifas_models.pemeriksaan_payudara as pemeriksaan_payudara',
        'pemeriksaan_nifas_models.nasihat as nasihat',
        'pemeriksaan_nifas_models.id as id',
        'pasien_models.nama as nama'
    )->join('pasien_models', 'pasien_models.id', '=', 'pemeriksaan_nifas_models.pasien_id')->first();
    
        return view('admin/editPemeriksaanNifas', ['pkn' => $pkn]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $updatepkn = PemeriksaanNifasModel::where('id', $id)->update([
            'tekanan_darah' => $request->tekanan_darah,
            'tinggi_fundus' => $request->tinggi_fundus,
            'lokhia' => $request->lokhia,
            'jalan_lahir' => $request->jalan_lahir,
            'pemeriksaan_payudara' => $request->pemeriksaan_payudara,
            'nasihat' => $request->nasihat,
        ]);
        Session::flash('success', 'Data Berhasil Diupdate!');                                      
        return redirect()->route('riwayatPemeriksaanNifas');
    }

    public function downloadPDF($id){
        
        $data = PemeriksaanNifasModel::select('*')->where('pasien_id', $id)->get();

        $pasien = PasienModel::select('*')->where('id', $id)->first();

        $kesimpulanNifas = PemeriksaanNifasModel::select('kesimpulan_nifas_models.keadaan')
        ->join('kesimpulan_nifas_models', 'kesimpulan_nifas_models.pemeriksaan_nifas_id','=', 'pemeriksaan_nifas_models.id')->first();
        
        $pdf = PDF::loadView('pdf/catatanNifas', ['data' => $data, 'pasien' => $pasien, 'kesimpulan' => $kesimpulanNifas]);
        return $pdf->stream('catatanNifas.pdf');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}