<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\PasienModel;
use Auth;

class BidanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
                
        $p1 = PasienModel::where('status', '1')->count();        
        $p2 = PasienModel::where('status', '2')->count();        
        $p3 = PasienModel::where('status', '3')->count();        
        $pn = PasienModel::where('status', '4')->count();        
        $ps = PasienModel::where('status', '5')->count();        
        $pt = PasienModel::count();
                
        return view('admin/dashboard', ['p1' => $p1, 'p2' => $p2, 'p3' => $p3, 'pn' => $pn, 'ps' => $ps, 'pt' => $pt]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}