<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\PasienModel;
use App\PemeriksaanKehamilanModel;
use App\notifikasiKehamilanModel;
use App\PemeriksaanKehamilanPertama;
use App\KekModel;
use App\PersalinanTerakhirModel;
use App\HasilLab;
use App\LetakJaninModel;

use Carbon\Carbon;
use Session;
use PDF;

class PemeriksaanKehamilanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($id)
    {                                 
        $pasien = PasienModel::select('id', 'nama', 'tgl_perkiraan_melahirkan', 'status')->where('id', $id)->first();
        
        // $hpl = Carbon::parse($pasien->tgl_perkiraan_melahirkan);        

        // $today = Carbon::today();
                
        if($pasien->status > 3){            

            Session::flash('error', 'Pasien Sudah masuk Fase Nifas');                                      
            return redirect()->route('listpasien');
            //dd(1);                        
        }                 

        $cekpasien = PemeriksaanKehamilanModel::where('pasien_id', $id)->first();       

        $janin = LetakJaninModel::get();
        
        //dd($cekpasien);
        
        //pemeriksaan pasien pertama
        if($cekpasien == null){
            $kek = KekModel::get();
            $persalinan = PersalinanTerakhirModel::get();            
            return view('admin/pemeriksaanKehamilanPertama', ['pasien' => $pasien, 'kek' => $kek, 'persalinan' => $persalinan, 'janin' => $janin]);
        }

        return view('admin/pemeriksaanKehamilan', ['pasien' => $pasien, 'janin' => $janin]);
        //dd(2);
                
                
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    // Pemeriksaan Pertama Kali
    public function firststore(Request $request, $id){                        
        $aturan = [            
            'tanggal_pemeriksaan' => 'required',
            'berat_badan' => 'required',
            'tekanan_darah' => 'required',
            'lingkar_lengan' => 'required',
            'tinggi_rahim' => 'required',
            'tanggal_kembali' => 'required',
            'letak_janin' => 'required',            
            'hasil_konsultasi' => 'required',
            'keluhan' => 'required',
            'denyut_jantung_janin' => 'required',
            'kaki_bengkak' => 'required',
            'anemia' => 'required',
            'hiv' => 'required',
            'sivilis' => 'required',
            'tindakan' => 'required',
            'hpht' => 'required',
            'tinggi_badan' => 'required',
            'jumlah_kehamilan' => 'required',
            'jumlah_persalinan' => 'required',
            'jumlah_keguguran' => 'required',
            'jumlah_anak_hidup' => 'required',
            'jumlah_anak_lahir_mati'  => 'required',
            'jumlah_anak_lahir_prematur' => 'required',
            'kontrasepsi' => 'required',
            'persalinan_terakhir' => 'required',
            'imunisasi_TT' => 'required',
            'riwayat_penyakit' => 'required',
            'riwayat_alergi' => 'required',
        ];

        $pesan = [
            'required' => 'Data ini wajib di Isi',
            'required|numeric' => 'Mohon isi dengan angka'
        ];

        $this->validate($request,$aturan,$pesan);
            
            //pemeriksaan rutin
            $pKehamilan = PemeriksaanKehamilanModel::create([
                'pasien_id' => $id,                
                'tanggal_pemeriksaan' => $request->tanggal_pemeriksaan,
                'berat_badan' => $request->berat_badan,
                'tekanan_darah' => $request->tekanan_darah,
                'lingkar_lengan' => $request->lingkar_lengan,
                'tinggi_rahim' => $request->tinggi_rahim,
                'tanggal_kembali' => $request->tanggal_kembali,
                'letak_janin' => $request->letak_janin,                
                'hasil_konsultasi' => $request->hasil_konsultasi,
                'keluhan' => $request->keluhan,
                'status' => 1,                   
                'denyut_jantung_janin' => $request->denyut_jantung_janin,
                'kaki_bengkak' => $request->kaki_bengkak,
                'tindakan' => $request->tindakan,                                
            ]);
                                    
            $hasilLab = HasilLab::create([
                'pemeriksaan_kehamilan_id' => $pKehamilan->id,
                'anemia' => $request->anemia,
                'hiv'=> $request->hiv,
                'sivilis' => $request->sivilis
            ]);

            $riwayatKesehatan = PemeriksaanKehamilanPertama::create([
                'pasien_id' => $id,
                'jumlah_kehamilan' => $request->jumlah_kehamilan,
                'jumlah_persalinan' => $request->jumlah_persalinan,
                'jumlah_keguguran'=> $request->jumlah_keguguran,
                'jumlah_anak_hidup' => $request->jumlah_anak_hidup,
                'jumlah_anak_lahir_mati' => $request->jumlah_anak_lahir_mati,
                'jumlah_anak_lahir_prematur' => $request->jumlah_anak_lahir_prematur,
                'kontrasepsi' => $request->kontrasepsi,
                'imunisasi_TT' => $request->imunisasi_TT,
                'kek_id' => $request->KEK,
                'riwayat_penyakit' => $request->riwayat_penyakit,
                'riwayat_alergi' => $request->riwayat_alergi,
                'persalinan_terakhir' => $request->persalinan_terakhir,
            ]);
                                
            $notif = notifikasiKehamilanModel::create([
                'pasien_id' => $id,
                'pemeriksan_kehamilan_id' => $pKehamilan->id,
            ]);
            
            $pasien = PasienModel::where('id', $id)->update([
                'tinggi_badan' => $request->tinggi_badan,
                'tgl_mens_terakhir' => $request->hpht,            
             ]);
    
            //  Cek Status Pasien
             $hpht = PasienModel::select('tgl_mens_terakhir')->where('id', $id)->first();
            
            //start semester 1 dan end semester 1
            $startS1 = Carbon::parse($hpht->tgl_mens_terakhir);
            $endS1 = $startS1->copy()->addMonths(3);            

            //start semester 2 dan end semester 2
            $startS2 = $endS1->copy()->addDays(1);
            $endS2 = $startS2->copy()->addMonths(3);

                                                
            //start semester 3 dan end semester 3
            $startS3 = $endS2->copy()->addDays(1);
            $tgl_perkiraan_melahirkan = PasienModel::select('tgl_perkiraan_melahirkan')->where('id', $id)->first();
            $endS3 = Carbon::parse($tgl_perkiraan_melahirkan->tgl_perkiraan_melahirkan);

            //today
            $tgl_pemeriksaan = PemeriksaanKehamilanModel::select('tanggal_pemeriksaan')->where('id', $pKehamilan->id)->first();
            
            $today = Carbon::parse($tgl_pemeriksaan->tanggal_pemeriksaan);
            
            //kondisi semester 1 
            if($startS1 < $today && $today <= $endS1){

                $status = PemeriksaanKehamilanModel::where('id', $pKehamilan->id)->update([            
                    'status' => '1',            
                ]);
                
                //status Pasien
                $statusPasien = PasienModel::where('id', $id)->update([
                    'status' => '1',
                ]);
            }
            //kondisi semster 2
            if($startS2 < $today && $today <= $endS2){
                $status = PemeriksaanKehamilanModel::where('id', $pKehamilan->id)->update([            
                    'status' => '2',            
                ]);
                
                //status Pasien
                $statusPasien = PasienModel::where('id', $id)->update([
                    'status' => '2',
                ]);
            }
            //kondisi semester 3
            if($startS3 < $today && $today < $endS3){

                $status = PemeriksaanKehamilanModel::where('id', $pKehamilan->id)->update([            
                    'status' => '3',            
                ]);
                
                //status Pasien
                $statusPasien = PasienModel::where('id', $id)->update([
                    'status' => '3',
                ]);

            }
            
            $newDates = Carbon::parse($hpht->tgl_mens_terakhir);
            
            if($newDates->month > 3){
                $lastDates = $newDates->copy()->addDays(7)->subMonths(3)->addYears(1)->format('Y-m-d');
                
                $pasien = PasienModel::where('id', $id)->update([            
                    'tgl_perkiraan_melahirkan' => $lastDates,            
                ]);
            } else {                
                $lastDates = $newDates->copy()->addDays(7)->addMonths(9)->format('Y-m-d');
                $pasien = PasienModel::where('id', $id)->update([            
                    'tgl_perkiraan_melahirkan' => $lastDates,            
                ]);
            }

       
        Session::flash('success', 'Data Berhasil Diupdate!');                                      
          return redirect()->route('listpasien');
        
    }

    // Pemeriksan Rutin
    public function store(Request $request, $id)
    {        
                                
        $aturan = [            
            'tanggal_pemeriksaan' => 'required',
            'berat_badan' => 'required',
            'tekanan_darah' => 'required',
            'lingkar_lengan' => 'required',
            'tinggi_rahim' => 'required',
            'tanggal_kembali' => 'required',
            'letak_janin' => 'required',            
            'hasil_konsultasi' => 'required',
            'keluhan' => 'required',
            'denyut_jantung_janin' => 'required',
            'kaki_bengkak' => 'required',
            'anemia' => 'required',
            'hiv' => 'required',
            'sivilis' => 'required',
            'tindakan' => 'required',
        ];

        $pesan = [
            'required' => 'Data ini wajib di Isi',
            'required|numeric' => 'Mohon isi dengan angka'
        ];

        $this->validate($request,$aturan,$pesan);        

        $pKehamilan = PemeriksaanKehamilanModel::create([
            'pasien_id' => $id,                
            'tanggal_pemeriksaan' => $request->tanggal_pemeriksaan,
            'berat_badan' => $request->berat_badan,
            'tekanan_darah' => $request->tekanan_darah,
            'lingkar_lengan' => $request->lingkar_lengan,
            'tinggi_rahim' => $request->tinggi_rahim,
            'tanggal_kembali' => $request->tanggal_kembali,
            'letak_janin' => $request->letak_janin,                
            'hasil_konsultasi' => $request->hasil_konsultasi,
            'keluhan' => $request->keluhan,
            'status' => 1,                   
            'denyut_jantung_janin' => $request->denyut_jantung_janin,
            'kaki_bengkak' => $request->kaki_bengkak,
            'tindakan' => $request->tindakan,                                
        ]);

        $hasilLab = HasilLab::create([
            'pemeriksaan_kehamilan_id' => $pKehamilan->id,
            'anemia' => $request->anemia,
            'hiv'=> $request->hiv,
            'sivilis' => $request->sivilis
        ]);
                                
            $notif = notifikasiKehamilanModel::create([
                'pasien_id' => $id,
                'pemeriksan_kehamilan_id' => $pKehamilan->id,
            ]);

            $hpht = PasienModel::select('tgl_mens_terakhir')->where('id', $id)->first();
            
            //start semester 1 dan end semester 1
            $startS1 = Carbon::parse($hpht->tgl_mens_terakhir);
            $endS1 = $startS1->copy()->addMonths(3);            

            //start semester 2 dan end semester 2
            $startS2 = $endS1->copy()->addDays(1);
            $endS2 = $startS2->copy()->addMonths(3);

                                                
            //start semester 3 dan end semester 3
            $startS3 = $endS2->copy()->addDays(1);
            $tgl_perkiraan_melahirkan = PasienModel::select('tgl_perkiraan_melahirkan')->where('id', $id)->first();
            $endS3 = Carbon::parse($tgl_perkiraan_melahirkan->tgl_perkiraan_melahirkan);

            //today
            $tgl_pemeriksaan = PemeriksaanKehamilanModel::select('tanggal_pemeriksaan')->where('id', $pKehamilan->id)->first();
            
            $today = Carbon::parse($tgl_pemeriksaan->tanggal_pemeriksaan);
            
            //kondisi semester 1 
            if($startS1 < $today && $today <= $endS1){

                $status = PemeriksaanKehamilanModel::where('id', $pKehamilan->id)->update([            
                    'status' => '1',            
                ]);
                
                //status Pasien
                $statusPasien = PasienModel::where('id', $id)->update([
                    'status' => '1',
                ]);
            }
            //kondisi semster 2
            if($startS2 < $today && $today <= $endS2){
                $status = PemeriksaanKehamilanModel::where('id', $pKehamilan->id)->update([            
                    'status' => '2',            
                ]);
                
                //status Pasien
                $statusPasien = PasienModel::where('id', $id)->update([
                    'status' => '2',
                ]);
            }
            //kondisi semester 3
            if($startS3 < $today && $today < $endS3){

                $status = PemeriksaanKehamilanModel::where('id', $pKehamilan->id)->update([            
                    'status' => '3',            
                ]);
                
                //status Pasien
                $statusPasien = PasienModel::where('id', $id)->update([
                    'status' => '3',
                ]);

            }               

            //kodisi masuk nifas
            if($today >= $endS3){

                $status = PemeriksaanKehamilanModel::where('id', $pKehamilan->id)->update([            
                    'status' => '3',            
                ]);
                
                //status Pasien
                $statusPasien = PasienModel::where('id', $id)->update([
                    'status' => '4',
                ]);

            }               
                                    
        Session::flash('success', 'Data Berhasil Diupdate!');                                      
          return redirect()->route('listpasien');

    }
    

    public function riwayatPemeriksaan()
    {
        $pasien = PasienModel::select('*')->orderBy('status')->get();              
        
        return view('admin/riwayatPemeriksaanKehamilan', ['pasien' => $pasien]);
    }

    public function detailPemeriksaan($id){
        //dd($id);
        $PemeriksaanById = PemeriksaanKehamilanModel::select('pemeriksaan_kehamilan_models.id as id','tanggal_pemeriksaan', 'tekanan_darah', 'keluhan','berat_badan', 'tinggi_rahim', 'posisi', 'tgl_mens_terakhir', 'semester')->join('letak_janin_models', 'pemeriksaan_kehamilan_models.letak_janin', '=', 'letak_janin_models.id')->join('pasien_models', 'pasien_models.id', '=', 'pemeriksaan_kehamilan_models.pasien_id')->join('semester_models', 'semester_models.id', '=', 'pemeriksaan_kehamilan_models.status')->where('pasien_id', $id)->get();
                
        return view('admin/detailRiwayatKehamilan', ['pkh' => $PemeriksaanById]);
    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {   
        $janin = LetakJaninModel::get();
        $pkh = PemeriksaanKehamilanModel::select('pemeriksaan_kehamilan_models.id as id','tanggal_pemeriksaan', 'berat_badan', 'tekanan_darah', 'lingkar_lengan', 'tinggi_rahim', 'letak_janin', 'denyut_jantung_janin', 'hasil_konsultasi', 'keluhan', 'kaki_bengkak', 'tindakan', 'anemia', 'hiv', 'sivilis', 'tanggal_kembali','pemeriksaan_kehamilan_models.id as id')->join('letak_janin_models', 'letak_janin_models.id', '=', 'pemeriksaan_kehamilan_models.letak_janin')->join('hasil_labs', 'hasil_labs.pemeriksaan_kehamilan_id', '=', 'pemeriksaan_kehamilan_models.id')->where('pemeriksaan_kehamilan_models.id', $id)->first();             
        //dd($pkh);   
        return view('admin/editPemeriksaanKehamilan', ['pkh' => $pkh, 'janin' => $janin]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $pKehamilan = PemeriksaanKehamilanModel::where('id', $id)->update([                            
            'tanggal_pemeriksaan' => $request->tanggal_pemeriksaan,
            'berat_badan' => $request->berat_badan,
            'tekanan_darah' => $request->tekanan_darah,
            'lingkar_lengan' => $request->lingkar_lengan,
            'tinggi_rahim' => $request->tinggi_rahim,
            'tanggal_kembali' => $request->tanggal_kembali,
            'letak_janin' => $request->letak_janin,                
            'hasil_konsultasi' => $request->hasil_konsultasi,
            'keluhan' => $request->keluhan,
            'status' => 1,                   
            'denyut_jantung_janin' => $request->denyut_jantung_janin,
            'kaki_bengkak' => $request->kaki_bengkak,
            'tindakan' => $request->tindakan,                                
        ]);

        $hasilLab = HasilLab::where('pemeriksaan_kehamilan_id', $id)->update([            
            'anemia' => $request->anemia,
            'hiv'=> $request->hiv,
            'sivilis' => $request->sifilis
        ]);
        
        Session::flash('success', 'Data Berhasil Diupdate!');                                      
          return redirect()->route('riwayatPemeriksaanKehamilan');
        

    }

    public function downloadPDF($id)
    {
    
    //Pemeriksaan Kehamilan Rutin  as data
    $pkh  = PasienModel::select('pemeriksaan_kehamilan_models.tanggal_pemeriksaan as tanggal_pemeriksaan', 'pemeriksaan_kehamilan_models.tekanan_darah as tekanan_darah', 'pemeriksaan_kehamilan_models.keluhan as keluhan','pemeriksaan_kehamilan_models.berat_badan as berat_badan', 'pemeriksaan_kehamilan_models.tinggi_rahim as tinggi_rahim', 'pemeriksaan_kehamilan_models.denyut_jantung_janin as denyut_jantung_janin', 'pemeriksaan_kehamilan_models.kaki_bengkak as kaki_bengkak', 'pemeriksaan_kehamilan_models.tindakan as tindakan', 'pemeriksaan_kehamilan_models.hasil_konsultasi as hasil_konsultasi', 'pemeriksaan_kehamilan_models.tanggal_kembali as tanggal_kembali', 
    'letak_janin_models.posisi as posisi','hasil_labs.anemia as hasil_labs', 'bidans.name as bidan_nama', 'pasien_models.tgl_mens_terakhir as hpht')    
    ->join('pemeriksaan_kehamilan_models', 'pemeriksaan_kehamilan_models.pasien_id', '=', 'pasien_models.id')
    ->join('bidans', 'bidans.id', '=', 'pasien_models.bidan_id')
    ->join('letak_janin_models', 'letak_janin_models.id', '=', 'pemeriksaan_kehamilan_models.letak_janin')
    ->join('hasil_labs', 'hasil_labs.pemeriksaan_kehamilan_id', '=', 'pemeriksaan_kehamilan_models.id')        
    ->where('pasien_id', $id)->get();

    //Pemeriksaan Kehamilan Pertama
    $pkp = PemeriksaanKehamilanPertama::select('pemeriksaan_kehamilan_pertamas.kontrasepsi as kontrasepsi', 'pemeriksaan_kehamilan_pertamas.riwayat_penyakit as riwayat_penyakit',  'pemeriksaan_kehamilan_pertamas.riwayat_alergi as riwayat_alergi', 'kek_models.jenis as kek', 'pemeriksaan_kehamilan_pertamas.jumlah_kehamilan as jumlah_kehamilan', 'pemeriksaan_kehamilan_pertamas.jumlah_persalinan as jumlah_persalinan', 'pemeriksaan_kehamilan_pertamas.jumlah_keguguran as jumlah_keguguran', 'pemeriksaan_kehamilan_pertamas.jumlah_anak_hidup as jumlah_anak_hidup', 'pemeriksaan_kehamilan_pertamas.jumlah_anak_lahir_mati as jumlah_anak_lahir_mati', 'pemeriksaan_kehamilan_pertamas.jumlah_anak_lahir_prematur as jumlah_anak_lahir_prematur', 'persalinan_terakhir_models.jenis as persalinan_terakhir', 'pemeriksaan_kehamilan_pertamas.imunisasi_TT as imunisasiTT', 'golongan_darah_models.darah as golongan_darah', 'pasien_models.tgl_mens_terakhir as hpht', 'pasien_models.tgl_perkiraan_melahirkan as hpl', 'pasien_models.nama as nama', 'pasien_models.tinggi_badan as tinggi_badan', 'bidans.name as nama_bidan')
    ->join('pasien_models', 'pasien_models.id', '=', 'pemeriksaan_kehamilan_pertamas.pasien_id')
    ->join('kek_models', 'kek_models.id', 'pemeriksaan_kehamilan_pertamas.kek_id')
    ->join('golongan_darah_models','golongan_darah_models.id', '=', 'pasien_models.golongan_darah')
    ->join('persalinan_terakhir_models', 'persalinan_terakhir_models.id','=', 'pemeriksaan_kehamilan_pertamas.persalinan_terakhir')
    ->join('bidans', 'bidans.id', '=', 'pasien_models.bidan_id')
    ->where('pasien_id', $id)->first();

    //dd($pkp);

        
    
        $pdf = PDF::loadView('pdf/catatanKehamilan', ['data' => $pkh, 'pkp' => $pkp]);
        return $pdf->setPaper('a3', 'landscape')->stream('catatanKehamilan.pdf');
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function test()
    {
        $pkh = PemeriksaanKehamilanModel::select('tanggal_pemeriksaan', 'keluhan', 'tanggal_kembali')->where('pasien_id', 1)->get();
        return view('admin/test', ['pkh' => $pkh]);
    }
    
}