<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class KesimpulanNifasModel extends Model
{
    protected $fillable = [
        'pemeriksaan_nifas_id',
        'keadaan',
    ];
}