<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class GejalaModel extends Model
{
    protected $fillable =   [
        'kode_gejala',
        'nama',
        'keterangan'
    ];
    protected $primaryKey = 'kode_gejala';
    protected $keyType = 'string';
}

//     public function penyakits(){
//         return $this->belongsToMany('App\PenyakitModel');
//     }
// }