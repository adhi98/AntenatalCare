<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DiagnosaModel extends Model
{
    protected $fillable =   [
        'id_pasien',
        'kode_penyakit',
        'tanggal',
        'status'
    ];

    public function penyakits(){
        return $this->belongsTo('App\PenyakitModel');
    }

    public function pasiens(){
        return $this->belongsTo('App\PasienModel');
    }
}
