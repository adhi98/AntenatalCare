<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class GolonganDarahModel extends Model
{
    protected $fillable = [
        'darah',
    ];
}