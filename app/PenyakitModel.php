<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PenyakitModel extends Model
{
    protected $fillable =   [
        'kode_penyakit',
        'nama',
        'definisi',
        'faktor',
        'pencegahan_umum',
        'pencegahan_khusus',
        'fase'
    ];

    public function gejalas(){
        return $this->belongsToMany('App\GejalaModel');
    }

    public function diagnosas_penyakit()
    {
        return $this->hasMany('App\DiagnosaModel');
    }
}
