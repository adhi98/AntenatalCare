<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PemeriksaanNifasModel extends Model
{
    protected $fillable = [
        'pasien_id',                        
        'lokhia',
        'jalan_lahir',        
        'nasihat',
        'status',
        'tgl_awal_nifas',
        'tgl_akhir_nifas',
        'tgl_pemeriksaan',
        'tgl_kembali',
        'tekanan_darah',
        'tinggi_fundus',
        'pemeriksaan_payudara',
    ];
}