<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class notifikasiNifasModel extends Model
{
    protected $fillable= [
        'pemeriksan_nifas_id',
        'pasien_id',
    ];
}