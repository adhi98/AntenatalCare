<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('admin/login');
});

Auth::routes();


View::composer(['*'], function($view){
    $Bidan_nama = Auth::user();
    $view->with('Bidan_nama', $Bidan_nama);
});


Route::group(['middleware' => ['auth']], function () {

    Route::get('logout', '\App\Http\Controllers\Auth\LoginController@logout');
    Route::get('/dashboard', 'BidanController@index')->name('dashboard');
    Route::get('/pasienbaru', 'PasienController@index')->name('pasienbaru');
    Route::post('/pasienbaru/daftar', 'PasienController@store')->name('pasienbaruDaftar');
    Route::get('/listpasien', 'PasienController@listPasien')->name('listpasien');
    Route::get('/listpasien/edit/{id}', 'PasienController@edit')->name('listpasienEdit');
    Route::post('/listpasien/update/{id}', 'PasienController@update')->name('listpasienUpdate');
    Route::get('/listpasien/pemeriksaankehamilan/{id}', 'PemeriksaanKehamilanController@index')->name('pemeriksaankehamilan');
    Route::post('/listpasien/pemeriksaankehamilan/simpan/{id}', 'PemeriksaanKehamilanController@store')->name('pemeriksaankehamilanSave');
    Route::get('/listpasien/pemeriksaannifas/{id}', 'PemeriksaanNifasController@index')->name('pemeriksaannifas');
    Route::post('/listpasien/pemeriksaannifas/simpan/{id}', 'PemeriksaanNifasController@store')->name('pemeriksaannifasSave');
    Route::get('/riwayat/pemeriksaan/kehamilan', 'PemeriksaanKehamilanController@riwayatPemeriksaan')->name('riwayatPemeriksaanKehamilan');
    Route::get('/riwayat/pemeriksaan/kehamilan/detail/{id}', 'PemeriksaanKehamilanController@detailPemeriksaan')->name('detailRiwayatPemeriksaanKehamilan');

    Route::get('/edit/pemeriksaan/kehamilan/{id}', 'PemeriksaanKehamilanController@edit')->name('editDetailRiwayatPemeriksaanKehamilan');
    Route::post('/riwayat/pemeriksaan/kehamilan/detail/update/{id}', 'PemeriksaanKehamilanController@update')->name('updateDetailRiwayatPemeriksaanKehamilan');

    Route::get('/riwayat/pemeriksaan/kehamilan/download/{id}', 'PemeriksaanKehamilanController@downloadPDF')->name('catatanKehamilan');

    Route::post('/listpasien/pemeriksaankehamilanpertama/simpan/{id}', 'PemeriksaanKehamilanController@firststore')->name('pemeriksaankehamilanpertamaSave');



    //nifas
    Route::get('/riwayat/pemeriksaan/nifas', 'PemeriksaanNifasController@riwayatPemeriksaan')->name('riwayatPemeriksaanNifas');
    Route::get('/riwayat/pemeriksaan/nifas/detail/{id}', 'PemeriksaanNifasController@detailPemeriksaan')->name('detailRiwayatPemeriksaanNifas');
    Route::get('/edit/pemeriksaan/nifas/{id}', 'PemeriksaanNifasController@edit')->name('editRiwayatPemeriksaanNifas');
    Route::post('/update/pemeriksaan/nifas/{id}', 'PemeriksaanNifasController@update')->name('updateRiwayatPemeriksaanNifas');
    Route::get('/riwayat/pemeriksaan/nifas/download/{id}', 'PemeriksaanNifasController@downloadPDF')->name('catatanNifas');

});

Route::get('/test', 'PemeriksaanKehamilanController@test');