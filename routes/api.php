<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('login', 'API\PasienControllerApi@login');
Route::post('pemeriksaankehamilanbyId', 'API\PasienControllerApi@pemeriksaankehamilanbyId');
Route::post('detailpemeriksaankehamilanbyId', 'API\PasienControllerApi@detailpemeriksaankehamilanbyId');
Route::post('grafik/beratbadan', 'API\PasienControllerApi@grafikBeratBadan');
Route::post('grafik/lila', 'API\PasienControllerApi@grafikLila');
Route::post('update/password', 'API\PasienControllerApi@updatePassword');
Route::post('umur/kehamilan', 'API\PasienControllerApi@umurKehamilanbyId');
Route::post('grafik/tinggirahim', 'API\PasienControllerApi@grafikTinggiRahim');
Route::post('notifikasi/kehamilan', 'API\PasienControllerApi@notifikasiKehamilan');
Route::post('notifikasi/nifas', 'API\PasienControllerApi@notifikasiNifas');
Route::post('pemeriksaannifasbyId', 'API\PasienControllerApi@pemeriksaannifasbyId');
Route::post('detailpemeriksaannifasbyId', 'API\PasienControllerApi@detailpemeriksaannifasbyId');
Route::post('konsultasi/nifas', 'API\PasienControllerApi@konsultasiNifasbyId');
Route::post('biodata', 'API\PasienControllerApi@Biodata');
Route::post('list/penyakit', 'API\PasienControllerApi@ListPenyakit');
Route::post('detail/penyakit', 'API\PasienControllerApi@DetailPenyakit');

Route::get('test', 'API\PasienControllerApi@test');

//bagian SP
Route::post('proses','API\RelasiControllerApi@proses');
Route::post('hasil_pemeriksaan_diagnosa','API\PasienControllerApi@hasil_pemeriksaan_diagnosa');
Route::post('simpan','API\RelasiControllerApi@simpan');
Route::post('riwayat','API\PasienControllerApi@riwayat');