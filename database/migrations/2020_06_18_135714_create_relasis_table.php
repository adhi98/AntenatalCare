<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRelasisTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('relasis', function (Blueprint $table) {
            $table->increments('id');
            // $table->string('kode_penyakit');
            $table->string('kode_penyakit', 10)->references('kode_penyakit')->on('penyakit_models');
            // $table->string('kode_gejala');
            $table->string('kode_gejala', 10)->references('kode_gejala')->on('gejala_models');
            $table->timestamps();
        });

        // Schema::table('relasis', function (Blueprint $table) {
            // $table->foreign('kode_penyakit')->references('kode_penyakit')->on('penyakit_models')->onDelete('cascade');
            // $table->foreign('kode_gejala')->references('kode_gejala')->on('gejala_models')->onDelete('cascade');
        //   });
    }

    public function gejala_models(){
        return $this->belongsTo('gejala_models', 'kode_gejala');
    }

    public function penyakit_models(){
        return $this->belongsTo('penyakit_models', 'kode_penyakit');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('relasis');
    }
}
