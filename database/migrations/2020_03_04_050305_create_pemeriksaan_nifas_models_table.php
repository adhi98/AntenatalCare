<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePemeriksaanNifasModelsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pemeriksaan_nifas_models', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('pasien_id')->unsigned();            
            $table->string('lokhia');
            $table->string('tekanan_darah');
            $table->string('tinggi_fundus');
            $table->string('jalan_lahir');            
            $table->string('pemeriksaan_payudara');
            $table->string('nasihat');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pemeriksaan_nifas_models');
    }
}