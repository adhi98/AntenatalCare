<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePenyakitModelsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('penyakit_models', function (Blueprint $table) {
            $table->string('kode_penyakit',10)->primary()->index();
            $table->string('nama',100);
            $table->text('definisi');
            $table->text('faktor');
            $table->text('pencegahan_umum');
            $table->text('pencegahan_khusus');
            $table->char('fase',10);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('penyakit_models');
    }
}
