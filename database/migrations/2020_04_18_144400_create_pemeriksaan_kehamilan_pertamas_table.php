<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePemeriksaanKehamilanPertamasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pemeriksaan_kehamilan_pertamas', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('pasien_id');
            $table->string('kontrasepsi');
            $table->string('riwayat_penyakit');
            $table->string('riwayat_alergi');
            $table->integer('kek_id');
            $table->integer('jumlah_kehamilan');
            $table->integer('jumlah_persalinan');
            $table->integer('jumlah_keguguran');
            $table->integer('jumlah_anak_hidup');
            $table->integer('jumlah_anak_lahir_mati');
            $table->integer('jumlah_anak_lahir_prematur');
            $table->integer('persalinan_terakhir');
            $table->string('imunisasi_TT');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pemeriksaan_kehamilan_pertamas');
    }
}