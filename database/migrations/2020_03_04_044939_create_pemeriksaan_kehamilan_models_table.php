<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePemeriksaanKehamilanModelsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pemeriksaan_kehamilan_models', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('pasien_id')->unsigned();            
            $table->date('tanggal_pemeriksaan');
            $table->float('berat_badan');
            $table->string('tekanan_darah');
            $table->float('lingkar_lengan');
            $table->float('tinggi_rahim');                        
            $table->date('tanggal_kembali');
            $table->integer('letak_janin');
            $table->string('denyut_jantung_janin');            
            $table->string('hasil_konsultasi');
            $table->string('keluhan');            
            $table->string('tindakan');
            $table->string('kaki_bengkak');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pemeriksaan_kehamilan_models');
    }
}