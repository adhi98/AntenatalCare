<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDiagnosaModelsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('diagnosa_models', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_pasien')->unsigned();
            $table->string('kode_penyakit');
            $table->date('tanggal');
            $table->integer('status');
            $table->timestamps();
        });

        Schema::table('diagnosa_models', function (Blueprint $table) {
            $table->foreign('kode_penyakit')->references('kode_penyakit')->on('penyakit_models')->onDelete('cascade');
            // $table->foreign('id_pasien')->references('id')->on('pasien_models')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('diagnosa_models');
    }
}
