<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddStatusPemeriksaanNifas extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('pemeriksaan_nifas_models', function (Blueprint $table) {
            $table->integer('status')->unsigned();
            $table->date('tgl_pemeriksaan');
            $table->date('tgl_kembali');
            $table->date('tgl_awal_nifas');
            $table->date('tgl_akhir_nifas');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('pemeriksaan_nifas_models', function (Blueprint $table) {
            $table->dropColumn('status');
        });
    }
}