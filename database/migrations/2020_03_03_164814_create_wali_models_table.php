<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWaliModelsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('wali_models', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nama');
            $table->date('tanggal_lahir');
            $table->integer('agama_id')->unsigned();
            $table->string('golongan_darah');
            $table->integer('pendidikan_id')->unsigned();
            $table->integer('pekerjaan_id')->unsigned();
            $table->bigInteger('no_tlp');
            $table->bigInteger('no_ktp');
            $table->string('alamat');
            $table->integer('pasien_id')->unsigned();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('wali_models');
    }
}