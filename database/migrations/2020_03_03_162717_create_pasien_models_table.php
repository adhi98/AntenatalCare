<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePasienModelsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pasien_models', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nama');
            $table->date('tanggal_lahir');
            $table->integer('agama_id')->unsigned();
            $table->integer('bidan_id')->unsigned();
            $table->integer('golongan_darah');
            $table->integer('pekerjaan_id')->unsigned();
            $table->bigInteger('no_tlp');
            $table->bigInteger('no_ktp');
            $table->bigInteger('no_jkn');
            $table->bigInteger('tinggi_badan')->nullable();
            $table->bigInteger('no_pasien');
            $table->string('password');
            $table->string('alamat');
            $table->date('tgl_mens_terakhir')->nullable();            
            $table->date('tgl_perkiraan_melahirkan')->nullable();            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pasien_models');
    }
}