<?php

use Illuminate\Database\Seeder;
use App\GejalaModel;
use Carbon\Carbon;

class GejalaDataTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        GejalaModel::create([
            'kode_gejala' => 'GK01',
            'nama' => 'Mual dan muntah (lebih dari 3-4 kali sehari)',
            'keterangan' => '',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);

        GejalaModel::create([
            'kode_gejala' => 'GK02',
            'nama' => 'Perut nyeri dan kaku',
            'keterangan' => '',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);

        GejalaModel::create([
            'kode_gejala' => 'GK03',
            'nama' => 'Pendarahan dalam jumlah banyak (sehari 3 kali)',
            'keterangan' => '',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);

        GejalaModel::create([
            'kode_gejala' => 'GK04',
            'nama' => 'Ukuran rahim lebih besar dari biasanya (standar pemeriksaan kehamilan)',
            'keterangan' => '',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);

        GejalaModel::create([
            'kode_gejala' => 'GK05',
            'nama' => 'Tekanan darah >= 140/90 mmHg',
            'keterangan' => '',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);

        GejalaModel::create([
            'kode_gejala' => 'GK06',
            'nama' => 'Berat badan turun secara signifikan (>5% dari berat sebelum hamil)',
            'keterangan' => '',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);

        GejalaModel::create([
            'kode_gejala' => 'GK07',
            'nama' => 'Dehidrasi',
            'keterangan' => '',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);

        GejalaModel::create([
            'kode_gejala' => 'GK08',
            'nama' => 'Ketonuria (terdapat banyak zat keton pada air seni)',
            'keterangan' => '',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);

        GejalaModel::create([
            'kode_gejala' => 'GK09',
            'nama' => 'Merasa lesu dan tidak bertenaga',
            'keterangan' => '',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);

        GejalaModel::create([
            'kode_gejala' => 'GK10',
            'nama' => 'Ukuran uterus/rahim lebih kecil dari biasanya (standar pemeriksaan kehamilan)',
            'keterangan' => '',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);

        GejalaModel::create([
            'kode_gejala' => 'GK11',
            'nama' => 'Keluar jaringan berbentuk seperti anggur',
            'keterangan' => '',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);

        GejalaModel::create([
            'kode_gejala' => 'GK12',
            'nama' => 'Detak jantung meningkat drastis',
            'keterangan' => '',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);

        GejalaModel::create([
            'kode_gejala' => 'GK13',
            'nama' => 'Sering terjadi pingsan',
            'keterangan' => '',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);

        GejalaModel::create([
            'kode_gejala' => 'GK14',
            'nama' => 'Muka atau kulit pucat',
            'keterangan' => '',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);

        GejalaModel::create([
            'kode_gejala' => 'GK15',
            'nama' => 'Tekanan darah rendah/hipotensi',
            'keterangan' => '',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);

        GejalaModel::create([
            'kode_gejala' => 'GK16',
            'nama' => 'Nyeri pada perut bawah/abdomen, leher, atau pundak',
            'keterangan' => '',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);

        GejalaModel::create([
            'kode_gejala' => 'GK17',
            'nama' => 'Pendarahan tidak nyeri',
            'keterangan' => '',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);

        GejalaModel::create([
            'kode_gejala' => 'GK18',
            'nama' => 'Syok akibat pendarahan dalam jumlah banyak',
            'keterangan' => '',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);

        GejalaModel::create([
            'kode_gejala' => 'GK19',
            'nama' => 'Tidak ada kontraksi pada uterus/rahim',
            'keterangan' => '',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);

        GejalaModel::create([
            'kode_gejala' => 'GK20',
            'nama' => 'Anemia',
            'keterangan' => '',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);

        GejalaModel::create([
            'kode_gejala' => 'GK21',
            'nama' => 'Hilangnya denyut jantung janin',
            'keterangan' => '',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);

        GejalaModel::create([
            'kode_gejala' => 'GK22',
            'nama' => 'Kontraksi rahim tegang terus menerus dan nyeri',
            'keterangan' => '',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);

        GejalaModel::create([
            'kode_gejala' => 'GK23',
            'nama' => 'Takikardia (mengalami peningkatan denyut jantung janin sebesar >160 kali/menit)',
            'keterangan' => '',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);

        GejalaModel::create([
            'kode_gejala' => 'GK24',
            'nama' => 'Terdapat riwayat penyakit hipertensi sebelum kehamilan',
            'keterangan' => '',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);

        GejalaModel::create([
            'kode_gejala' => 'GK25',
            'nama' => 'Nyeri ulu hati',
            'keterangan' => '',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);

        GejalaModel::create([
            'kode_gejala' => 'GK26',
            'nama' => 'Pusing atau sakit kepala berat',
            'keterangan' => '',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);

        GejalaModel::create([
            'kode_gejala' => 'GK27',
            'nama' => 'Bengkak pada tangan, wajah, dan beberapa bagian tubuh lainnya',
            'keterangan' => '',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);

        GejalaModel::create([
            'kode_gejala' => 'GK28',
            'nama' => 'Gangguan penglihatan, seperti pandangan kabur atau sensitive terhadap cahaya',
            'keterangan' => '',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);

        GejalaModel::create([
            'kode_gejala' => 'GK29',
            'nama' => 'Frekuensi buang air kecil dan volume urin menurun',
            'keterangan' => '',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);

        GejalaModel::create([
            'kode_gejala' => 'GK30',
            'nama' => 'Sesak nafas',
            'keterangan' => '',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);

        GejalaModel::create([
            'kode_gejala' => 'GK31',
            'nama' => 'Hipertensi',
            'keterangan' => '',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);

        GejalaModel::create([
            'kode_gejala' => 'GK32',
            'nama' => 'Batuk berbusa',
            'keterangan' => '',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);

        GejalaModel::create([
            'kode_gejala' => 'GK33',
            'nama' => 'Penurunan tingkat kesadaran',
            'keterangan' => '',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);

        GejalaModel::create([
            'kode_gejala' => 'GK34',
            'nama' => 'Pembengkakan pada kaki atau perut',
            'keterangan' => '',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);

        GejalaModel::create([
            'kode_gejala' => 'GK35',
            'nama' => 'Keringetan berlebih',
            'keterangan' => '',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);

        GejalaModel::create([
            'kode_gejala' => 'GK36',
            'nama' => '36.	Pendarahan dalam jumlah sedang (sehari 1 kali)',
            'keterangan' => '',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);

        GejalaModel::create([
            'kode_gejala' => 'GK37',
            'nama' => 'Kejang-kejang',
            'keterangan' => '',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);

        GejalaModel::create([
            'kode_gejala' => 'GK38',
            'nama' => 'Demam tinggi >38 derajat',
            'keterangan' => '',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);

        GejalaModel::create([
            'kode_gejala' => 'GK39',
            'nama' => 'Frekuensi nadi ibu >100kali/menit',
            'keterangan' => '',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);

        GejalaModel::create([
            'kode_gejala' => 'GK40',
            'nama' => 'Nyeri pada bagian rahim',
            'keterangan' => '',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);

        GejalaModel::create([
            'kode_gejala' => 'GK41',
            'nama' => 'Vagina mengeluarkan keputihan dengan warna yang tidak biasa dan bau yang tidak sedap',
            'keterangan' => '',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);

        GejalaModel::create([
            'kode_gejala' => 'GK42',
            'nama' => 'Bentuk Rahim tidak seperti normalnya atau konturnya tidak jelas',
            'keterangan' => '',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);

        GejalaModel::create([
            'kode_gejala' => 'GK43',
            'nama' => 'Menggembung dibawah tulang kemaluan',
            'keterangan' => '',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);

        GejalaModel::create([
            'kode_gejala' => 'GK44',
            'nama' => 'Bagian-bagian janin mudah dipalpasi/diraba',
            'keterangan' => '',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);

        GejalaModel::create([
            'kode_gejala' => 'GN01',
            'nama' => 'Demam tinggi disertai dengan menggigil >38 derajat',
            'keterangan' => '',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);

        GejalaModel::create([
            'kode_gejala' => 'GN02',
            'nama' => 'Nyeri perut bawah',
            'keterangan' => '',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);

        GejalaModel::create([
            'kode_gejala' => 'GN03',
            'nama' => 'Nyeri uterus/rahim',
            'keterangan' => '',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);

        GejalaModel::create([
            'kode_gejala' => 'GN04',
            'nama' => 'Payudara terasa keras, memerah, dan nyeri',
            'keterangan' => '',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);

        GejalaModel::create([
            'kode_gejala' => 'GN05',
            'nama' => 'Lochia (pendarahan dalam jumlah kecil yang berwarna kekuningan) berbau',
            'keterangan' => '',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);

        GejalaModel::create([
            'kode_gejala' => 'GN06',
            'nama' => 'Subinvolusi uterus (setelah melahirkan, bentuk rahim tidak balik ke ukuran normal)',
            'keterangan' => '',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);

        GejalaModel::create([
            'kode_gejala' => 'GN07',
            'nama' => 'Pendarahan vagina',
            'keterangan' => '',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);

        GejalaModel::create([
            'kode_gejala' => 'GN08',
            'nama' => 'Syok',
            'keterangan' => '',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);

        GejalaModel::create([
            'kode_gejala' => 'GN09',
            'nama' => 'Perut Kembung',
            'keterangan' => '',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);

        GejalaModel::create([
            'kode_gejala' => 'GN10',
            'nama' => 'Respon buruk terhadap antibiotik',
            'keterangan' => '',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);

        GejalaModel::create([
            'kode_gejala' => 'GN11',
            'nama' => 'Pembekakan pada adneksa (nyeri daerah panggul) atau kavum douglas (Benjolan pada perut)',
            'keterangan' => '',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);

        GejalaModel::create([
            'kode_gejala' => 'GN12',
            'nama' => 'Trismus/keterbatasan pergerakan rahang dan pengunyahan',
            'keterangan' => '',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);

        GejalaModel::create([
            'kode_gejala' => 'GN13',
            'nama' => 'Bagian belakang leher / kuduk, wajah, dan perut terasa kaku',
            'keterangan' => '',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);

        GejalaModel::create([
            'kode_gejala' => 'GN14',
            'nama' => 'Punggung melengkung',
            'keterangan' => '',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);

        GejalaModel::create([
            'kode_gejala' => 'GN15',
            'nama' => 'Spasme/kontraksi otot yang tiba tiba muncul',
            'keterangan' => '',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);

        GejalaModel::create([
            'kode_gejala' => 'GN16',
            'nama' => 'Puting terlihat lecet',
            'keterangan' => '',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);

        GejalaModel::create([
            'kode_gejala' => 'GN17',
            'nama' => 'Kedua payudara terasa keras, memerah, dan nyeri',
            'keterangan' => '',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);
    }
}