<?php

use Illuminate\Database\Seeder;
use App\PersalinanTerakhirModel;

class PersalinanTerakhirDataSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {        
        PersalinanTerakhirModel::create([
            'jenis' => 'Spontan'
        ]);
        PersalinanTerakhirModel::create([
            'jenis' => 'Normal'
        ]);
        PersalinanTerakhirModel::create([
            'jenis' => 'Belum Pernah'
        ]);
    }
}