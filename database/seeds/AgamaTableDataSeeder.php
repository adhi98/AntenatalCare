<?php

use Illuminate\Database\Seeder;
use App\AgamaModel;
use Carbon\Carbon;

class AgamaTableDataSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        AgamaModel::create([            
            'agama' => 'Islam',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);

        AgamaModel::create([            
            'agama' => 'Nasrani',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);
        AgamaModel::create([            
            'agama' => 'Hindu',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);
        AgamaModel::create([            
            'agama' => 'Budha',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);
    }
}