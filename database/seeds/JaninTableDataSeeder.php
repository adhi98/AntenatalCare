<?php

use Illuminate\Database\Seeder;
use App\LetakJaninModel;

class JaninTableDataSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        LetakJaninModel::create([
            'posisi' => 'Kepala Bawah'
        ]);
        LetakJaninModel::create([
            'posisi' => 'Sungsang'
        ]);
        LetakJaninModel::create([
            'posisi' => 'Melintang'
        ]);
    }
}