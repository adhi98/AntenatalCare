<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(AgamaTableDataSeeder::class);
        $this->call(PekerjaanTableDataSeeder::class);
        $this->call(PendidikanTableDataSeeder::class);
        $this->call(SemesterTableDataSeeder::class);
        $this->call(BidanTableDataSeeder::class);
        $this->call(KEKTableDataSeeder::class);
        $this->call(GolonganDarahDataSeeder::class);
        $this->call(JaninTableDataSeeder::class);
        $this->call(PersalinanTerakhirDataSeeder::class);
        $this->call(GejalaDataTableSeeder::class);
        $this->call(PenyakitDataTableSeeder::class);
        $this->call(RelasiTableSeeder::class);
    }
}