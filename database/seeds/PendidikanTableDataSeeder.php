<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;
use App\PendidikanModel;

class PendidikanTableDataSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        PendidikanModel::create([            
            'pendidikan' => 'Sarjana',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);
        PendidikanModel::create([            
            'pendidikan' => 'SMA/SMK',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);
        PendidikanModel::create([            
            'pendidikan' => 'SMP',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);
        PendidikanModel::create([            
            'pendidikan' => 'SD',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);
        PendidikanModel::create([            
            'pendidikan' => 'Tidak Sekolah',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);
    }
}