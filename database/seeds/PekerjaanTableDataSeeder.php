<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;
use App\PekerjaanModel;

class PekerjaanTableDataSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        PekerjaanModel::create([            
            'pekerjaan' => 'Ibu Rumah Tangga',
            'status' => 'P',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);
        PekerjaanModel::create([            
            'pekerjaan' => 'PNS',
            'status' => 'P',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);        
        PekerjaanModel::create([            
            'pekerjaan' => 'PNS',
            'status' => 'w',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);
        PekerjaanModel::create([            
            'pekerjaan' => 'Karyawan Swasta',
            'status' => 'w',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);
        PekerjaanModel::create([            
            'pekerjaan' => 'Wirausaha',
            'status' => 'w',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);
    }
}