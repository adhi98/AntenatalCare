<?php

use Illuminate\Database\Seeder;
use App\SemesterModel;

class SemesterTableDataSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {        
        SemesterModel::create([
            'semester' => 'Trimester 1'
        ]);

        SemesterModel::create([
            'semester' => 'Trimester 2'
        ]);
        
        SemesterModel::create([
            'semester' => 'Trimester 3'
        ]);

        SemesterModel::create([
            'semester' => 'Nifas'
        ]);

        SemesterModel::create([
            'semester' => 'Selesai'
        ]);
    }
}