<?php

use Illuminate\Database\Seeder;
use App\User;

class BidanTableDataSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {           

        User::create([
            'name' => 'Sofia',
            'email' => 'sofia@gmail.com',
            'password' => bcrypt('sofia123')
        ]);

        User::create([
            'name' => 'Tia',
            'email' => 'tia@gmail.com',
            'password' => bcrypt('tia123')
        ]);

        User::create([
            'name' => 'Nurul',
            'email' => 'nurul@gmail.com',
            'password' => bcrypt('nurul123')
        ]);

    }
}