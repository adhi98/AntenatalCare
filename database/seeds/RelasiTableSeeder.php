<?php

use Illuminate\Database\Seeder;
use App\Relasi;
use Carbon\Carbon;


class RelasiTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Relasi::create([
            'kode_penyakit' => 'PK01',
            'kode_gejala' => 'GK01',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);

        Relasi::create([
            'kode_penyakit' => 'PK01',
            'kode_gejala' => 'GK06',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);

        Relasi::create([
            'kode_penyakit' => 'PK01',
            'kode_gejala' => 'GK07',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);

        Relasi::create([
            'kode_penyakit' => 'PK01',
            'kode_gejala' => 'GK08',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);

        Relasi::create([
            'kode_penyakit' => 'PK01',
            'kode_gejala' => 'GK09',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);

        Relasi::create([
            'kode_penyakit' => 'PK02',
            'kode_gejala' => 'GK02',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);

        Relasi::create([
            'kode_penyakit' => 'PK02',
            'kode_gejala' => 'GK03',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);

        Relasi::create([
            'kode_penyakit' => 'PK02',
            'kode_gejala' => 'GK10',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);

        Relasi::create([
            'kode_penyakit' => 'PK03',
            'kode_gejala' => 'GK01',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);

        Relasi::create([
            'kode_penyakit' => 'PK03',
            'kode_gejala' => 'GK02',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);

        Relasi::create([
            'kode_penyakit' => 'PK03',
            'kode_gejala' => 'GK03',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);

        Relasi::create([
            'kode_penyakit' => 'PK03',
            'kode_gejala' => 'GK04',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);

        Relasi::create([
            'kode_penyakit' => 'PK03',
            'kode_gejala' => 'GK11',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);

        Relasi::create([
            'kode_penyakit' => 'PK03',
            'kode_gejala' => 'GK12',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);

        Relasi::create([
            'kode_penyakit' => 'PK04',
            'kode_gejala' => 'GK13',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);

        Relasi::create([
            'kode_penyakit' => 'PK04',
            'kode_gejala' => 'GK14',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);

        Relasi::create([
            'kode_penyakit' => 'PK04',
            'kode_gejala' => 'GK15',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);

        Relasi::create([
            'kode_penyakit' => 'PK04',
            'kode_gejala' => 'GK16',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);

        Relasi::create([
            'kode_penyakit' => 'PK04',
            'kode_gejala' => 'GK48',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);

        Relasi::create([
            'kode_penyakit' => 'PK05',
            'kode_gejala' => 'GK03',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);

        Relasi::create([
            'kode_penyakit' => 'PK05',
            'kode_gejala' => 'GK17',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);

        Relasi::create([
            'kode_penyakit' => 'PK05',
            'kode_gejala' => 'GK18',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);

        Relasi::create([
            'kode_penyakit' => 'PK05',
            'kode_gejala' => 'GK19',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);

        Relasi::create([
            'kode_penyakit' => 'PK06',
            'kode_gejala' => 'GK18',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);

        Relasi::create([
            'kode_penyakit' => 'PK06',
            'kode_gejala' => 'GK20',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);

        Relasi::create([
            'kode_penyakit' => 'PK06',
            'kode_gejala' => 'GK21',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);

        Relasi::create([
            'kode_penyakit' => 'PK06',
            'kode_gejala' => 'GK22',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);

        Relasi::create([
            'kode_penyakit' => 'PK07',
            'kode_gejala' => 'GK05',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);

        Relasi::create([
            'kode_penyakit' => 'PK07',
            'kode_gejala' => 'GK24',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);

        Relasi::create([
            'kode_penyakit' => 'PK08',
            'kode_gejala' => 'GK05',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);

        Relasi::create([
            'kode_penyakit' => 'PK08',
            'kode_gejala' => 'GK24',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);

        Relasi::create([
            'kode_penyakit' => 'PK08',
            'kode_gejala' => 'GK25',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);

        Relasi::create([
            'kode_penyakit' => 'PK09',
            'kode_gejala' => 'GK01',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);

        Relasi::create([
            'kode_penyakit' => 'PK09',
            'kode_gejala' => 'GK05',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);

        Relasi::create([
            'kode_penyakit' => 'PK09',
            'kode_gejala' => 'GK26',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);

        Relasi::create([
            'kode_penyakit' => 'PK09',
            'kode_gejala' => 'GK27',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);

        Relasi::create([
            'kode_penyakit' => 'PK09',
            'kode_gejala' => 'GK28',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);

        Relasi::create([
            'kode_penyakit' => 'PK09',
            'kode_gejala' => 'GK29',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);

        Relasi::create([
            'kode_penyakit' => 'PK10',
            'kode_gejala' => 'GK01',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);

        Relasi::create([
            'kode_penyakit' => 'PK10',
            'kode_gejala' => 'GK05',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);

        Relasi::create([
            'kode_penyakit' => 'PK10',
            'kode_gejala' => 'GK26',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);

        Relasi::create([
            'kode_penyakit' => 'PK10',
            'kode_gejala' => 'GK27',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);

        Relasi::create([
            'kode_penyakit' => 'PK10',
            'kode_gejala' => 'GK28',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);

        Relasi::create([
            'kode_penyakit' => 'PK10',
            'kode_gejala' => 'GK29',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);

        Relasi::create([
            'kode_penyakit' => 'PK10',
            'kode_gejala' => 'GK38',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);

        Relasi::create([
            'kode_penyakit' => 'PK11',
            'kode_gejala' => 'GK30',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);

        Relasi::create([
            'kode_penyakit' => 'PK11',
            'kode_gejala' => 'GK31',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);

        Relasi::create([
            'kode_penyakit' => 'PK11',
            'kode_gejala' => 'GK32',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);

        Relasi::create([
            'kode_penyakit' => 'PK11',
            'kode_gejala' => 'GK33',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);

        Relasi::create([
            'kode_penyakit' => 'PK11',
            'kode_gejala' => 'GK34',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);

        Relasi::create([
            'kode_penyakit' => 'PK11',
            'kode_gejala' => 'GK35',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);

        Relasi::create([
            'kode_penyakit' => 'PK12',
            'kode_gejala' => 'GK23',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);

        Relasi::create([
            'kode_penyakit' => 'PK12',
            'kode_gejala' => 'GK39',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);

        Relasi::create([
            'kode_penyakit' => 'PK12',
            'kode_gejala' => 'GK40',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);

        Relasi::create([
            'kode_penyakit' => 'PK12',
            'kode_gejala' => 'GK41',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);

        Relasi::create([
            'kode_penyakit' => 'PK12',
            'kode_gejala' => 'GK42',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);

        Relasi::create([
            'kode_penyakit' => 'PK13',
            'kode_gejala' => 'GK02',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);

        Relasi::create([
            'kode_penyakit' => 'PK13',
            'kode_gejala' => 'GK18',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);

        Relasi::create([
            'kode_penyakit' => 'PK13',
            'kode_gejala' => 'GK19',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);

        Relasi::create([
            'kode_penyakit' => 'PK13',
            'kode_gejala' => 'GK21',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);

        Relasi::create([
            'kode_penyakit' => 'PK13',
            'kode_gejala' => 'GK23',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);

        Relasi::create([
            'kode_penyakit' => 'PK13',
            'kode_gejala' => 'GK43',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);

        Relasi::create([
            'kode_penyakit' => 'PK13',
            'kode_gejala' => 'GK44',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);

        Relasi::create([
            'kode_penyakit' => 'PK13',
            'kode_gejala' => 'GK45',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);

        Relasi::create([
            'kode_penyakit' => 'PN01',
            'kode_gejala' => 'GN01',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);

        Relasi::create([
            'kode_penyakit' => 'PN01',
            'kode_gejala' => 'GN02',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);

        Relasi::create([
            'kode_penyakit' => 'PN01',
            'kode_gejala' => 'GN03',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);

        Relasi::create([
            'kode_penyakit' => 'PN01',
            'kode_gejala' => 'GN05',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);

        Relasi::create([
            'kode_penyakit' => 'PN01',
            'kode_gejala' => 'GN06',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);

        Relasi::create([
            'kode_penyakit' => 'PN01',
            'kode_gejala' => 'GN07',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);

        Relasi::create([
            'kode_penyakit' => 'PN01',
            'kode_gejala' => 'GN08',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);

        Relasi::create([
            'kode_penyakit' => 'PN02',
            'kode_gejala' => 'GN01',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);

        Relasi::create([
            'kode_penyakit' => 'PN02',
            'kode_gejala' => 'GN02',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);

        Relasi::create([
            'kode_penyakit' => 'PN02',
            'kode_gejala' => 'GN03',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);

        Relasi::create([
            'kode_penyakit' => 'PN02',
            'kode_gejala' => 'GN09',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);

        Relasi::create([
            'kode_penyakit' => 'PN02',
            'kode_gejala' => 'GN10',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);

        Relasi::create([
            'kode_penyakit' => 'PN02',
            'kode_gejala' => 'GN11',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);

        Relasi::create([
            'kode_penyakit' => 'PN03',
            'kode_gejala' => 'GN12',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);

        Relasi::create([
            'kode_penyakit' => 'PN03',
            'kode_gejala' => 'GN13',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);

        Relasi::create([
            'kode_penyakit' => 'PN03',
            'kode_gejala' => 'GN14',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);

        Relasi::create([
            'kode_penyakit' => 'PN03',
            'kode_gejala' => 'GN15',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);

        Relasi::create([
            'kode_penyakit' => 'PN04',
            'kode_gejala' => 'GN01',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);

        Relasi::create([
            'kode_penyakit' => 'PN04',
            'kode_gejala' => 'GN04',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);

        Relasi::create([
            'kode_penyakit' => 'PN04',
            'kode_gejala' => 'GN16',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);

        Relasi::create([
            'kode_penyakit' => 'PN05',
            'kode_gejala' => 'GN04',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);

        Relasi::create([
            'kode_penyakit' => 'PN05',
            'kode_gejala' => 'GN17',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);

        Relasi::create([
            'kode_penyakit' => 'PN05',
            'kode_gejala' => 'GN18',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);
    }
}
