<?php

use Illuminate\Database\Seeder;
use App\GolonganDarahModel;

class GolonganDarahDataSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        GolonganDarahModel::create([
            'darah' => 'A',            
        ]);
        GolonganDarahModel::create([
            'darah' => 'B',            
        ]);
        GolonganDarahModel::create([
            'darah' => 'AB',            
        ]);
        GolonganDarahModel::create([
            'darah' => 'O',            
        ]);
    }
}