<?php

use Illuminate\Database\Seeder;
use App\PenyakitModel;
use Carbon\Carbon;

class PenyakitDataTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        PenyakitModel::create([
            'kode_penyakit' => 'PK01',
            'nama' => 'Mual dan Muntah pada Kehamilan',
            'definisi' => 'Mual dan muntah yang terjadi pada kehamilan hingga usia 16 minggu. Pada keadaan muntah-muntah yang berat, dapat terjadi dehidrasi, gangguan asambasa dan elektrolit dan ketosis; keadaan ini disebut hiperemesis gravidarum.',
            'faktor' => 'Terdapat perasaan emosi pada saat kehamilan dan terdapat perasaan stress pada saat kehamilan',
            'pencegahan_umum' => 'Anjurkan istirahat yang cukup dan hindari kelelahan.',
            'pencegahan_khusus' => 'Bila masih belum teratasi, tapi terjadi dehidrasi, datang dan periksa ke dokter',
            'fase' => 'Kehamilan',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);

        PenyakitModel::create([
            'kode_penyakit' => 'PK02',
            'nama' => 'Abortus',
            'definisi' => 'Abortus adalah ancaman atau pengeluaran hasil konsepsi sebelum janin dapat hidup di luar kandungan.',
            'faktor' => 'Faktor dari janin (fetal), yang terdiri dari: kelainan genetik (kromosom) dan faktor dari ibu (maternal), yang terdiri dari: penggunaan obatobatan, merokok, dan konsumsi alkohol',
            'pencegahan_umum' => 'Lakukan penilaian secara cepat mengenai keadaan umum ibu termasuk tanda-tanda vital (nadi, tekanan darah, pernapasan, suhu).',
            'pencegahan_khusus' => 'Lakukan konseling untuk menjelaskan kemungkinan risiko dan rasa tidak nyaman selama tindakan evakuasi',
            'fase' => 'Kehamilan',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);

        PenyakitModel::create([
            'kode_penyakit' => 'PK03',
            'nama' => 'Mola Hidatosa',
            'definisi' => 'Mola hidatidosa adalah bagian dari penyakit trofoblastik gestasional, yang disebabkan oleh kelainan pada villi khorionik yang disebabkan oleh proliferasi trofoblastik dan edem.',
            'faktor' => 'Usia – kehamilan terlalu muda dan tua, riwayat kehamilan mola sebelumnya, dan beberapa penelitian menunjukkan penggunaan kontraseptif oral',
            'pencegahan_umum' => 'Ibu harus dirujuk ke fasilitas kesehatan yang lebih lengkap.',
            'pencegahan_khusus' => 'Ibu dianjurkan menggunakan kontrasepsi hormonal bila masih ingin memiliki anak',
            'fase' => 'Kehamilan',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);

        PenyakitModel::create([
            'kode_penyakit' => 'PK04',
            'nama' => 'Kehamilan Ektopik Terganggu',
            'definisi' => 'Kehamilan ektopik adalah kehamilan yang terjadi di luar rahim (uterus).',
            'faktor' => 'Riwayat kehamilan ektopik sebelumnya, merokok, riwayat operasi di daerah tuba dan/atau tubektomi',
            'pencegahan_umum' => 'Segera rujuk ibu ke rumah sakit.',
            'pencegahan_khusus' => 'Lakukan konseling untuk penggunaan
            kontrasepsi.',
            'fase' => 'Kehamilan',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);

        PenyakitModel::create([
            'kode_penyakit' => 'PK05',
            'nama' => 'Plasenta Previa',
            'definisi' => 'kondisi ketika ari-ari atau plasenta berada di bagian bawah rahim, sehingga menutupi sebagian atau seluruh jalan lahir.',
            'faktor' => 'Kehamilan dengan ibu berusia lanjut, multiparitas atau kelahiran bayi hidup dua kali, riwayat seksio sesarea sebelumnya',
            'pencegahan_umum' => 'Segera rujuk ibu ke rumah sakit untuk lakukan penilaian jumlah perdarahan.',
            'pencegahan_khusus' => 'Rawat inap, tirah baring, dan lakukan pemeriksaan USG untuk memastikan letak plasenta.',
            'fase' => 'Kehamilan',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);

        PenyakitModel::create([
            'kode_penyakit' => 'PK06',
            'nama' => 'Solusio Plasenta',
            'definisi' => 'Terlepasnya plasenta dari tempat implantasinya.',
            'faktor' => 'Hipertensi, trauma Abdomen, hidramnion / kondisi air ketuban berlebihan',
            'pencegahan_umum' => 'Jika terjadi perdarahan hebat (nyata atau tersembunyi) dengan tanda-tanda awal syok pada ibu, lakukan persalinan.',
            'pencegahan_khusus' => '-',
            'fase' => 'Kehamilan',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);

        PenyakitModel::create([
            'kode_penyakit' => 'PK07',
            'nama' => 'Hipertensi Kronik',
            'definisi' => 'Hipertensi tanpa proteinuria yang timbul dari sebelum kehamilan dan menetap setelah persalinan.',
            'faktor' => 'Kehamilan kembar, diabetes melitus, obesitas sebelum hamil',
            'pencegahan_umum' => 'Anjurkan istirahat lebih banyak dan pantau pertumbuhan janin.',
            'pencegahan_khusus' => '-',
            'fase' => 'Kehamilan',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);

        PenyakitModel::create([
            'kode_penyakit' => 'PK08',
            'nama' => 'Hipertensi Gestasional',
            'definisi' => 'Hipertensi tanpa proteinuria yang timbul setelah kehamilan 20 minggu dan menghilang setelah persalinan.',
            'faktor' => 'Kehamilan kembar, diabetes melitus, obesitas sebelum hamil',
            'pencegahan_umum' => 'Pantau tekanan darah, urin (untuk proteinuria), dan kondisi janin setiap minggu.',
            'pencegahan_khusus' => '-',
            'fase' => 'Kehamilan',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);

        PenyakitModel::create([
            'kode_penyakit' => 'PK09',
            'nama' => 'Preeklamsia',
            'definisi' => ' sindrom yang ditandai dengan tekanan darah tinggi, kenaikan kadar protein di dalam urin (proteinuria), dan pembengkakan pada tungkai (edema).',
            'faktor' => '-',
            'pencegahan_umum' => 'Ibu hamil dengan preeklampsia harus segera dirujuk ke rumah sakit.',
            'pencegahan_khusus' => '-',
            'fase' => 'Kehamilan',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);

        PenyakitModel::create([
            'kode_penyakit' => 'PK10',
            'nama' => 'Eklamsia',
            'definisi' => 'komplikasi kehamilan yang ditandai tekanan darah tinggi dan kejang sebelum, selama, atau setelah persalinan.',
            'faktor' => '-',
            'pencegahan_umum' => 'Bila terjadi kejang, perhatikan jalan napas, pernapasan (oksigen), dan sirkulasi (cairan intravena).',
            'pencegahan_khusus' => '-',
            'fase' => 'Kehamilan',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);

        PenyakitModel::create([
            'kode_penyakit' => 'PK11',
            'nama' => 'Edema Paru',
            'definisi' => 'Edema paru adalah suatu kondisi yang ditandai dengan gejala sulit bernapas akibat terjadinya penumpukan cairan di dalam kantong paru-paru (alveoli).',
            'faktor' => '-',
            'pencegahan_umum' => 'Berikan oksigen.',
            'pencegahan_khusus' => '-',
            'fase' => 'Kehamilan',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);

        PenyakitModel::create([
            'kode_penyakit' => 'PK12',
            'nama' => 'Korioamnionitis',
            'definisi' => 'Korioamnionitis adalah infeksi pada korion dan amnion.',
            'faktor' => 'Alkohol, rokok, ketuban pecah lama',
            'pencegahan_umum' => 'Rujuk pasien ke rumah sakit.',
            'pencegahan_khusus' => 'Jika terdapat metritis (demam, cairan vagina berbau), berikan antibiotika di rumah sakit',
            'fase' => 'Kehamilan',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);

        PenyakitModel::create([
            'kode_penyakit' => 'PK13',
            'nama' => 'Ruptura Uteri',
            'definisi' => 'Ruptura uteri atau robeknya dinding rahim terjadi akibat terlampauinya daya regang miometrium.',
            'faktor' => '-',
            'pencegahan_umum' => 'Berikan oksigen.',
            'pencegahan_khusus' => 'Segera lakukan konusltasi ke dokter dengan fasilitas yang lengkap untuk memeriksa banyak titik',
            'fase' => 'Kehamilan',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);

        PenyakitModel::create([
            'kode_penyakit' => 'PN01',
            'nama' => 'Metritis',
            'definisi' => 'Metritis ialah infeksi pada uterus setelah persalinan. Keterlambatan terapi akan menyebabkan abses, peritonitis, syok, trombosis vena, emboli paru, infeksi panggul kronik, sumbatan tuba, infertilitas.',
            'faktor' => 'kurangnya tindakan aseptik saat melakukan tindakan, kurangnya higien pasien, kurangnya nutrisi',
            'pencegahan_umum' => 'Cegah Dehidrasi, jika masih merasakan hal yang sama segera rujuk ke dokter',
            'pencegahan_khusus' => '-',
            'fase' => 'Nifas',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);

        PenyakitModel::create([
            'kode_penyakit' => 'PN02',
            'nama' => 'Abses Pelvis',
            'definisi' => 'Abses pelvis adalah abses pada regio pelvis / bagian permukaan dari pintu bawah panggul.',
            'faktor' => 'kurangnya tindakan aseptik saat melakukan tindakan, kurangnya higien pasien, kurangnya nutrisi',
            'pencegahan_umum' => '-',
            'pencegahan_khusus' => 'Jika demam berkelanjutan, segera bawa ke dokter untuk diperiksa lebih lanjut',
            'fase' => 'Nifas',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);

        PenyakitModel::create([
            'kode_penyakit' => 'PN03',
            'nama' => 'Tetanus',
            'definisi' => 'Tetanus merupakan penyakit yang langka dan fatal yang mempengaruhi susunan saraf pusat dan menyebabkan kontraksi otot yang nyeri.',
            'faktor' => 'Imuniasasi tidak lengkap / tidak imunisasi, luka tusuk, dan sisa paku atau kayu yang menusuk tertinggal di dalam, adanya infeksi bakteri lainnya',
            'pencegahan_umum' => 'Rujuk ibu ke rumah sakit dengan fasilitas yang memadai',
            'pencegahan_khusus' => '-',
            'fase' => 'Nifas',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);

        PenyakitModel::create([
            'kode_penyakit' => 'PN04',
            'nama' => 'Mastitis',
            'definisi' => 'Mastitis adalah inflamasi atau infeksi payudara',
            'faktor' => 'Menyusui selama beberapa minggu setelah melahirkan, puting yang lecet, menyusui hanya pada satu posisi, sehingga drainase payudara tidak sempurna, menggunakan bra yang ketat dan menghambat aliran ASI, riwayat mastitis sebelumnya saat menyusui',
            'pencegahan_umum' => 'Disarankan untuk ibu lebih banyak berbaring',
            'pencegahan_khusus' => 'Dorong ibu untuk tetap menyusui, dimulai dengan payudara yang tidak sakit dan pompa asi ibu untuk keluar.',
            'fase' => 'Nifas',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);

        PenyakitModel::create([
            'kode_penyakit' => 'PN05',
            'nama' => 'Bendungan Payudara',
            'definisi' => 'Bendungan payudara adalah bendungan yang terjadi pada kelenjar payudara oleh karena ekspansi dan tekanan dari produksi dan penampungan ASI.',
            'faktor' => 'Posisi menyusui yang tidak baik, membatasi menyusui, membatasi waktu bayi dengan payudara, implan payudara',
            'pencegahan_umum' => 'Kompres payudara dengan menggunakan kain basah/hangat selama 5 menit.',
            'pencegahan_khusus' => '-',
            'fase' => 'Nifas',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);
    }
}
