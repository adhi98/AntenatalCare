<?php

use Illuminate\Database\Seeder;
use App\KekModel;

class KEKTableDataSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        KekModel::create([
            'jenis' => 'KEK',
        ]);
        KekModel::create([
            'jenis' => 'NON-KEK',
        ]);
    }
}