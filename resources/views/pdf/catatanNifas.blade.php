<!DOCTYPE html>
<html lang="en">

<head>
    <title>Document</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
        integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
</head>

<body>
    <style type="text/css">
    table tr td,
    table tr th {
        font-size: 9pt;
    }
    </style>

    <center>
        <h5>Catatan Pelayanan Kesehatan Ibu Nifas (Diisi oleh petugas)</h5>
        <h6>Atas Nama {{$pasien->nama}}</h6>
    </center>

    <!-- <div style="border: 1px solid green; width:400px; padding:5px, height:200px; background-color:yellow">
        <p>Nama Pasien : {{$pasien->nama}}</p>
        <p>Haid Pertama Haid Terakhir (HPHT), tanggal {{$pasien->tgl_mens_terakhir}}</p>
        <p>Hari Taksiran Persalinan (HTP), tanggal {{$pasien->tgl_perkiraan_melahirkan}}</p>
        <p>Tinggi Badan {{$pasien->tinggi_badan}} cm</p>
    </div> -->

    <table class='table table-bordered mt-4 table-sm'>
        <thead class="thead-light">
            <tr>
                <th>No</th>
                <th>Tanggal</th>
                <th>Tekanan Darah</th>
                <th>Lokhia</th>
                <th>Jalan Lahir</th>
                <th>Tinggi Fundus</th>
                <th>Pemeriksaan Payudara</th>
                <th>Catatan Bidan</th>
            </tr>
        </thead>
        <tbody>
            @foreach($data as $d)
            <tr>
                <td>{{$loop->iteration}}</td>
                <td>{{$d->tgl_pemeriksaan}}</td>
                <td>{{$d->tekanan_darah}}</td>
                <td>{{$d->lokhia}}</td>
                <td>{{$d->jalan_lahir}}</td>
                <td>{{$d->tinggi_fundus}}</td>
                <td>{{$d->pemeriksaan_payudara}}</td>
                <td>{{$d->nasihat}}</td>
            </tr>
            @endforeach
        </tbody>
    </table>

    <div class="mt-5">
        <h7>Kesimpulan Akhir Nifas : {{$kesimpulan->keadaan}}</h7>
    </div>


</body>

</html>