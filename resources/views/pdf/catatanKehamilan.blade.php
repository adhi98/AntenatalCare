<!DOCTYPE html>
<html lang="en">

<head>
    <title>Document</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
        integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
</head>

<body>
    <style type="text/css">
    table tr td,
    table tr th {
        font-size: 9pt;
    }
    </style>

    <center>
        <h2>Catatan Pelayanan Kesehatan Ibu (Diisi oleh petugas)</h2>
        <h3>Atas Nama {{$pkp->nama}}</h3>
    </center>




    <div
        style="border: 1px solid green; border-radius: 15px; width:450px; position: fixed; top: 75; left: 0; padding-left:10px; padding-top:10px">
        <p style="font-size:13px">Haid Pertama Haid Terakhir (HPHT), tanggal {{$pkp->hpht}}</p>
        <p style="font-size:13px">Hari Taksiran Persalinan (HTP), tanggal {{$pkp->hpl}}</p>
        <p style="font-size:13px">Tinggi Badan {{$pkp->tinggi_badan}} cm , Status {{$pkp->kek}}, Golongan Darah
            {{$pkp->golongan_darah}}</p>
        <p style="font-size:13px">Penggunaan Kontrasepsi Sebelum ini {{$pkp->kontrasepsi}}</p>
        <p style="font-size:13px">Riwayat Penyakit yang diderita Ibu {{$pkp->riwayat_penyakit}}</p>
        <p style="font-size:13px">Riwayat Alergi {{$pkp->riwayat_alergi}}</p>
    </div>



    <div
        style="border:1px solid green; border-radius: 15px; width:450px; height:200px;position: fixed; top: 75; padding-left:10px; padding-top:15px; right: 0">
        <p style="font-size:13px">Hamil ke {{$pkp->jumlah_kehamilan}} Jumlah Persalinan {{$pkp->jumlah_persalinan}}
            Jumlah_keguguran{{$pkp->jumlah_keguguran}}</p>
        <p style="font-size:13px">Jumlah anak hidup {{$pkp->jumlah_anak_hidup}} Jumlah lahir mati
            {{$pkp->jumlah_anak_lahir_mati}}</p>
        <p style="font-size:13px">Jumlah anak lahir kurang bulan {{$pkp->jumlah_anak_prematur}} anak</p>
        <p style="font-size:13px">Status Imunisasi Terakhir {{$pkp->imunisasiTT}} [bulan]</p>
        <p style="font-size:13px">Penolong Persalinan Terakhir {{$pkp->nama_bidan}}</p>
        <p style="font-size:13px">Cara Persalinan Terakhir {{$pkp->persalinan_terakhir}}</p>
    </div>

    <table class='table table-bordered table-sm' style="position: fixed; top: 310;">
        <thead class="thead-light">
            <tr>
                <th>No</th>
                <th>Tanggal</th>
                <th>Keluhan Sekarang</th>
                <th>Tekanan Darah (mmHG)</th>
                <th>Berat Badan (kg)</th>
                <th>Umur Kehamilan </th>
                <th>Tinggi Fundus</th>
                <th>Letak Janin</th>
                <th>Denyut Jantung Janin</th>
                <th>Kaki Bengkak</th>
                <th>Hasil Lab</th>
                <th>Tindakan</th>
                <th>Nasihat yang Disampaikan</th>
                <th>Nama Bidan</th>
                <th>Tanggal Kembali</th>
            </tr>
        </thead>
        <tbody>
            @foreach($data as $d)
            <tr>
                <td>{{$loop->iteration}}</td>
                <td>{{$d->tanggal_pemeriksaan}}</td>
                <td>{{$d->keluhan}}</td>
                <td>{{$d->tekanan_darah}}</td>
                <td>{{$d->berat_badan}}</td>
                <td>{{ $diff = Carbon\Carbon::parse($d->hpht)->diffInWeeks(Carbon\Carbon::parse($d->tanggal_pemeriksaan)) }}
                </td>
                <td>{{$d->tinggi_rahim}}</td>
                <td>{{$d->posisi}}</td>
                <td>{{$d->denyut_jantung_janin}}</td>
                <td>{{$d->kaki_bengkak}}</td>
                <td>{{$d->hasil_labs}}</td>
                <td>{{$d->tindakan}}</td>
                <td>{{$d->hasil_konsultasi}}</td>
                <td>{{$d->bidan_nama}}</td>
                <td>{{$d->tanggal_kembali}}</td>
            </tr>
            @endforeach
        </tbody>
    </table>







</body>

</html>