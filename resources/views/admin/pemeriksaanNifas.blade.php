@extends('admin/master')
@section('content')
<div class="content-wrapper" style="background-color:white;">
    <div class="row">
        <div class="col-12 grid-margin">
            <div class="card">
                <div class="card-header">
                    <div class="row">
                        <div class="col">
                            <h4 class="float-left">Pemeriksaan Nifas</h4>
                            <h4 class="float-right">Saudari {{$pasien->nama}}</h4>
                        </div>
                    </div>
                </div>
                <div class="card-body" style="background-color:#F7F7F7;">
                    <form autocomplete="off" action="{{route('pemeriksaannifasSave', ['id' => $pasien->id])}}"
                        method="post" class="form-horizontal">
                        {{ csrf_field() }}
                        <div class="row">
                            <div class="col-md-12">

                                <div class="form-group">
                                    <label>Tanggal Pemeriksaan </label>
                                    <input name="tgl_pemeriksaan" type="text" class="datepicker-here form-control"
                                        data-language='en' data-multiple-dates="1" data-multiple-dates-separator=", " />
                                </div>

                                @if($akhirNifas <= 1) <div class="form-group">
                                    <label>Tanggal Kembali </label>
                                    <input name="tgl_kembali" type="text" class="datepicker-here form-control"
                                        data-language='en' data-multiple-dates="1" data-multiple-dates-separator=", " />
                            </div>
                            @endif

                            <div class="form-group">
                                <label>Tekanan Darah </label>
                                <input class="form-control" name="tekanan_darah">
                            </div>

                            <div class="form-group">
                                <label>Kondisi Lokhia </label>
                                <input class="form-control" name="lokhia">
                            </div>

                            <div class="form-group">
                                <label>Kondisi Payudara </label>
                                <input class="form-control" name="pemeriksaan_payudara">
                            </div>

                            <div class="form-group">
                                <label>Kondisi Jalan Lahir </label>
                                <input class="form-control" name="jalan_lahir">
                            </div>
                            <div class="form-group">
                                <label>Tinggi Fundus </label>
                                <input class="form-control" name="tinggi_fundus">
                            </div>
                            <div class="form-group">
                                <label>Nasihat </label>
                                <textarea cols="30" rows="3" class="form-control" name="nasihat"></textarea>
                            </div>
                            @if($akhirNifas > 1)
                            <div class="form-group">
                                <label>Kesimpulan Akhir Nifas </label>
                                <textarea cols="30" rows="6" class="form-control" name="keadaan"></textarea>
                            </div>
                            @endif
                            <div class="form-group">
                                <button class="btn btn-info" type="submit">Simpan</button>
                            </div>
                        </div>

                </div>
                </form>
            </div>


        </div>
    </div>
</div>

@endsection