@extends('admin/master')
@section('content')
<div class="content-wrapper" style="background-color:white;">
    <div class="row">
        <div class="col-12 grid-margin">
            <div class="card">
                <div class="card-header">
                    <h4 class="page-title">
                        <span class="page-title-icon bg-gradient-primary text-white mr-2">
                            <i class="mdi mdi-format-list-bulleted menu-icon"></i>
                        </span> Detail Riwayat Pemeriksaan Kehamilan </h4>
                </div>
                <div class="card-body">

                    <table class="table table-bordered table-striped">
                        <thead>
                            <th>No</th>
                            <th>Tanggal Pemeriksaan</th>
                            <th>Keluhan</th>
                            <th>Tekanan Darah (mmHg)</th>
                            <th>Berat Badan (Kg)</th>
                            <th>Umur Kehamilan (Minggu)</th>
                            <th>Tinggi Fundus(Cm)</th>
                            <th>Letak Janin</th>
                            <th>Trisemester</th>
                            <th>Action</th>
                        </thead>
                        <tbody>
                            @foreach($pkh as $p)
                            <tr>
                                <td>{{$loop->iteration}}</td>
                                <td>{{$p->tanggal_pemeriksaan}}</td>
                                <td>{{$p->keluhan}}</td>
                                <td>{{$p->tekanan_darah}}</td>
                                <td>{{$p->berat_badan}}</td>
                                <td>{{ $diff = Carbon\Carbon::parse($p->tgl_mens_terakhir)->diffInWeeks(Carbon\Carbon::parse($p->tanggal_pemeriksaan)) }}
                                </td>
                                <td>{{$p->tinggi_rahim}}</td>
                                <td>{{$p->posisi}}</td>
                                <td>{{$p->semester}}</td>
                                <td><a href="{{route('editDetailRiwayatPemeriksaanKehamilan', ['id' => $p->id])}}"
                                        class="btn btn-warning">Edit</a></td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>


                </div>


            </div>
        </div>
    </div>

    @endsection