<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
</head>

<body>
    <table class="table">
        <thead>
            <tr>
                <th>Tanggal Pemeriksaan</th>
                <th>Keluham</th>
                <th>Umur Kehamilan</th>
            </tr>
        </thead>
        <tbody>
            @foreach($pkh as $p)
            <tr>
                <td>{{$p->tanggal_pemeriksaan}}</td>
                <td>{{$p->keluhan}}</td>
                <td>{{ $diff = Carbon\Carbon::parse($p->tanggal_pemeriksaan)->diffInWeeks(Carbon\Carbon::parse($p->tanggal_kembali)) }}
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
</body>

</html>