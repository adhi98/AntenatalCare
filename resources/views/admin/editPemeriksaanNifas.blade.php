@extends('admin/master')
@section('content')
<div class="content-wrapper" style="background-color:white;">
    <div class="row">
        <div class="col-12 grid-margin">
            <div class="card">
                <div class="card-header">
                    <div class="row">
                        <div class="col">
                            <h4 class="float-left">Edit Pemeriksaan Nifas</h4>
                            <h4 class="float-right">Saudari {{$pkn->nama}}</h4>
                        </div>
                    </div>
                </div>
                <div class="card-body" style="background-color:#F7F7F7;">
                    <form autocomplete="off" action="{{route('updateRiwayatPemeriksaanNifas', ['id' => $pkn->id])}}"
                        method="post" class="form-horizontal">
                        {{ csrf_field() }}
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Tekanan Darah </label>
                                    <input class="form-control" name="tekanan_darah" value="{{$pkn->tekanan_darah}}">
                                </div>
                                <div class="form-group">
                                    <label>Tinggi Fundus </label>
                                    <input class="form-control" value="{{$pkn->tinggi_fundus}}" name="tinggi_fundus">
                                </div>
                                <div class="form-group">
                                    <label>Kondisi Lokhia </label>
                                    <input class="form-control" value="{{$pkn->lokhia}}" name="lokhia">
                                </div>
                                <div class="form-group">
                                    <label>Kondisi Jalan Lahir </label>
                                    <input class="form-control" value="{{$pkn->jalan_lahir}}" name="jalan_lahir">
                                </div>
                                <div class="form-group">
                                    <label>Pemeriksaan Payudara </label>
                                    <input class="form-control" value="{{$pkn->pemeriksaan_payudara}}"
                                        name="pemeriksaan_payudara">
                                </div>
                                <div class="form-group">
                                    <label>Nasihat </label>
                                    <textarea cols="30" rows="3" class="form-control"
                                        name="nasihat">{{$pkn->nasihat}}</textarea>
                                </div>
                                <div class="form-group">
                                    <button class="btn btn-info" type="submit">Update</button>
                                </div>
                            </div>

                        </div>
                    </form>
                </div>


            </div>
        </div>
    </div>

    @endsection