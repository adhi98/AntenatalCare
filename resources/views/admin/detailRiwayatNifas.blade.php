@extends('admin/master')
@section('content')
<div class="content-wrapper" style="background-color:white;">
    <div class="row">
        <div class="col-12 grid-margin">
            <div class="card">
                <div class="card-header">
                    <h4 class="page-title">
                        <span class="page-title-icon bg-gradient-primary text-white mr-2">
                            <i class="mdi mdi-format-list-bulleted menu-icon"></i>
                        </span> Detail Riwayat Pemeriksaan Nifas </h4>
                </div>
                <div class="card-body">

                    <table class="table table-bordered table-striped">
                        <thead>
                            <th>No</th>
                            <th>Tanggal Pemeriksaan</th>
                            <th>Tekanan Darah</th>
                            <th>Lokhia</th>
                            <th>Jalan Lahir</th>
                            <th>Tinggi Fundus</th>
                            <th>Pemeriksaan Payudara</th>
                            <th>nasihat</th>
                            <th>Action</th>
                        </thead>
                        <tbody>
                            @foreach($pkn as $p)
                            <tr>
                                <td>{{$loop->iteration}}</td>
                                <td>{{$p->tgl_pemeriksaan}}</td>
                                <td>{{$p->tekanan_darah}}</td>
                                <td>{{$p->lokhia}}</td>
                                <td>{{$p->jalan_lahir}}</td>
                                <td>{{$p->tinggi_fundus}}</td>
                                <td>{{$p->pemeriksaan_payudara}}</td>
                                <td>{{$p->nasihat}}</td>
                                <td><a href="{{route('editRiwayatPemeriksaanNifas', ['id' => $p->id])}}"
                                        class="btn btn-warning">Edit</a></td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>


                </div>


            </div>
        </div>
    </div>

    @endsection