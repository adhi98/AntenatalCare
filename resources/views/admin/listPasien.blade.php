@extends('admin/master')
@section('content')
<div class="content-wrapper" style="background-color:white;">
    <div class="row">
        <div class="col-12 grid-margin">
            <div class="card">
                <div class="card-header">
                    <h4 class="page-title">
                        <span class="page-title-icon bg-gradient-primary text-white mr-2">
                            <i class="mdi mdi-format-list-bulleted menu-icon"></i>
                        </span> List Pasien </h4>
                </div>
                <div class="card-body">

                    <table class="table table-bordered table-striped">
                        <thead>
                            <th>No</th>
                            <th>Nama</th>
                            <th>Tanggal Lahir</th>
                            <th>Pekerjaan</th>
                            <th>Agama</th>
                            <th>Alamat</th>
                            <th>No Telepon</th>
                            <th>Fase</th>
                            <th>Operasi</th>
                            <th>Pemeriksaan</th>
                        </thead>
                        <tbody>

                            @foreach($pasien as $p)

                            <tr>
                                <td>{{$loop->iteration}}</td>
                                <td>{{$p->nama}}</td>
                                <td>{{$p->tanggal_lahir}}</td>
                                <td>{{$p->pekerjaan}}</td>
                                <td>{{$p->agama}}</td>
                                <td>{{$p->alamat}}</td>
                                <td>{{$p->no_tlp}}</td>
                                <td>{{$p->status}}</td>
                                <td>
                                    <a href="{{route('listpasienEdit', ['id' => $p->id])}}"
                                        class="btn btn-warning">Edit</a>
                                </td>
                                <td>
                                    @if($p->fase === 5)
                                    <button type="button" class="btn btn-danger" disabled>Selesai</button>
                                    @else
                                    <a href="{{route('pemeriksaankehamilan', ['id' => $p->id])}}"
                                        class="btn btn-info">Kehamilan</a>
                                    <a href="{{route('pemeriksaannifas', ['id' => $p->id])}}"
                                        class="btn btn-info">Nifas</a>
                                    @endif

                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>


                </div>


            </div>
        </div>
    </div>

    @endsection