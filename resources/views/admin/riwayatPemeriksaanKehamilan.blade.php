@extends('admin/master')
@section('content')
<div class="content-wrapper" style="background-color:white;">
    <div class="row">
        <div class="col-12 grid-margin">
            <div class="card">
                <div class="card-header">
                    <h4 class="page-title">
                        <span class="page-title-icon bg-gradient-primary text-white mr-2">
                            <i class="mdi mdi-format-list-bulleted menu-icon"></i>
                        </span> Riwayat Pemeriksaan Kehamilan </h4>
                </div>
                <div class="card-body">

                    <table class="table table-bordered table-striped">
                        <thead>
                            <th>No</th>
                            <th>Nomor Pasien</th>
                            <th>Nama</th>
                            <th>Alamat</th>
                            <th>Detail</th>
                            <th>Download PDF</th>
                        </thead>
                        <tbody>
                            @foreach($pasien as $p)
                            <tr>
                                <td>{{$loop->iteration}}</td>
                                <td>{{$p->no_pasien}}</td>
                                <td>{{$p->nama}}</td>
                                <td>{{$p->alamat}}</td>
                                <td><a href="{{route('detailRiwayatPemeriksaanKehamilan', ['id' => $p->id])}}"
                                        class="btn btn-warning">Detail</a>
                                </td>
                                <td><a href="{{route('catatanKehamilan', ['id' => $p->id])}}"
                                        class="btn btn-warning">Download</a>
                                </td>

                            </tr>
                            @endforeach
                        </tbody>
                    </table>


                </div>


            </div>
        </div>
    </div>

    @endsection