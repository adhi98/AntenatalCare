@extends('admin/master')
@section('content')
<div class="content-wrapper" style="background-color:white;">
    <div class="row">
        <div class="col-12 grid-margin">
            <div class="card">
                <div class="card-header">
                    <div class="row">
                        <div class="col">
                            <h4 class="page-title float-left">
                                <span class="page-title-icon bg-gradient-primary text-white mr-2">
                                    <i class="mdi mdi-format-list-bulleted menu-icon"></i>
                                </span> Edit Pasien </h4>
                            <h4 class="float-right">Saudari {{$pasien->pnama}}</h4>
                        </div>
                    </div>
                </div>
                <div class="card-body" style="background-color:#F7F7F7;">

                    <form action="{{route('listpasienUpdate', ['id' => $pasien->pid])}}" method="post"
                        class="form-horizontal">
                        {{ csrf_field() }}
                        <div class="row">
                            <div class="col-md-6">
                                <p>IDENTITAS PASIEN</p>
                                <div class="form-group row">

                                    @if($errors->has('nojkn_pasien'))

                                    <label class="col-sm-3 col-form-label" style="color:red">Nomor JKN : </label>
                                    <div class="col-sm-9">
                                        <input class="form-control" name="nojkn_pasien"
                                            placeholder="{{$errors->first('nojkn_pasien')}}"
                                            onkeypress='validate(event)'>
                                    </div>

                                    @else
                                    <label class="col-sm-3 col-form-label">Nomor JKN : </label>
                                    <div class="col-sm-9">
                                        <input class="form-control" name="nojkn_pasien" onkeypress='validate(event)'
                                            value='{{$pasien->pnojkn}}'>
                                    </div>
                                    @endif

                                </div>
                                <div class="form-group row">
                                    @if($errors->has('nopasien'))
                                    <label class="col-sm-3 col-form-label" style="color:red">Nomor Pasien : </label>
                                    <div class="col-sm-9">
                                        <input class="form-control" name="nopasien"
                                            placeholder="{{$errors->first('nopasien')}}" onkeypress='validate(event)'>
                                    </div>
                                    @else
                                    <label class="col-sm-3 col-form-label">Nomor Pasien : </label>
                                    <div class="col-sm-9">
                                        <input class="form-control" name="nopasien" onkeypress='validate(event)'
                                            value='{{$pasien->pnopasien}}'>
                                    </div>
                                    @endif

                                </div>
                                <div class="form-group row">
                                    @if($errors->has('nama_pasien'))
                                    <label class="col-sm-3 col-form-label" style="color:red">Nama Lengkap : </label>
                                    <div class="col-sm-9">
                                        <input class="form-control" name="nama_pasien"
                                            placeholder="{{$errors->first('nama_pasien')}}">
                                    </div>
                                    @else
                                    <label class="col-sm-3 col-form-label">Nama Lengkap : </label>
                                    <div class="col-sm-9">
                                        <input class="form-control" name="nama_pasien" value='{{$pasien->pnama}}'>
                                    </div>
                                    @endif

                                </div>
                                <div class="form-group row">
                                    @if($errors->has('tanggal_lahir'))
                                    <label class="col-sm-3 col-form-label" style="color:red">Tanggal Lahir : </label>
                                    <div class="col-sm-9">
                                        <input class="form-control" type="date" name="tgl_lahir_pasien"
                                            placeholder="{{$errors->first('tanggal_lahir')}}">
                                    </div>
                                    @else
                                    <label class=" col-sm-3 col-form-label">Tanggal Lahir : </label>
                                    <div class="col-sm-9">
                                        <input class="form-control" type="date" name="tgl_lahir_pasien"
                                            value='{{$pasien->ptgl_lahir}}'>
                                    </div>
                                    @endif

                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-3 col-form-label">Agama : </label>
                                    <div class="col-sm-9">
                                        <select class="form-control" name="agama_pasien">
                                            @foreach($pagama as $pagama)
                                            <option value="{{$pagama->id}}" @if($pagama->id === $pasien->pagama)
                                                selected
                                                @endif
                                                >{{$pagama->agama}}</option>
                                            @endforeach

                                        </select>
                                    </div>
                                </div>
                                <div class="form-group row">

                                    @if($errors->has('golongan_darah_pasien'))
                                    <label class="col-sm-3 col-form-label" style="color:red">Golongan Darah : </label>
                                    <div class="col-sm-9">
                                        <select class="form-control" name="golongan_darah_pasien">
                                            @foreach($goldarp as $goldarp)
                                            <option value="{{$goldarp->id}}" @if($goldarp->id ===
                                                $pasien->pgoldar)
                                                selected
                                                @endif>{{$goldarp->darah}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    @else
                                    <label class="col-sm-3 col-form-label">Golongan Darah : </label>
                                    <div class="col-sm-9">
                                        <select class="form-control" name="golongan_darah_pasien">
                                            @foreach($goldarp as $goldarp)
                                            <option value="{{$goldarp->id}}" @if($goldarp->id ===
                                                $pasien->pgoldar)
                                                selected
                                                @endif>{{$goldarp->darah}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    @endif
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-3 col-form-label">Pekerjaan : </label>
                                    <div class="col-sm-9">
                                        <select class="form-control" name="pekerjaan_pasien">
                                            @foreach($ppekerjaan as $ppekerjaan)
                                            <option value="{{$ppekerjaan->id}}" @if($ppekerjaan->id ===
                                                $pasien->ppekerjaan)
                                                selected
                                                @endif
                                                >{{$ppekerjaan->pekerjaan}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    @if($errors->has('notlp_pasien'))
                                    <label class="col-sm-3 col-form-label" style="color:red">Nomor Telepon : </label>
                                    <div class="col-sm-9">
                                        <input class="form-control" name="notlp_pasien"
                                            placeholder="{{$errors->first('notlp_pasien')}}"
                                            onkeypress='validate(event)'>
                                    </div>
                                    @else
                                    <label class="col-sm-3 col-form-label">Nomor Telepon : </label>
                                    <div class="col-sm-9">
                                        <input class="form-control" name="notlp_pasien" onkeypress='validate(event)'
                                            value='{{$pasien->pnotlp}}'>
                                    </div>
                                    @endif

                                </div>
                                <div class="form-group row">
                                    @if($errors->has('noktp_pasien'))
                                    <label class="col-sm-3 col-form-label" style="color:red">Nomor KTP : </label>
                                    <div class="col-sm-9">
                                        <input class="form-control" name="noktp_pasien" onkeypress='validate(event)'
                                            placeholder="{{$errors->first('noktp_pasien')}}">
                                    </div>
                                    @else
                                    <label class="col-sm-3 col-form-label">Nomor KTP : </label>
                                    <div class="col-sm-9">
                                        <input class="form-control" name="noktp_pasien" onkeypress='validate(event)'
                                            value='{{$pasien->pnoktp}}'>
                                    </div>
                                    @endif

                                </div>
                                <div class="form-group row">
                                    @if($errors->has('alamat_pasien'))
                                    <label class="col-sm-3 col-form-label" style="color:red">Alamat : </label>
                                    <div class="col-sm-9">
                                        <textarea class="form-control" rows="3" id="comment" name="alamat_pasien"
                                            placeholder="{{$errors->first('alamat_pasien')}}"></textarea>
                                    </div>
                                    @else
                                    <label class="col-sm-3 col-form-label">Alamat : </label>
                                    <div class="col-sm-9">
                                        <textarea class="form-control" rows="3" id="comment"
                                            name="alamat_pasien">{{$pasien->palamat}}</textarea>
                                    </div>
                                    @endif

                                </div>

                            </div>
                            <div class="col-md-6">
                                <p>IDENTITAS Wali</p>
                                <div class="form-group row">
                                    @if($errors->has('nama_wali'))
                                    <label class="col-sm-3 col-form-label" style="color:red">Nama Wali : </label>
                                    <div class="col-sm-9">
                                        <input class="form-control" name="nama_wali"
                                            placeholder="{{$errors->first('nama_wali')}}">
                                    </div>
                                    @else
                                    <label class="col-sm-3 col-form-label">Nama Wali : </label>
                                    <div class="col-sm-9">
                                        <input class="form-control" name="nama_wali" value='{{$pasien->wnama}}'>
                                    </div>
                                    @endif

                                </div>
                                <div class="form-group row">
                                    @if($errors->has('tgl_lahir_wali'))
                                    <label class="col-sm-3 col-form-label" style="color:red">Tanggal Lahir : </label>
                                    <div class="col-sm-9">
                                        <input class="form-control" type="date" name="tgl_lahir_wali"
                                            placeholder="{{$errors->first('tgl_lahir_wali')}}">
                                    </div>
                                    @else
                                    <label class="col-sm-3 col-form-label">Tanggal Lahir : </label>
                                    <div class="col-sm-9">
                                        <input class="form-control" type="date" name="tgl_lahir_wali"
                                            value='{{$pasien->wtgl_lahir}}'>
                                    </div>
                                    @endif

                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-3 col-form-label">Agama : </label>
                                    <div class="col-sm-9">
                                        <select class="form-control" name="agama_wali">
                                            @foreach($wagama as $wagama)
                                            <option value="{{$wagama->id}}" @if($wagama->id === $pasien->wagama)
                                                selected
                                                @endif
                                                >{{$wagama->agama}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    @if($errors->has('golongan_darah_wali'))
                                    <label class="col-sm-3 col-form-label" style="color:red">Golongan Darah : </label>
                                    <div class="col-sm-9">
                                        <select class="form-control" name="golongan_darah_wali">
                                            @foreach($goldarw as $goldarw)
                                            <option value="{{$goldarw->id}}" @if($goldarw->id ===
                                                $pasien->wgoldar)
                                                selected
                                                @endif>{{$goldarw->darah}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    @else
                                    <label class="col-sm-3 col-form-label">Golongan Darah : </label>
                                    <div class="col-sm-9">
                                        <select class="form-control" name="golongan_darah_wali">
                                            @foreach($goldarw as $goldarw)
                                            <option value="{{$goldarw->id}}" @if($goldarw->id ===
                                                $pasien->wgoldar)
                                                selected
                                                @endif>{{$goldarw->darah}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    @endif

                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-3 col-form-label">Pendidikan Terakhir : </label>
                                    <div class="col-sm-9">
                                        <select class="form-control" name="pendidikan_wali">
                                            @foreach($pendidikan as $pendidikan)
                                            <option value="{{$pendidikan->id}}" @if($pendidikan->id ===
                                                $pasien->wpendidikan)
                                                selected
                                                @endif
                                                >{{$pendidikan->pendidikan}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-3 col-form-label">Pekerjaan : </label>
                                    <div class="col-sm-9">
                                        <select class="form-control" name="pekerjaan_wali">
                                            @foreach($wpekerjaan as $wpekerjaan)
                                            <option value="{{$wpekerjaan->id}}" @if($wpekerjaan->id ===
                                                $pasien->wpekerjaan)
                                                selected
                                                @endif
                                                >{{$wpekerjaan->pekerjaan}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    @if($errors->has('notlp_wali'))
                                    <label class="col-sm-3 col-form-label" style="color:red">Nomor Telepon : </label>
                                    <div class="col-sm-9">
                                        <input class="form-control" name="notlp_wali"
                                            placeholder="{{$errors->first('notlp_wali')}}" onkeypress='validate(event)'>
                                    </div>
                                    @else
                                    <label class="col-sm-3 col-form-label">Nomor Telepon : </label>
                                    <div class="col-sm-9">
                                        <input class="form-control" name="notlp_wali" onkeypress='validate(event)'
                                            value='{{$pasien->wnotlp}}'>
                                    </div>
                                    @endif

                                </div>
                                <div class="form-group row">
                                    @if($errors->has('noktp_wali'))
                                    <label class="col-sm-3 col-form-label">Nomor KTP : </label>
                                    <div class="col-sm-9">
                                        <input class="form-control" name="noktp_wali" onkeypress='validate(event)'
                                            placeholder="{{$errors->first('noktp_wali')}}">
                                    </div>
                                    @else
                                    <label class="col-sm-3 col-form-label">Nomor KTP : </label>
                                    <div class="col-sm-9">
                                        <input class="form-control" name="noktp_wali" onkeypress='validate(event)'
                                            value='{{$pasien->wnoktp}}'>
                                    </div>
                                    @endif

                                </div>
                                <div class="form-group row">
                                    @if($errors->has('alamat_wali'))
                                    <label class="col-sm-3 col-form-label" style="color:red">Alamat : </label>
                                    <div class="col-sm-9">
                                        <textarea class="form-control" rows="2" id="comment" name="alamat_wali"
                                            placeholder="{{$errors->first('alamat_wali')}}"></textarea>
                                    </div>
                                    @else
                                    <label class="col-sm-3 col-form-label">Alamat : </label>
                                    <div class="col-sm-9">
                                        <textarea class="form-control" rows="2" id="comment"
                                            name="alamat_wali">{{$pasien->walamat}}</textarea>
                                    </div>
                                    @endif

                                </div>
                                <div class="form-group row justify-content-md-center mt-5">
                                    <button class="btn btn-info col-md-9" type="submit">Edit</button>
                                </div>

                            </div>

                    </form>


                </div>


            </div>
        </div>
    </div>

    @endsection