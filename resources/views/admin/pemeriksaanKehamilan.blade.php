@extends('admin/master')
@section('content')
<div class="content-wrapper" style="background-color:white;">
    <div class="row">
        <div class="col-12 grid-margin">
            <div class="card">
                <div class="card-header">
                    <div class="row">
                        <div class="col">
                            <h4 class="float-left">Pemeriksaan Kehamilan</h4>
                            <h4 class="float-right">Saudari {{$pasien->nama}}</h4>
                        </div>
                    </div>

                </div>
                <div class="card-body" style="background-color:#F7F7F7;">
                    <form autocomplete="off" action="{{route('pemeriksaankehamilanSave', ['id' => $pasien->id])}}"
                        method="post">

                        {{ csrf_field() }}

                        <div class="row">

                            <div class="col-md-6">
                                @if($errors->has('tanggal_pemeriksaan'))
                                <div class="row mb-2">
                                    <div class="col-sm-3">
                                        <label style="color:red">Tanggal Pemeriksaan </label>
                                    </div>
                                    <div class="col-sm-9">
                                        <div class="input-group mb-3 input-group-sm">
                                            <input type="text" name="tanggal_pemeriksaan" data-range="true"
                                                data-multiple-dates-separator=" - " data-language="en"
                                                class="datepicker-here form-control" />
                                        </div>
                                    </div>
                                </div>
                                @else
                                <div class="row mb-2">
                                    <div class="col-sm-3">
                                        <label>Tanggal Pemeriksaan </label>
                                    </div>
                                    <div class="col-sm-9">
                                        <div class="input-group mb-3 input-group-sm">
                                            <input type="text" name="tanggal_pemeriksaan" data-range="true"
                                                data-multiple-dates-separator=" - " data-language="en"
                                                class="datepicker-here form-control" />
                                        </div>
                                    </div>
                                </div>
                                @endif

                                <div class="row mb-3">
                                    @if($errors->has('lingkar_lengan'))
                                    <div class="col-sm-3">
                                        <label style="color:red">Panjang Lila </label>
                                    </div>
                                    <div class="col-sm-9">
                                        <div class="input-group input-group-sm">
                                            <input type="text" name="lingkar_lengan" class="form-control"
                                                onkeypress='validate(event)'
                                                placeholder="{{$errors->first('lingkar_lengan')}}">
                                            <div class="input-group-append">
                                                <span class="input-group-text">cm</span>
                                            </div>
                                        </div>
                                    </div>
                                    @else
                                    <div class="col-sm-3">
                                        <label>Panjang Lila </label>
                                    </div>
                                    <div class="col-sm-9">
                                        <div class="input-group input-group-sm">
                                            <input type="text" name="lingkar_lengan" class="form-control"
                                                onkeypress='validate(event)'>
                                            <div class="input-group-append">
                                                <span class="input-group-text">cm</span>
                                            </div>
                                        </div>
                                    </div>
                                    @endif

                                </div>

                                <div class="row mb-3">
                                    @if($errors->has('berat_badan'))
                                    <div class="col-sm-3">
                                        <label style="color:red">Berat Badan </label>
                                    </div>
                                    <div class="col-sm-9">
                                        <div class="input-group input-group-sm">
                                            <input type="text" name="berat_badan" class="form-control"
                                                onkeypress='validate(event)'
                                                placeholder="{{$errors->first('berat_badan')}}">
                                            <div class="input-group-append">
                                                <span class="input-group-text">kg</span>
                                            </div>
                                        </div>
                                    </div>
                                    @else
                                    <div class="col-sm-3">
                                        <label>Berat Badan </label>
                                    </div>
                                    <div class="col-sm-9">
                                        <div class="input-group input-group-sm">
                                            <input type="text" name="berat_badan" class="form-control"
                                                onkeypress='validate(event)'>
                                            <div class="input-group-append">
                                                <span class="input-group-text">kg</span>
                                            </div>
                                        </div>
                                    </div>
                                    @endif

                                </div>

                                <div class="row mb-3">
                                    @if($errors->has('tinggi_rahim'))
                                    <div class="col-sm-3">
                                        <label style="color:red">Tinggi Rahim </label>
                                    </div>
                                    <div class="col-sm-9">
                                        <div class="input-group input-group-sm">
                                            <input type="text" name="tinggi_rahim" class="form-control"
                                                onkeypress='validate(event)'
                                                placeholder="{{$errors->first('tinggi_rahim')}}">
                                            <div class="input-group-append">
                                                <span class="input-group-text">cm</span>
                                            </div>
                                        </div>
                                    </div>
                                    @else
                                    <div class="col-sm-3">
                                        <label>Tinggi Rahim </label>
                                    </div>
                                    <div class="col-sm-9">
                                        <div class="input-group input-group-sm">
                                            <input type="text" name="tinggi_rahim" class="form-control"
                                                onkeypress='validate(event)'>
                                            <div class="input-group-append">
                                                <span class="input-group-text">cm</span>
                                            </div>
                                        </div>
                                    </div>
                                    @endif

                                </div>

                                <div class="row mb-3">
                                    @if($errors->has('tekanan_darah'))
                                    <div class="col-sm-3">
                                        <label style="color:red">Tekanan Darah </label>
                                    </div>
                                    <div class="col-sm-9">
                                        <div class="input-group input-group-sm">
                                            <input type="text" name="tekanan_darah" class="form-control"
                                                placeholder="{{$errors->first('tekanan_darah')}}">
                                            <div class="input-group-append">
                                                <span class="input-group-text">mmHg</span>
                                            </div>
                                        </div>
                                    </div>

                                    @else
                                    <div class="col-sm-3">
                                        <label>Tekanan Darah </label>
                                    </div>
                                    <div class="col-sm-9">
                                        <div class="input-group input-group-sm">
                                            <input type="text" name="tekanan_darah" class="form-control">
                                            <div class="input-group-append">
                                                <span class="input-group-text">mmHg</span>
                                            </div>
                                        </div>
                                    </div>

                                    @endif
                                </div>

                                <div class="row mb-3">
                                    @if($errors->has('letak_janin'))
                                    <div class="col-sm-3">
                                        <label style="color:red">Letak Janin </label>
                                    </div>
                                    <div class="col-sm-9">
                                        <div class="input-group input-group-sm">
                                            <!-- <input type="text" name="letak_janin" class="form-control"
                                                        placeholder="{{$errors->first('letak_janin')}}"> -->
                                            <select class="form-control" name="letak_janin">
                                                @foreach($janin as $janin)
                                                <option value="{{$janin->id}}">{{$janin->posisi}}
                                                </option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    @else
                                    <div class="col-sm-3">
                                        <label>Letak Janin </label>
                                    </div>
                                    <div class="col-sm-9">
                                        <div class="input-group input-group-sm">
                                            <select class="form-control" name="letak_janin">
                                                @foreach($janin as $janin)
                                                <option value="{{$janin->id}}">{{$janin->posisi}}
                                                </option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    @endif
                                </div>

                                <div class="row mb-3">
                                    @if($errors->has('denyut_jantung_janin'))
                                    <div class="col-sm-3">
                                        <label style="color:red">Denyut Jantung Janin </label>
                                    </div>
                                    <div class="col-sm-9">
                                        <div class="input-group input-group-sm">
                                            <input type="text" name="denyut_jantung_janin" class="form-control"
                                                placeholder="{{$errors->first('denyut_jantung_janin')}}">
                                        </div>
                                    </div>
                                    @else
                                    <div class="col-sm-3">
                                        <label>Denyut Janin </label>
                                    </div>
                                    <div class="col-sm-9">
                                        <div class="input-group input-group-sm">
                                            <input type="text" name="denyut_jantung_janin" class="form-control">
                                        </div>
                                    </div>
                                    @endif
                                </div>

                                <div class="row mb-3">
                                    @if($errors->has('kaki_bengkak'))
                                    <div class="col-sm-3">
                                        <label style="color:red">Kaki Bengkak </label>
                                    </div>
                                    <div class="col-sm-9">
                                        <div class="input-group input-group-sm">
                                            <input type="text" name="kaki_bengkak" class="form-control"
                                                placeholder="{{$errors->first('kaki_bengkak')}}">
                                        </div>
                                    </div>
                                    @else
                                    <div class="col-sm-3">
                                        <label>Kaki Bengkak </label>
                                    </div>
                                    <div class="col-sm-9">
                                        <div class="input-group input-group-sm">
                                            <input type="text" name="kaki_bengkak" class="form-control">
                                        </div>
                                    </div>
                                    @endif
                                </div>

                                <div class="row mb-2">
                                    @if($errors->has('anemia'))
                                    <div class="col-sm-3">
                                        <label style="color:red">Tes Lab Anemia </label>
                                    </div>
                                    <div class="col-sm-9">
                                        <div class="input-group input-group-sm">
                                            <textarea name="anemia" id="" rows="2" class="form-control"
                                                placeholder="Data ini wajib di Isi"></textarea>
                                        </div>
                                    </div>
                                    @else
                                    <div class="col-sm-3">
                                        <label>Tes Lab Anemia </label>
                                    </div>
                                    <div class="col-sm-9">
                                        <div class="input-group input-group-sm">
                                            <textarea name="anemia" id="" rows="2" class="form-control"></textarea>
                                        </div>
                                    </div>
                                    @endif

                                </div>


                            </div>

                            <div class="col-md-6">

                                <!-- Hasil Laboratorium New -->

                                <div class="row mb-2">
                                    @if($errors->has('hiv'))
                                    <div class="col-sm-3">
                                        <label style="color:red">Tes Lab HIV </label>
                                    </div>
                                    <div class="col-sm-9">
                                        <div class="input-group input-group-sm">
                                            <textarea name="hiv" id="" rows="2" class="form-control"
                                                placeholder="Data ini wajib di Isi"></textarea>
                                        </div>
                                    </div>
                                    @else
                                    <div class="col-sm-3">
                                        <label>Tes Lab HIV </label>
                                    </div>
                                    <div class="col-sm-9">
                                        <div class="input-group input-group-sm">
                                            <textarea name="hiv" id="" rows="2" class="form-control"></textarea>
                                        </div>
                                    </div>
                                    @endif
                                </div>

                                <div class="row mb-2">
                                    @if($errors->has('sivilis'))
                                    <div class="col-sm-3">
                                        <label style="color:red">Tes Lab Sivilis </label>
                                    </div>
                                    <div class="col-sm-9">
                                        <div class="input-group input-group-sm">
                                            <textarea name="sivilis" id="" rows="2" class="form-control"
                                                placeholder="Data ini wajib di Isi"></textarea>
                                        </div>
                                    </div>
                                    @else
                                    <div class="col-sm-3">
                                        <label>Tes Lab Sivilis </label>
                                    </div>
                                    <div class="col-sm-9">
                                        <div class="input-group input-group-sm">
                                            <textarea name="sivilis" id="" rows="2" class="form-control"></textarea>
                                        </div>
                                    </div>
                                    @endif
                                </div>

                                <!--  -->

                                <div class="row mb-2">
                                    @if($errors->has('keluhan'))
                                    <div class="col-sm-3">
                                        <label style="color:red">Keluhan </label>
                                    </div>
                                    <div class="col-sm-9">
                                        <div class="input-group input-group-sm">
                                            <textarea name="keluhan" id="" rows="2" class="form-control"
                                                placeholder="{{$errors->first('keluhan')}}"></textarea>
                                        </div>
                                    </div>
                                    @else
                                    <div class="col-sm-3">
                                        <label>Keluhan </label>
                                    </div>
                                    <div class="col-sm-9">
                                        <div class="input-group input-group-sm">
                                            <textarea name="keluhan" id="" rows="2" class="form-control"></textarea>
                                        </div>
                                    </div>
                                    @endif
                                </div>

                                <div class="row mb-2">
                                    @if($errors->has('tindakan'))
                                    <div class="col-sm-3">
                                        <label style="color:red">Tindakan </label>
                                    </div>
                                    <div class="col-sm-9">
                                        <div class="input-group input-group-sm">
                                            <textarea name="tindakan" id="" rows="2" class="form-control"
                                                placeholder="{{$errors->first('tindakan')}}"></textarea>
                                        </div>
                                    </div>
                                    @else
                                    <div class="col-sm-3">
                                        <label>Tindakan Khusus </label>
                                    </div>
                                    <div class="col-sm-9">
                                        <div class="input-group input-group-sm">
                                            <textarea name="tindakan" id="" rows="2" class="form-control"></textarea>
                                        </div>
                                    </div>
                                    @endif
                                </div>

                                <div class="row mb-2">
                                    @if($errors->has('hasil_konsultasi'))
                                    <div class="col-sm-3">
                                        <label style="color:red">Nasihat </label>
                                    </div>
                                    <div class="col-sm-9">
                                        <div class="input-group input-group-sm">
                                            <textarea name="hasil_konsultasi" id="" rows="4" class="form-control"
                                                placeholder="{{$errors->first('hasil_konsultasi')}}"></textarea>
                                        </div>
                                    </div>
                                    @else
                                    <div class="col-sm-3">
                                        <label>Nasihat </label>
                                    </div>
                                    <div class="col-sm-9">
                                        <div class="input-group input-group-sm">
                                            <textarea name="hasil_konsultasi" id="" rows="4"
                                                class="form-control"></textarea>
                                        </div>
                                    </div>
                                    @endif
                                </div>


                                <div class="row mb-2">
                                    @if($errors->has('tanggal_kembali'))
                                    <div class="col-sm-3">
                                        <label style="color:red">Tanggal Kembali </label>
                                    </div>
                                    <div class="col-sm-9">
                                        <div class="input-group mb-3 input-group-sm">
                                            <input name="tanggal_kembali" type="text"
                                                class="datepicker-here form-control" data-language='en'
                                                data-multiple-dates="1" data-multiple-dates-separator=", "
                                                data-position='top left' />
                                        </div>
                                    </div>
                                    @else
                                    <div class="col-sm-3">
                                        <label>Tanggal Kembali </label>
                                    </div>
                                    <div class="col-sm-9">
                                        <div class="input-group mb-3 input-group-sm">
                                            <input type="text" name="tanggal_kembali" data-range="true"
                                                data-multiple-dates-separator=" - " data-language="en"
                                                class="datepicker-here form-control" />
                                        </div>
                                    </div>
                                    @endif

                                </div>

                                <div class="row mb-2">
                                    <div class="col-sm-12">
                                        <button class="btn btn-info btn-block" type="submit">Simpan</button>
                                    </div>
                                </div>


                            </div>


                        </div>
                </div>
                </form>
            </div>
        </div>
    </div>

    @endsection