@extends('admin/master')
@section('content')
<div class="content-wrapper" style="background-color:white;">
    <div class="row">
        <div class="col-12 grid-margin">
            <div class="card">
                <div class="card-header">
                    <h4 class="page-title">
                        <span class="page-title-icon bg-gradient-primary text-white mr-2">
                            <i class="mdi mdi-contacts menu-icon"></i>
                        </span> Pendaftaran Pasien Baru </h4>
                </div>
                <div class="card-body" style="background-color:#F7F7F7;">
                    <form autocomplete="off" action="{{route('pasienbaruDaftar')}}" method="post"
                        class="form-horizontal">
                        {{ csrf_field() }}
                        <div class="row">
                            <div class="col-md-6">
                                <p>IDENTITAS PASIEN</p>
                                <div class="form-group row">

                                    @if($errors->has('nojkn_pasien'))

                                    <label class="col-sm-3 col-form-label" style="color:red">Nomor JKN : </label>
                                    <div class="col-sm-9">
                                        <input class="form-control" name="nojkn_pasien"
                                            placeholder="{{$errors->first('nojkn_pasien')}}"
                                            onkeypress='validate(event)'>
                                    </div>

                                    @else
                                    <label class="col-sm-3 col-form-label">Nomor JKN : </label>
                                    <div class="col-sm-9">
                                        <input class="form-control" name="nojkn_pasien" onkeypress='validate(event)'>
                                    </div>
                                    @endif

                                </div>
                                <div class="form-group row">
                                    @if($errors->has('nopasien'))
                                    <label class="col-sm-3 col-form-label" style="color:red">Nomor Pasien : </label>
                                    <div class="col-sm-9">
                                        <input class="form-control" name="nopasien"
                                            placeholder="{{$errors->first('nopasien')}}" onkeypress='validate(event)'>
                                    </div>
                                    @else
                                    <label class="col-sm-3 col-form-label">Nomor Pasien : </label>
                                    <div class="col-sm-9">
                                        <input class="form-control" name="nopasien" onkeypress='validate(event)'>
                                    </div>
                                    @endif

                                </div>
                                <div class="form-group row">
                                    @if($errors->has('nama_pasien'))
                                    <label class="col-sm-3 col-form-label" style="color:red">Nama Lengkap : </label>
                                    <div class="col-sm-9">
                                        <input class="form-control" name="nama_pasien"
                                            placeholder="{{$errors->first('nama_pasien')}}">
                                    </div>
                                    @else
                                    <label class="col-sm-3 col-form-label">Nama Lengkap : </label>
                                    <div class="col-sm-9">
                                        <input class="form-control" name="nama_pasien">
                                    </div>
                                    @endif

                                </div>
                                <div class="form-group row">
                                    @if($errors->has('tgl_lahir_pasien'))
                                    <label class="col-sm-3 col-form-label" style="color:red">Tanggal Lahir : </label>
                                    <div class="col-sm-9">
                                        <!-- <input class="form-control" type="date" name="tgl_lahir_pasien"
                                            placeholder="{{$errors->first('tanggal_lahir')}}"> -->
                                        <input name="tgl_lahir_pasien" type="text" class="datepicker-here form-control"
                                            data-language='en' data-multiple-dates="1"
                                            data-multiple-dates-separator=", " data-position='top left' />
                                    </div>
                                    @else
                                    <label class=" col-sm-3 col-form-label">Tanggal Lahir : </label>
                                    <div class="col-sm-9">
                                        <!-- <input class="form-control" type="date" name="tgl_lahir_pasien"> -->
                                        <input name="tgl_lahir_pasien" type="text" class="datepicker-here form-control"
                                            data-language='en' data-multiple-dates="1"
                                            data-multiple-dates-separator=", " data-position='top left' />
                                    </div>
                                    @endif

                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-3 col-form-label">Agama : </label>
                                    <div class="col-sm-9">
                                        <select class="form-control" name="agama_pasien">
                                            @foreach($pagama as $agama)
                                            <option value="{{$agama->id}}">{{$agama->agama}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group row">

                                    @if($errors->has('golongan_darah_pasien'))
                                    <label class="col-sm-3 col-form-label" style="color:red">Golongan Darah : </label>
                                    <div class="col-sm-9">
                                        <select class="form-control" name="golongan_darah_pasien">
                                            @foreach($goldarp as $goldarp)
                                            <option value="{{$goldarp->id}}">{{$goldarp->darah}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    @else
                                    <label class="col-sm-3 col-form-label">Golongan Darah : </label>
                                    <div class="col-sm-9">
                                        <select class="form-control" name="golongan_darah_pasien">
                                            @foreach($goldarp as $goldarp)
                                            <option value="{{$goldarp->id}}">{{$goldarp->darah}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    @endif
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-3 col-form-label">Pekerjaan : </label>
                                    <div class="col-sm-9">
                                        <select class="form-control" name="pekerjaan_pasien">
                                            @foreach($ppekerjaan as $ppekerjaan)
                                            <option value="{{$ppekerjaan->id}}">{{$ppekerjaan->pekerjaan}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    @if($errors->has('notlp_pasien'))
                                    <label class="col-sm-3 col-form-label" style="color:red">Nomor Telepon : </label>
                                    <div class="col-sm-9">
                                        <input class="form-control" name="notlp_pasien"
                                            placeholder="{{$errors->first('notlp_pasien')}}"
                                            onkeypress='validate(event)'>
                                    </div>
                                    @else
                                    <label class="col-sm-3 col-form-label">Nomor Telepon : </label>
                                    <div class="col-sm-9">
                                        <input class="form-control" name="notlp_pasien" onkeypress='validate(event)'>
                                    </div>
                                    @endif

                                </div>
                                <div class="form-group row">
                                    @if($errors->has('noktp_pasien'))
                                    <label class="col-sm-3 col-form-label" style="color:red">Nomor KTP : </label>
                                    <div class="col-sm-9">
                                        <input class="form-control" name="noktp_pasien" onkeypress='validate(event)'
                                            placeholder="{{$errors->first('noktp_pasien')}}">
                                    </div>
                                    @else
                                    <label class="col-sm-3 col-form-label">Nomor KTP : </label>
                                    <div class="col-sm-9">
                                        <input class="form-control" name="noktp_pasien" onkeypress='validate(event)'>
                                    </div>
                                    @endif

                                </div>
                                <div class="form-group row">
                                    @if($errors->has('alamat_pasien'))
                                    <label class="col-sm-3 col-form-label" style="color:red">Alamat : </label>
                                    <div class="col-sm-9">
                                        <textarea class="form-control" rows="3" id="comment" name="alamat_pasien"
                                            placeholder="{{$errors->first('alamat_pasien')}}"></textarea>
                                    </div>
                                    @else
                                    <label class="col-sm-3 col-form-label">Alamat : </label>
                                    <div class="col-sm-9">
                                        <textarea class="form-control" rows="3" id="comment"
                                            name="alamat_pasien"></textarea>
                                    </div>
                                    @endif

                                </div>

                            </div>
                            <div class="col-md-6">
                                <p>IDENTITAS Wali</p>
                                <div class="form-group row">
                                    @if($errors->has('nama_wali'))
                                    <label class="col-sm-3 col-form-label" style="color:red">Nama Wali : </label>
                                    <div class="col-sm-9">
                                        <input class="form-control" name="nama_wali"
                                            placeholder="{{$errors->first('nama_wali')}}">
                                    </div>
                                    @else
                                    <label class="col-sm-3 col-form-label">Nama Wali : </label>
                                    <div class="col-sm-9">
                                        <input class="form-control" name="nama_wali">
                                    </div>
                                    @endif

                                </div>
                                <div class="form-group row">
                                    @if($errors->has('tgl_lahir_wali'))
                                    <label class="col-sm-3 col-form-label" style="color:red">Tanggal Lahir : </label>
                                    <div class="col-sm-9">
                                        <!-- <input class="form-control" type="date" name="tgl_lahir_wali"
                                            placeholder="{{$errors->first('tgl_lahir_wali')}}"> -->
                                        <input name="tgl_lahir_wali" type="text" class="datepicker-here form-control"
                                            data-language='en' data-multiple-dates="1"
                                            data-multiple-dates-separator=", " data-position='top left' />

                                    </div>
                                    @else
                                    <label class="col-sm-3 col-form-label">Tanggal Lahir : </label>
                                    <div class="col-sm-9">
                                        <!-- <input class="form-control" type="date" name="tgl_lahir_wali"> -->
                                        <input name="tgl_lahir_wali" type="text" class="datepicker-here form-control"
                                            data-language='en' data-multiple-dates="1"
                                            data-multiple-dates-separator=", " data-position='top left' />
                                    </div>
                                    @endif

                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-3 col-form-label">Agama : </label>
                                    <div class="col-sm-9">
                                        <select class="form-control" name="agama_wali">
                                            @foreach($wagama as $wagama)
                                            <option value="{{$wagama->id}}">{{$wagama->agama}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    @if($errors->has('golongan_darah_wali'))
                                    <label class="col-sm-3 col-form-label" style="color:red">Golongan Darah : </label>
                                    <div class="col-sm-9">
                                        <select class="form-control" name="golongan_darah_wali">
                                            @foreach($goldarw as $goldarw)
                                            <option value="{{$goldarw->id}}">{{$goldarw->darah}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    @else
                                    <label class="col-sm-3 col-form-label">Golongan Darah : </label>
                                    <div class="col-sm-9">
                                        <!-- <input class="form-control" name="golongan_darah_wali"> -->
                                        <select class="form-control" name="golongan_darah_wali">
                                            @foreach($goldarw as $goldarw)
                                            <option value="{{$goldarw->id}}">{{$goldarw->darah}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    @endif

                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-3 col-form-label">Pendidikan Terakhir : </label>
                                    <div class="col-sm-9">
                                        <select class="form-control" name="pendidikan_wali">
                                            @foreach($pendidikan as $pendidikan)
                                            <option value="{{$pendidikan->id}}">{{$pendidikan->pendidikan}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-3 col-form-label">Pekerjaan : </label>
                                    <div class="col-sm-9">
                                        <select class="form-control" name="pekerjaan_wali">
                                            @foreach($wpekerjaan as $wpekerjaan)
                                            <option value="{{$wpekerjaan->id}}">{{$wpekerjaan->pekerjaan}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    @if($errors->has('notlp_wali'))
                                    <label class="col-sm-3 col-form-label" style="color:red">Nomor Telepon : </label>
                                    <div class="col-sm-9">
                                        <input class="form-control" name="notlp_wali"
                                            placeholder="{{$errors->first('notlp_wali')}}" onkeypress='validate(event)'>
                                    </div>
                                    @else
                                    <label class="col-sm-3 col-form-label">Nomor Telepon : </label>
                                    <div class="col-sm-9">
                                        <input class="form-control" name="notlp_wali" onkeypress='validate(event)'>
                                    </div>
                                    @endif

                                </div>
                                <div class="form-group row">
                                    @if($errors->has('noktp_wali'))
                                    <label class="col-sm-3 col-form-label" style="color:red">Nomor KTP : </label>
                                    <div class="col-sm-9">
                                        <input class="form-control" name="noktp_wali" onkeypress='validate(event)'
                                            placeholder="{{$errors->first('noktp_wali')}}">
                                    </div>
                                    @else
                                    <label class="col-sm-3 col-form-label">Nomor KTP : </label>
                                    <div class="col-sm-9">
                                        <input class="form-control" name="noktp_wali" onkeypress='validate(event)'>
                                    </div>
                                    @endif

                                </div>
                                <div class="form-group row">
                                    @if($errors->has('alamat_wali'))
                                    <label class="col-sm-3 col-form-label" style="color:red">Alamat : </label>
                                    <div class="col-sm-9">
                                        <textarea class="form-control" rows="2" id="comment" name="alamat_wali"
                                            placeholder="{{$errors->first('alamat_wali')}}"></textarea>
                                    </div>
                                    @else
                                    <label class="col-sm-3 col-form-label">Alamat : </label>
                                    <div class="col-sm-9">
                                        <textarea class="form-control" rows="2" id="comment"
                                            name="alamat_wali"></textarea>
                                    </div>
                                    @endif

                                </div>
                                <div class="form-group row justify-content-md-center mt-5">
                                    <button class="btn btn-info col-md-9" type="submit">Simpan</button>
                                </div>

                            </div>

                    </form>
                </div>


            </div>
        </div>
    </div>
    <script>
    $(function() {
        $('#datetimepicker1').datetimepicker({
            format: 'DD-MM-YYYY LT'
        });
        $('#datetimepicker2').datetimepicker({
            format: 'DD-MM-YYYY'
        });
        $('#datetimepicker3').datetimepicker({
            format: 'LT'
        });
        $('#datetimepicker3').datetimepicker({
            format: 'LT'
        });
    });
    </script>

    @endsection