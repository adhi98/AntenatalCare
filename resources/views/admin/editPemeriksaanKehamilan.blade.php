@extends('admin/master')
@section('content')
<div class="content-wrapper" style="background-color:white;">
    <div class="row">
        <div class="col-12 grid-margin">
            <div class="card">
                <div class="card-header">
                    <div class="row">
                        <div class="col">
                            <h4 class="float-left">Edit Pemeriksaan Kehamilan</h4>
                        </div>
                    </div>

                </div>
                <div class="card-body" style="background-color:#F7F7F7;">
                    <form autocomplete="off"
                        action="{{route('updateDetailRiwayatPemeriksaanKehamilan', ['id' => $pkh->id ])}}"
                        method="post">

                        {{ csrf_field() }}

                        <div class="row">

                            <div class="col-md-6">
                                <div class="row mb-2">
                                    <div class="col-sm-3">
                                        <label>Tanggal Pemeriksaan </label>
                                    </div>
                                    <div class="col-sm-9">
                                        <div class="input-group mb-3 input-group-sm">
                                            <input type="text" value="{{$pkh->tanggal_pemeriksaan}}"
                                                name="tanggal_pemeriksaan" data-range="true"
                                                data-multiple-dates-separator=" - " data-language="en"
                                                class="datepicker-here form-control" />
                                        </div>
                                    </div>
                                </div>

                                <div class="row mb-3">

                                    <div class="col-sm-3">
                                        <label>Panjang Lila </label>
                                    </div>
                                    <div class="col-sm-9">
                                        <div class="input-group input-group-sm">
                                            <input type="text" name="lingkar_lengan" class="form-control"
                                                onkeypress='validate(event)' value="{{$pkh->lingkar_lengan}}">
                                            <div class="input-group-append">
                                                <span class="input-group-text">cm</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="row mb-3">
                                    <div class="col-sm-3">
                                        <label>Berat Badan </label>
                                    </div>
                                    <div class="col-sm-9">
                                        <div class="input-group input-group-sm">
                                            <input type="text" name="berat_badan" class="form-control"
                                                value="{{$pkh->berat_badan}}" onkeypress='validate(event)'>
                                            <div class="input-group-append">
                                                <span class="input-group-text">cm</span>
                                            </div>
                                        </div>
                                    </div>

                                </div>

                                <div class="row mb-3">
                                    <div class="col-sm-3">
                                        <label>Tinggi Rahim </label>
                                    </div>
                                    <div class="col-sm-9">
                                        <div class="input-group input-group-sm">
                                            <input type="text" name="tinggi_rahim" class="form-control"
                                                value="{{$pkh->tinggi_rahim}}" onkeypress='validate(event)'>
                                            <div class="input-group-append">
                                                <span class="input-group-text">cm</span>
                                            </div>
                                        </div>
                                    </div>

                                </div>

                                <div class="row mb-3">
                                    <div class="col-sm-3">
                                        <label>Tekanan Darah </label>
                                    </div>
                                    <div class="col-sm-9">
                                        <div class="input-group input-group-sm">
                                            <input type="text" name="tekanan_darah" value="{{$pkh->tekanan_darah}}"
                                                class="form-control">
                                            <div class="input-group-append">
                                                <span class="input-group-text">mmHg</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="row mb-3">
                                    <div class="col-sm-3">
                                        <label>Letak Janin </label>
                                    </div>
                                    <div class="col-sm-9">
                                        <div class="input-group input-group-sm">
                                            <select class="form-control" name="letak_janin">
                                                @foreach($janin as $janin)
                                                <option value="{{$janin->id}}" @if($janin->id ===
                                                    $pkh->letak_janin)
                                                    selected
                                                    @endif
                                                    >{{$janin->posisi}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                </div>

                                <div class="row mb-3">
                                    <div class="col-sm-3">
                                        <label>Denyut Janin </label>
                                    </div>
                                    <div class="col-sm-9">
                                        <div class="input-group input-group-sm">
                                            <input type="text" value="{{$pkh->denyut_jantung_janin}}"
                                                name="denyut_jantung_janin" class="form-control">
                                        </div>
                                    </div>
                                </div>

                                <div class="row mb-3">
                                    <div class="col-sm-3">
                                        <label>Kaki Bengkak </label>
                                    </div>
                                    <div class="col-sm-9">
                                        <div class="input-group input-group-sm">
                                            <input type="text" value="{{$pkh->kaki_bengkak}}" name="kaki_bengkak"
                                                class="form-control">
                                        </div>
                                    </div>
                                </div>

                                <div class="row mb-2">
                                    <div class="col-sm-3">
                                        <label>Tes Lab Anemia </label>
                                    </div>
                                    <div class="col-sm-9">
                                        <div class="input-group input-group-sm">
                                            <textarea name="anemia" id="" rows="2"
                                                class="form-control">{{$pkh->anemia}}</textarea>
                                        </div>
                                    </div>
                                </div>


                            </div>

                            <div class="col-md-6">

                                <!-- Hasil Laboratorium New -->

                                <div class="row mb-2">
                                    <div class="col-sm-3">
                                        <label>Tes Lab HIV </label>
                                    </div>
                                    <div class="col-sm-9">
                                        <div class="input-group input-group-sm">
                                            <textarea name="hiv" id="" rows="2"
                                                class="form-control">{{$pkh->hiv}}</textarea>
                                        </div>
                                    </div>
                                </div>

                                <div class="row mb-2">
                                    <div class="col-sm-3">
                                        <label>Tes Lab Sivilis </label>
                                    </div>
                                    <div class="col-sm-9">
                                        <div class="input-group input-group-sm">
                                            <textarea name="sifilis" id="" rows="2"
                                                class="form-control">{{$pkh->sivilis}}</textarea>
                                        </div>
                                    </div>
                                </div>

                                <!--  -->

                                <div class="row mb-2">
                                    <div class="col-sm-3">
                                        <label>Keluhan </label>
                                    </div>
                                    <div class="col-sm-9">
                                        <div class="input-group input-group-sm">
                                            <textarea name="keluhan" id="" rows="2"
                                                class="form-control">{{$pkh->keluhan}}</textarea>
                                        </div>
                                    </div>
                                </div>

                                <div class="row mb-2">
                                    <div class="col-sm-3">
                                        <label>Tindakan Khusus </label>
                                    </div>
                                    <div class="col-sm-9">
                                        <div class="input-group input-group-sm">
                                            <textarea name="tindakan" id="" rows="2"
                                                class="form-control">{{$pkh->tindakan}}</textarea>
                                        </div>
                                    </div>
                                </div>

                                <div class="row mb-2">
                                    <div class="col-sm-3">
                                        <label>Nasihat </label>
                                    </div>
                                    <div class="col-sm-9">
                                        <div class="input-group input-group-sm">
                                            <textarea name="hasil_konsultasi" id="" rows="4"
                                                class="form-control">{{$pkh->hasil_konsultasi}}</textarea>
                                        </div>
                                    </div>
                                </div>


                                <div class="row mb-2">
                                    <div class="col-sm-3">
                                        <label>Tanggal Kembali </label>
                                    </div>
                                    <div class="col-sm-9">
                                        <div class="input-group mb-3 input-group-sm">
                                            <input type="text" name="tanggal_kembali" data-range="true"
                                                value="{{$pkh->tanggal_kembali}}" data-multiple-dates-separator=" - "
                                                data-language="en" class="datepicker-here form-control" />
                                        </div>
                                    </div>

                                </div>

                                <div class="row mb-2">
                                    <div class="col-sm-12">
                                        <button class="btn btn-warning btn-block" type="submit">Update</button>
                                    </div>
                                </div>


                            </div>


                        </div>


                </div>
                </form>
            </div>
        </div>
    </div>

    @endsection